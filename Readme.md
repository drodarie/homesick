# Homesick: a RPG game interface
![Build](https://gitlab.com/drodarie/Homesick/badges/master/pipeline.svg)
![Coverage](https://gitlab.com/drodarie/Homesick/badges/master/coverage.svg)

## Summary
**Homesick** is a game interface to create RPG games in an discontinuous environment.<br>
It provides functions to create instances of **Item**, **Character**, **Map** and **Mission** along with all the interactions
they can produce in terms of **Action** and **Buff**.<br>
Each player interprets a **Character** which list of **Skill** and characteristics (Buff) evolves through time.
They can interact with other **Entity** of the game to accomplish their **Objective**.

## Installation
### 1. Download
From terminal:`git clone https://gitlab.com/drodarie/Homesick/`

### 2. Install Requirements
This game is working in C++11 and is actually only supported on Linux.

| Usage | CMake flag | CMake Libraries | How to install (Ubuntu) |
|---|---|---|---|
| Minimal requirements | None | CMake, Make and [ZLib](https://www.zlib.net/) | `apt-get install zlib1g-dev` |
| Code documentation | None | [Doxygen](http://doxygen.nl/) and [Dot](https://dotuml.com/) | `apt-get install graphviz doxygen` |  
| Unit testing | `test` | [Gtest](https://github.com/google/googletest) | `apt-get install libgtest-dev` (needs to be compiled) |
| Code coverage | `coverage`| [Lcov](https://wiki.documentfoundation.org/Development/Lcov) | `apt-get install lcov` |
| Memory checks | `memory_leaks`| [Valgrind](http://valgrind.org/) | `apt-get install valgrind` |

A Docker image is available [here](https://hub.docker.com/r/drodarie/cpp-env) with all the requirements.<br>
The following command will download the image (if needed), create a container, and connect to it.<br>
`docker run -ti --name Homesick -v /PATH_TO_PROJECT/Homesick/:/home/root/Homesick -w /home/root/Homesick 
--rm drodarie/cpp-env:latest`<br>
Replace PATH_TO_PROJECT with the absolute path to the folder Homesick
### 3. Build
From a Linux terminal or docker container, inside the project folder:<br>
`cmake -H. -Bbuild; make -C build`

### 4. Usage
* Main program: `./build/homesick`
* Documentation: `make -C build doc` Documentation will be stored in the *doc/documentation* folder.
* Unit tests: `./build/unit-testing`
* Tests coverage: `cmake --build build/ --target testing_coverage` Results will be stored in the 
*doc/testing_coverage* folder.
* Memory Checks: `cmake --build build/ --target valgrind`

## Architecture
The project has been shaped so that the main classes and functionalities are available and it can be easily extended 
if need be.<br>
It is based on a [Model View Controller Pattern](https://en.wikipedia.org/wiki/Model-view-controller)

### Models
The model classes are meant to be stored and retrieved from binary files.<br>
In order to dynamically load classes during file reading, model classes implement the 
[Curiously Recurring Template Pattern](https://en.wikipedia.org/wiki/Curiously_recurring_template_pattern) which 
allows the creation of static maps holding pointers to their functions.<br>
Each Model classe **T** multi-inherits from 2 classes: 
* The first is its parent class inheriting of **Object**, and its functions to work with binary files.<br>
**T** implements therefore a [copy constructor](https://en.wikipedia.org/wiki/Copy_constructor_(C++)), and it 
should have a static function **load** with the same signature as **Object::load** and overrides the **save** function.
* The second is **ObjectProxy\<T\>** which populates static maps with its load and copy constructor so 
that they can be retrieved by the **Object** class during the reading of binary files.

By default, model classes store their instances in the *data/defaults/CLASS_NAME.bin* file.<br>
During tests, a *data/tests* folder will be created to store test files.

## Documentation
For further information, consider reading:
* [Code documentation](https://drodarie.gitlab.io/homesick/)