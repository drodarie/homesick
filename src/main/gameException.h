/**
 * @file gameException.h
 * @author drodarie
 * @date 02/08/17
 */

#ifndef HOMESICK_GAME_EXCEPTION_H
#define HOMESICK_GAME_EXCEPTION_H

#include <exception>
#include <string>

using namespace std;

/**
 * @brief Class extension for game handled exceptions
 */
class gameException : public exception
{
protected:
  string message_; //!< Error Message
  int code_; //!< Error code
public:
  gameException(string message = "GAME EXCEPTION", int code = 1) : message_(std::move(message)), code_(code)
  {}

  int getValue()
  { return code_; }

  virtual ~gameException() throw()
  {}

  const char *what() const noexcept override
  {
    return message_.c_str();
  }
};

#define _GAME_ERR_(_N, _id) class _N : public gameException { \
    public: _N(const char *message) : gameException(message,_id) {} \
            _N(const std::string &s) : gameException(s,_id) {} };


_GAME_ERR_(ARGUMENT_ERROR, 10) //!< Function argument error
_GAME_ERR_(CANT_DO_ERROR, 11) //!< Function can't perform as expected
_GAME_ERR_(CONTEXT_ERROR, 12) //!< Specific context made function stop
_GAME_ERR_(FILE_ERROR, 20) //!< File handling exception
_GAME_ERR_(READING_FILE_ERROR, 21) //!< Reading file exception
_GAME_ERR_(WRITING_FILE_ERROR, 22) //!< Writing file exception
_GAME_ERR_(DELETING_FILE_ERROR, 23) //!< Deleting file exception

#undef _GAMEERR_

#endif //HOMESICK_GAME_EXCEPTION_H
