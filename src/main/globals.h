/**
 * @file globals.h
 * @author drodarie
 * @date 12/18/17.
*/

#ifndef HOMESICK_GLOBALS_H
#define HOMESICK_GLOBALS_H

#include <cstdio>
#include <string>
#include <sys/stat.h>
#include <zconf.h>
#include <vector>
#include <dirent.h>
#include "gameException.h"

using namespace std;

/**
 * @brief Get File absolute filename
 * @param file pointer to file
 * @return Absolute filename
 */
string getFileFilename(FILE *file);

/**
 * @brief Get Absolute path from local path
 * @param path local path
 * @return Absolute path
 */
string getAbsolutePath(const string &path);

bool exists_file(const string &name);

void delete_file(const string &name) noexcept(false);

vector<string> listDir(const string &name) noexcept(false);

const string _DATA_FOLDER_ = getAbsolutePath("../data/");
const string _DEFAULT_DATA_FOLDER_ = (_DATA_FOLDER_ + "defaults/");
const string _TEST_FOLDER_ = (_DATA_FOLDER_ + "tests/");

#endif //HOMESICK_GLOBALS_H
