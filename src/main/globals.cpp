/**
 * @file globals.cpp
 * @author drodarie
 * @date 12/18/17.
*/

#include "globals.h"

using namespace std;

string getFileFilename(FILE *file)
{
  char filename[PATH_MAX + 1];
  ssize_t size = readlink(string("/proc/self/fd/" + to_string(file->_fileno)).c_str(), filename, PATH_MAX);
  if (size != -1)
    filename[size] = '\0';
  return string(filename);
}

string getAbsolutePath(const string &path)
{
  char resolved_path[PATH_MAX];
  realpath(path.c_str(), resolved_path);
  return string(resolved_path) + "/";
}

bool exists_file(const std::string &name)
{
  struct stat buffer;
  return (stat(name.c_str(), &buffer) == 0);
}

void delete_file(const string &name) noexcept(false)
{
  if (remove(name.c_str()) != 0)
    throw DELETING_FILE_ERROR("delete_file: Error deleting file: " + name);
}

vector <string> listDir(const string &name) noexcept(false)
{
  DIR *dir;
  struct dirent *ent;
  vector <string> result;
  if ((dir = opendir(name.c_str())) != NULL)
  {
    while ((ent = readdir(dir)) != NULL)
    {
      if (DT_REG == ent->d_type)
        result.push_back(name + ent->d_name);
    }
    closedir(dir);
  } else
    throw READING_FILE_ERROR("listDir: Could not access directory: " + name);
  return result;
}