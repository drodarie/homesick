/**
 * @file action.h
 * @author drodarie
 * @date 30/07/17
 */

#ifndef HOMESICK_ACTION_H
#define HOMESICK_ACTION_H


#include "iaction.h"

/**
 * @brief Class to describe the Actions that Characters can perform in Homesick.
 */
class Action: public IAction
{
protected:
  Character* const actor_; //!< Character performing the action
  vector<Entity*> const targets_; //!< Entity that the action is targeting
  Map* const location_;
  float global_time_; //!< Current time of the action
  float local_time_; //!< Time of last update
  int bonus_; //!< Global bonus to test score
  Test resultTest_; //!< Results of the action test
  bool hasProceed_; //!< Has the action been initialized with the proceed function

public:
  /**
   * @brief Default constructor for an Action
   * @param action IAction to perform
   * @param actor Character performing the action
   * @param targets List of entity targeted by the action
   * @param location Area where the action is taking place
   * @param initial_time Time at which the action starts
   */
  Action(const IAction &action, Character* const &actor,
    vector<Entity*> const& targets, Map* const &location, float initial_time):
    IAction(action), actor_(actor), targets_(targets), location_(location), global_time_(initial_time)
  {
    bonus_=0;
    local_time_ = initial_time;
    hasProceed_ = false;
  }

  Action(const Action &action):
    IAction(action),
    actor_(action.actor_),
    targets_(action.targets_),
    location_(action.location_),
    global_time_(action.global_time_),
    bonus_(action.bonus_)
  {
    local_time_ = global_time_;
    hasProceed_ = false;
  }

  ~Action(){}

  float getCurrentTime() const
  { return global_time_; }

  float getLocalTime() const
  { return local_time_; }

  int getGlobalBonus() const
  { return bonus_; }

  void setGlobalBonus(int bonus)
  { bonus_ = bonus; }

  const Test &getResultTest() const {
    return resultTest_;
  }

  /**
   * @brief Apply instantaneous effects of the action an actor
   * @return True if the action has been completed
   */
  virtual bool proceed() noexcept(false);

  /**
   * @brief Update the action effects after a certain time period
   * @param timeStep time period since last update
   * @return True if the action has been completed
   */
  virtual bool update(float timeStep);
};

#endif //HOMESICK_ACTION_H
