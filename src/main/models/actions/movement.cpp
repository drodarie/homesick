/**
 * @file movement.cpp
 * @author drodarie
 * @date 7/20/18.
*/

#include "movement.h"


bool Movement::proceed()
{
  if (!location_->hasEntity(actor_)) {
    throw ARGUMENT_ERROR("Movement::proceed: The actor is not on the area");
  }
  Action::proceed();
  pathfinder_ = new MTDstar(actor_->getPosition(), *target_, min_height_, *location_, 1);
  return false;
}

bool Movement::update(float timeStep)
{
  if (not hasProceed_ || pathfinder_== nullptr)
    return false;
  global_time_ += timeStep;
  current_speed_ = max(1e-3f, min(timeStep*acceleration_ + current_speed_, actor_->getTotalBuff().getSpeed()));
  auto stepDistance = round(timeStep * current_speed_ * 1e3f) / 1e3f, dist=1.0f-update_distance_;
  update_distance_ += stepDistance;
  auto hasUpdate = update_distance_>=1.0f;
  try
  {
    pathfinder_->setTarget(*target_);
    while(update_distance_>=1.0f)
    {
      pathfinder_->update(min_height_, *location_);
      location_->moveEntity(actor_, pathfinder_->getCurrentPosition());
      local_time_ += update_time_ + timeStep*dist/stepDistance;
      update_time_ = 0;
      dist = 1.0f;
      update_distance_--;
    }
  }catch(ARGUMENT_ERROR &e)
  {}
  if (hasUpdate)
    update_time_ = timeStep*update_distance_/stepDistance;
  else update_time_ += timeStep;
  return pathfinder_->getCurrentPosition() == pathfinder_->getTarget();
}
