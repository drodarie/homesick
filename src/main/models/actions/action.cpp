/**
 * @file action.cpp
 * @author drodarie
 * @date 04/08/17
 */

#include "action.h"

bool Action::proceed() noexcept(false)
{
  if (!canPerform(actor_, targets_))
    throw CONTEXT_ERROR("Action::proceed: The actor cannot perform the action");
  resultTest_ = testAction(actor_, bonus_);
  hasProceed_ = true;
  return false;
}

bool Action::update(float timeStep)
{
  if (not hasProceed_)
    return false;
  global_time_ += timeStep;
  return global_time_ - local_time_ > difficulty_.getDuration();
}
