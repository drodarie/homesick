/**
 * @file iaction.h
 * @author drodarie
 * @date 5/26/19
 */

#ifndef HOMESICK_IACTION_H
#define HOMESICK_IACTION_H

#include <ostream>
#include "../buff.h"
#include "../skill.h"
#include "../entities/characters/character.h"

/**
 * @brief Structure to describe the results of the random tests.
 */
typedef struct
{
  int success; //!< Score of the random test.
  vector<unsigned int> randomTests; //!< Random picked values for debug or display
} Test;

/**
 * @brief Class to describe the Actions that Characters can perform in Homesick.
 */
class IAction : public NamedObject, public ObjectProxy<IAction>
{
protected:
  set<Skill> required_skills_; //!< List of the required Skill to perform the action
  map<Skill, int> linked_skills_; //!< Map of the linked Skill and the bonus they provide to the action.
  Buff difficulty_; //!< Default difficulty of the task, contains also its duration.
                    //!< Negative characteristics will not be tested.
  bool check_availability_; //!< Flag which to assert if the linked entities should be available for action
  bool can_react_; //!< Check if linked entities can react to action


public:
  static const unsigned int _MAX_DIFFICULTY_; //!< Maximum difficulty for the actions.

  /**
   * @brief Default constructor for an Action
   * @param id
   * @param name
   * @param description
   * @param difficulty
   * @param check_availability Does the action need its target(s) to be available
   * @param can_react Does the action allow its target to react
   * @param filename path to file where the Action will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  IAction(unsigned int id,
         const string& name,
         const string& description,
         const Buff& difficulty,
         bool check_availability = false,
         bool can_react = false,
         const string& filename=ObjectProxy<IAction>::defaultFilename_,
         const string& nameClass=ObjectProxy<IAction>::className_):
    NamedObject(id, filename, name, description, nameClass),
    difficulty_(difficulty),
    check_availability_(check_availability),
    can_react_(can_react)
  {
    difficulty_.cutCharacteristicsTooHigh(IAction::_MAX_DIFFICULTY_);
  }

  /**
   * @brief Default constructor for an Action
   * @param name
   * @param description
   * @param difficulty
   * @param check_availability Does the action need its target(s) to be available
   * @param can_react Does the action allow its target to react
   * @param filename path to file where the Action will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  IAction(const string& name,
          const string& description,
          const Buff& difficulty,
          bool check_availability = false,
          bool can_react = false,
          const string& filename=ObjectProxy<IAction>::defaultFilename_,
          const string& nameClass=ObjectProxy<IAction>::className_):
    NamedObject(filename, name, description, nameClass),
    difficulty_(difficulty),
    check_availability_(check_availability),
    can_react_(can_react)
  {
    difficulty_.cutCharacteristicsTooHigh(IAction::_MAX_DIFFICULTY_);
  }

  IAction(const IAction &action): NamedObject(action), difficulty_({action.difficulty_}),
                                check_availability_(action.check_availability_), can_react_(action.can_react_)
  {
    std::copy(action.required_skills_.begin(),action.required_skills_.end(),
              inserter(required_skills_, required_skills_.begin()));
    std::copy(action.linked_skills_.begin(),action.linked_skills_.end(),
              inserter(linked_skills_, linked_skills_.begin()));
  }

  set<Skill> getRequiredSkills() const
  { return required_skills_; }

  bool requireSkill(const Skill& skill) const;

  void addRequiredSkill(const Skill& skill);

  void removeRequiredSkill(const Skill& skill);

  set<Skill> getLinkedSkills() const;

  void setLinkedSkill(const Skill &skill, int bonus);

  void removeLinkedSkill(const Skill& skill);

  Buff getDifficulty() const
  { return difficulty_; }

  void setDifficulty(const Buff& difficulty);

  bool checkAvailability() const
  { return check_availability_; }

  void setCheckAvailability(bool check_availability)
  { check_availability_ = check_availability; }

  bool canReact() const
  { return can_react_; }

  void setCanReact(bool can_react)
  { can_react_ = can_react; }

  /**
   * @brief Check if the actor can perform a specific action according to its set of skill
   * @param actor Character who wants to perform the action
   * @param targets List of target to the action, the action might require them to be not busy
   * @return True if the Character can perform the action
   */
  bool canPerform(const Character* actor, const vector<Entity*>& targets) const;

  int getBonus(const Skill& skill) const;

  /**
   * @brief Get the total bonus linked to a set of skills
   * @param skills
   * @return
   */
  int getTotalBonus(const set<Skill>& skills) const;

  /**
   * @brief Perform a test to see if an action succeeds or not
   * @param actor
   * @param extra_bonus Additional bonus for every dice roll
   * @return Results of the test
   */
  virtual Test testAction(const Character *actor, int extra_bonus=0) const;

  friend ostream &operator<<(ostream &os, const IAction &action);

  bool equals(const IAction &rhs) const;

  void save(Store *store, bool subclass=false) const;

  static Object* load(FILE *file);
};

#endif //HOMESICK_IACTION_H
