/**
 * @file movement.cpp
 * @author drodarie
 * @date 31/07/17.
*/

#ifndef HOMESICK_MOVEMENT_H
#define HOMESICK_MOVEMENT_H


#include "action.h"
#include "../utils/mtdstar.h"

/**
 * @brief Class to describe movements that Characters can perform in Homesick.
 */
class Movement : public Action
{
protected:
  MTDstar* pathfinder_= nullptr; //!< Pathfinder algorithm
  const Position* target_; //!< Target position of the movement
  unsigned int min_height_; //!< Minimum height of the character
  float acceleration_; //!< Acceleration of the movement
  float current_speed_; //!< Current speed of the character
  float update_distance_; //!< Distance not yet travelled
  float update_time_; //!< Time waited since last update
public:
  /**
   * @brief Default Constructor
   * @param action IAction to perform
   * @param actor Character performing the action
   * @param targets List of entity targeted by the action
   * @param location Area where the action is taking place
   * @param initial_time Initial time of the action
   * @param target Position to reach through movements
   * @param acceleration Acceleration of the character movement
   * @param min_height Minimum height of the space in which the character can move
   */
  Movement(const IAction &action, Character* const &actor,
    vector<Entity*> const& targets, Map* const &location, float initial_time,
    const Position* target, const float acceleration=0, const unsigned int min_height=1):
    Action(action, actor, targets, location, initial_time),
    target_(target),
    min_height_(min_height),
    acceleration_(acceleration)
  {
    current_speed_=min(action.getDifficulty().getSpeed(), actor->getTotalBuff().getSpeed());
    update_distance_ = 0;
    update_time_ = 0;
  }

  Movement(const Movement &movement):
    Action(movement),
    target_(movement.target_),
    min_height_(movement.min_height_),
    acceleration_(movement.acceleration_)
  {
    current_speed_=min(movement.difficulty_.getSpeed(), movement.actor_->getTotalBuff().getSpeed());
    update_distance_ = 0;
    update_time_ = 0;
  }

  ~Movement()
  {
    delete pathfinder_;
  }

  const Position &getLocalTarget() const
  {
    return pathfinder_->getTarget();
  }

  float getAcceleration() const
  {
    return acceleration_;
  }

  void setAcceleration(float acceleration)
  {
    acceleration_ = acceleration;
  }

  /**
   * @brief Apply instantaneous effects of the action an actor
   * @return True if the action has been completed
   */
  bool proceed() override;

  /**
   * @brief Update the action effects after a certain time period
   * @param timeStep time period since last update
   * @return True if the action has been completed
   */
  bool update(float timeStep) override;
};

#endif //HOMESICK_MOVEMENT_H
