/**
 * @file action.cpp
 * @author drodarie
 * @date 5/26/19
 */

#include "iaction.h"

const unsigned int IAction::_MAX_DIFFICULTY_ = 100;

void IAction::removeLinkedSkill(const Skill& skill)
{
  for (auto i = linked_skills_.begin(); i != linked_skills_.end(); i++)
    if (i.operator*().first == skill)
    {
      if (required_skills_.find(skill) == required_skills_.end())
        linked_skills_.erase(i);
      else
        linked_skills_[skill] = 0;
      break;
    }
}

bool IAction::requireSkill(const Skill& skill) const
{
  for (auto &required_skill : required_skills_)
    if (required_skill == skill)
      return true;
  return false;
}

bool IAction::canPerform(const Character* actor, const vector<Entity*>& targets) const
{
  if (!actor->isAvailable())
    return false;
  auto skills = actor->getSkills();
  vector<Skill> copy = {skills.begin(), skills.end()};
  auto i = required_skills_.begin();
  for (; i!= required_skills_.end(); i++)
  {
    auto available_skill = copy.begin();
    if (available_skill == copy.end())
      break;
    for (; available_skill != copy.end(); available_skill++)
      if (*available_skill == *i)
      {
        copy.erase(available_skill);
        break;
      }
  }
  if (i != required_skills_.end())
    return false;
  if (!checkAvailability())
    return true;
  for (auto &i: targets)
    if (!i->isAvailable())
      return false;
  return true;
}

int IAction::getTotalBonus(const set<Skill>& skills) const
{
  int result = 0;
  for (auto &i : skills)
  {
    auto it = linked_skills_.find(i);
    if (it != linked_skills_.end())
      result += it.operator*().second;
  }
  return result;
}

Test IAction::testAction(const Character *actor, int extra_bonus) const
{
  Test test;
  test.success = 0;
  vector<int> actionBuff = (actor->getTotalBuff() - difficulty_).getTestCharacteristics();

  int bonus = getTotalBonus(actor->getSkills()) + extra_bonus;

  for (int i : difficulty_.getTestCharacteristics())
    if (i >= 0) // Only positive difficulties are tested
    {
      unsigned int randomTest = static_cast<unsigned int>(random()) % 100 + 1;
      if (randomTest <= 5) // critical success
        test.success += 100;
      else if (randomTest > 95) // critical failure
        test.success -= 100;
      else
        test.success += actionBuff[i] + bonus - randomTest;
      test.randomTests.push_back(randomTest);
    }
  return test;
}

int IAction::getBonus(const Skill& skill) const
{
  auto it = linked_skills_.find(skill);
  if (it !=linked_skills_.end())
    return it.operator*().second;
  return 0;
}

void IAction::save(Store *store, bool subclass) const
{
  NamedObject::save(store, subclass);
  difficulty_.save(store,true);
  *store<<check_availability_;
  *store<<can_react_;

  *store<<static_cast<int>(required_skills_.size());
  for(auto &skill: required_skills_)
    skill.save(store, true);

  *store<<static_cast<int>(linked_skills_.size());
  for(auto &skill: linked_skills_)
  {
    skill.first.save(store, true);
    *store<<skill.second;
  }
}

Object *IAction::load(FILE *file)
{
  auto *namedObject = load_T<NamedObject>(file);
  auto * difficulty = load_T<Buff>(file);

  auto *result = new IAction(namedObject->getId(), namedObject->getName(), namedObject->getDescription(), *difficulty,
                             readBool(file), readBool(file), namedObject->getFilename());
  auto size = static_cast<unsigned int>(readInt(file));
  for (unsigned int i=0; i<size; i++)
  {
    auto* skill = load_T<Skill>(file);
    result->addRequiredSkill(*skill);
    delete skill;
  }

  size = static_cast<unsigned int>(readInt(file));
  for (unsigned int i=0; i<size; i++)
  {
    auto* skill = load_T<Skill>(file);
    result->setLinkedSkill(*skill, readInt(file));
    delete skill;
  }

  delete(namedObject);
  delete(difficulty);
  return result;
}

void IAction::addRequiredSkill(const Skill &skill)
{
  required_skills_.insert(Skill(skill));
  if (linked_skills_.find(skill)==linked_skills_.end())
    linked_skills_[skill] = 0;
}

void IAction::removeRequiredSkill(const Skill &skill) {
  for (auto i = required_skills_.begin(); i != required_skills_.end(); i++)
    if (i.operator*() == skill)
    {
      required_skills_.erase(i);
      break;
    }
  for (auto i = linked_skills_.begin(); i != linked_skills_.end(); i++)
    if (i.operator*().first == skill)
    {
      linked_skills_.erase(i);
      break;
    }
}

set<Skill> IAction::getLinkedSkills() const
{
  set<Skill> result;
  for (auto &i: linked_skills_)
    result.insert(i.first);
  return result;
}

void IAction::setLinkedSkill(const Skill &skill, int bonus)
{
  auto it = linked_skills_.find(skill);
  if (it==linked_skills_.end())
    linked_skills_[skill] = bonus;
  else
    it.operator*().second = bonus;
}

void IAction::setDifficulty(const Buff& difficulty) {
  if (! difficulty.areCharacteristicsTooHigh(_MAX_DIFFICULTY_))
    difficulty_ = difficulty;
}

ostream &operator<<(ostream &os, const IAction &action) {
  os << static_cast<const NamedObject &>(action) << ",\nrequired skills: {";
  for (auto &i: action.required_skills_)
    os << "\n\t{"<<i<<"},";
  os << "}\nlinked skills: {";
  for (auto &i: action.linked_skills_)
    os << "\n\t{"<<i.first<<"}, bonus: "<<i.second<<",";
  os << "}\ndifficulty: {" << action.difficulty_ << "},\ncheck availability: " << action.check_availability_
     << ", can react: " << action.can_react_;
  return os;
}

bool IAction::equals(const IAction &rhs) const {
  return NamedObject::equals(rhs) &&
         required_skills_ == rhs.required_skills_ &&
         linked_skills_ == rhs.linked_skills_ &&
         difficulty_ == rhs.difficulty_ &&
         check_availability_ == rhs.check_availability_ &&
         can_react_ == rhs.can_react_;
}
