/**
 * @file position.h
 * @author drodarie
 * @date 31/07/17
 */

#ifndef HOMESICK_POSITION_H
#define HOMESICK_POSITION_H

#include <cstdlib>
#include <cmath>
#include <ostream>
#include "base.h"

enum class Orientation : int
{
  Steady = 0,
  Up,
  Down,
  Left,
  Right,
  Forward,
  Backward
};

/**
 * @brief Class representing a 3D position in space
 */
class Position : public Object, public ObjectProxy<Position>
{
protected:
  int x_; //!< x-axis coordinate
  int y_; //!< y-axis coordinate
  int z_; //!< z-axis coordinate

public:
  /**
   * @brief Default Constructor
   * @param x_
   * @param y_
   * @param z_
   * @param nameClass name of the class. See Object
   */
  Position(int x_ = 0, int y_ = 0, int z_ = 0, const string &nameClass = ObjectProxy<Position>::className_) :
    Object(nameClass), x_(x_), y_(y_), z_(z_)
  {}

  /**
   * @brief Constructor copy
   * @param position
   */
  Position(const Position &position) :
    Object(position), x_(position.x_), y_(position.y_), z_(position.z_)
  {}

  Position(const Orientation &orientation, const string &nameClass = ObjectProxy<Position>::className_) :
    Object(nameClass)
  {
    switch (orientation)
    {
      case Orientation::Up:
        x_ = 0;
        y_ = 0;
        z_ = 1;
        break;
      case Orientation::Down:
        x_ = 0;
        y_ = 0;
        z_ = -1;
        break;
      case Orientation::Left:
        x_ = -1;
        y_ = 0;
        z_ = 0;
        break;
      case Orientation::Right:
        x_ = 1;
        y_ = 0;
        z_ = 0;
        break;
      case Orientation::Forward:
        x_ = 0;
        y_ = 1;
        z_ = 0;
        break;
      case Orientation::Backward:
        x_ = 0;
        y_ = -1;
        z_ = 0;
        break;
      default:
        x_ = 0;
        y_ = 0;
        z_ = 0;
        break;
    }
  }

  Position(const int position[3], const string &nameClass = ObjectProxy<Position>::className_)
  {
    x_ = position[0];
    y_ = position[1];
    z_ = position[2];
  }

  int getX() const
  {
    return x_;
  }

  void setX(int x)
  {
    x_ = x;
  }

  int getY() const
  {
    return y_;
  }

  void setY(int y)
  {
    y_ = y;
  }

  int getZ() const
  {
    return z_;
  }

  void setZ(int z)
  {
    z_ = z;
  }

  bool operator==(const Position &rhs) const
  {
    return x_ == rhs.x_ && y_ == rhs.y_ && z_ == rhs.z_;
  }

  bool operator!=(const Position &rhs) const
  {
    return !(rhs == *this);
  }

  Position operator*(const int k)
  {
    return Position(x_ * k, y_ * k, z_ * k);
  }

  void operator+=(const Position position)
  {
    x_ += position.x_;
    y_ += position.y_;
    z_ += position.z_;
  }

  void operator-=(const Position position)
  {
    x_ -= position.x_;
    y_ -= position.y_;
    z_ -= position.z_;
  }

  Position operator+(const Position &position) const
  {
    return Position(x_ + position.x_, y_ + position.y_, z_ + position.z_);
  }

  Position operator-(const Position &position) const
  {
    return Position(x_ - position.x_, y_ - position.y_, z_ - position.z_);
  }

  int dot(const Position &position) const
  {
    return x_ * position.x_ + y_ * position.y_ + z_ * position.z_;
  }

  static double dot(vector<double> v1, vector<double> v2);

  /**
   * @brief Euclidian distance between Position and (0,0,0)
   * @return Position's vector length
   */
  double length() const
  {
    return sqrt(x_ * x_ + y_ * y_ + z_ * z_);
  }

  /**
   * @brief Normalizes the Position
   * @return Normalized Position
   * @throw ARGUMENT_ERROR if one of the Positions length is 0
   */
  vector<double> norm() const noexcept(false);

  /**
   * @brief Computes angle between current and position vectors
   * @param position Position considered as vector
   * @return Angle between Positions in radians
   * @throw ARGUMENT_ERROR if one of the Positions length is 0
   */
  double angle(const Position &position) const noexcept(false);

  /**
   * @brief Computes the Manhattan distance between the class and a parameter position
   * @param position
   * @return Manhattan distance to the position
   */
  unsigned int getManhattanDistance(Position position) const;

  /**
   * @brief Computes the Manhattan distance between the class and a parameter position according to the x-y plan
   * @param position
   * @return Manhattan distance to the position
   */
  unsigned int get2DManhattanDistance(Position position) const;

  /**
   * @brief Computes the distance between the class and a parameter position
   * @param position
   * @return distance to the position
   */
  double getDistance(Position position) const;

  /**
   * @brief Computes the distance between the class and a parameter position according to the x-y plan
   * @param position
   * @return distance to the position
   */
  double get2DDistance(Position position) const;

  /**
 * @brief Checks if a position is close enough and in the angle limits around an orientation.
 * @param target position to check
 * @param orientation direction for the angle test
 * @param radius Maximum distance from the center.
 * @param angle limit centered on the orientation axis.
 * @return 0 if the position is not in the limits,
 * 1 if it is inside the spherical range, 2 if it is on the spherical range
 */
  int isInField(const Position &target,
                const Orientation &orientation, unsigned int radius, float angle) const;

  /**
 * @brief Compute the list of Position between the position and a target comprised in a 3D line.
 * Code from Anthony Thyssen.
 * @param target End position of the line
 * @return List of Position on the 3D line.
 */
  vector<Position> line3d(const Position &target) const;

  bool operator<(const Position &rhs) const;

  bool operator>(const Position &rhs) const
  {
    return !(rhs < *this);
  }

  friend ostream &operator<<(ostream &os, const Position &position);

  void save(Store *store, bool subclass = false) const override;

  static Object *load(FILE *file)
  {
    return new Position(readInt(file), readInt(file), readInt(file));
  }
};

const Position _ERR_POS_ = Position(INT16_MAX, INT16_MAX, INT16_MAX);

#endif //HOMESICK_POSITION_H
