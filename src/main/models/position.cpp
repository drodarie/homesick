/**
 * @file position.cpp
 * @author drodarie
 * @date 01/08/17
 */

#include "position.h"


unsigned int Position::getManhattanDistance(Position position) const
{
  return static_cast<unsigned int>(abs(position.x_ - x_) + abs(position.y_ - y_) + abs(position.z_ - z_));
}

unsigned int Position::get2DManhattanDistance(Position position) const
{
  return static_cast<unsigned int>(abs(position.x_ - x_) + abs(position.y_ - y_));
}

double Position::getDistance(Position position) const
{
  return sqrt(pow(position.x_ - x_, 2) + pow(position.y_ - y_, 2) + pow(position.z_ - z_, 2));
}

double Position::get2DDistance(Position position) const
{
  return sqrt(pow(position.x_ - x_, 2) + pow(position.y_ - y_, 2));
}

bool Position::operator<(const Position &rhs) const
{
  if (x_ < rhs.x_)
    return true;
  if (rhs.x_ < x_)
    return false;
  if (y_ < rhs.y_)
    return true;
  if (rhs.y_ < y_)
    return false;
  return z_ < rhs.z_;
}

void Position::save(Store *store, bool subclass) const
{
  Object::save(store, subclass);
  *store << x_;
  *store << y_;
  *store << z_;
}

ostream &operator<<(ostream &os, const Position &position)
{
  os << "x: " << position.x_ << ", y: " << position.y_ << ", z: " << position.z_;
  return os;
}

vector<double> Position::norm() const noexcept(false)
{
  double len = length();
  if (len <= 0)
    throw ARGUMENT_ERROR("Position::norm: The vector size is null");
  return {x_ / len, y_ / len, z_ / len};
}

double Position::angle(const Position &position) const noexcept(false)
{
  try
  {
    vector<double> v1 = norm(), v2 = position.norm();
    return acos(dot(v1, v2));
  }
  catch (exception &e)
  {
    throw ARGUMENT_ERROR("Position::angle: One of the position length is null.\n" + string(e.what()));
  }
}

double Position::dot(vector<double> v1, vector<double> v2)
{
  double result = 0;
  unsigned long size = min(v1.size(), v2.size());
  for (unsigned long i = 0; i < size; i++)
    result += v1[i] * v2[i];
  return result;
}

int Position::isInField(const Position &target,
                        const Orientation &orientation, unsigned int radius, float angle) const
{
  double distance = getDistance(target);
  if (distance > radius)
    return 0;
  Position v1 = target - *this;
  auto angle_ = abs(v1.angle({orientation}));
  if (angle_ < abs(angle / 2) + 1e-6)
    return 2 - int(distance <= radius - 1);
  return 0;
}

vector<Position> Position::line3d(const Position &target) const
{
  vector<Position> result;
  int i, dx, dy, dz, l, m, n, x_inc, y_inc, z_inc,
    err_1, err_2, dx2, dy2, dz2;
  int pixel[3];

  pixel[0] = x_;
  pixel[1] = y_;
  pixel[2] = z_;
  dx = target.getX() - x_;
  dy = target.getY() - y_;
  dz = target.getZ() - z_;
  x_inc = (dx < 0) ? -1 : 1;
  l = abs(dx);
  y_inc = (dy < 0) ? -1 : 1;
  m = abs(dy);
  z_inc = (dz < 0) ? -1 : 1;
  n = abs(dz);
  dx2 = l << 1;
  dy2 = m << 1;
  dz2 = n << 1;

  if ((l >= m) && (l >= n))
  {
    err_1 = dy2 - l;
    err_2 = dz2 - l;
    for (i = 0; i < l; i++)
    {
      result.emplace_back(pixel);
      if (err_1 > 0)
      {
        pixel[1] += y_inc;
        err_1 -= dx2;
      }
      if (err_2 > 0)
      {
        pixel[2] += z_inc;
        err_2 -= dx2;
      }
      err_1 += dy2;
      err_2 += dz2;
      pixel[0] += x_inc;
    }
  } else if ((m >= l) && (m >= n))
  {
    err_1 = dx2 - m;
    err_2 = dz2 - m;
    for (i = 0; i < m; i++)
    {
      result.emplace_back(pixel);
      if (err_1 > 0)
      {
        pixel[0] += x_inc;
        err_1 -= dy2;
      }
      if (err_2 > 0)
      {
        pixel[2] += z_inc;
        err_2 -= dy2;
      }
      err_1 += dx2;
      err_2 += dz2;
      pixel[1] += y_inc;
    }
  } else
  {
    err_1 = dy2 - n;
    err_2 = dx2 - n;
    for (i = 0; i < n; i++)
    {
      result.emplace_back(pixel);
      if (err_1 > 0)
      {
        pixel[1] += y_inc;
        err_1 -= dz2;
      }
      if (err_2 > 0)
      {
        pixel[0] += x_inc;
        err_2 -= dz2;
      }
      err_1 += dy2;
      err_2 += dx2;
      pixel[2] += z_inc;
    }
  }
  result.emplace_back(pixel);
  return result;
}