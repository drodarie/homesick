/**
 * @file skill.h
 * @author drodarie
 * @date 27/07/17
 */

#ifndef HOMESICK_SKILL_H
#define HOMESICK_SKILL_H

#include <string>
#include <utility>
#include <algorithm>
#include "namedObject.h"
#include "../globals.h"

using namespace std;

/**
 * @brief Class to describe the aptitudes of the characters enabling them to perform actions
 */
class Skill : public NamedObject, public ObjectProxy<Skill>
{

public:
  /**
   * @brief Default constructor with all required parameters
   * @param id
   * @param name
   * @param description
   * @param filename path to file where the Skill will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  Skill(unsigned int id,
        const string &name,
        const string &description,
        const string &filename = ObjectProxy<Skill>::defaultFilename_,
        const string &nameClass = ObjectProxy<Skill>::className_) :
    NamedObject(id, filename, name, description, nameClass)
  {}

  /**
   * @brief Default constructor with all required parameters
   * @param name
   * @param description
   * @param filename path to file where the Skill will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  Skill(const string &name,
        const string &description,
        const string &filename = ObjectProxy<Skill>::defaultFilename_,
        const string &nameClass = ObjectProxy<Skill>::className_) :
    NamedObject(filename, name, description, nameClass)
  {}

  Skill(const Skill &skill) : NamedObject(skill)
  {}

  static Object *load(FILE *file)
  {
    auto id = static_cast<unsigned int>(readInt(file));
    auto filename = readString(file);
    return new Skill(id, readString(file), readString(file), filename);
  }
};

#endif //HOMESICK_SKILL_H
