/**
 * @file base.cpp
 * @author drodarie
 * @date 4/7/18.
*/

#include "base.h"

map<string, Object *(*)(FILE *)> &getFactory()
{
  static map<string, Object *(*)(FILE *)> factories;
  return factories;
}

void Factories::addFactory(const string &name, Object *(*factory)(FILE *))
{
  getFactory().emplace(name, factory);
}

map<string, Object *(*)(const Object *)> &getCopyFunction()
{
  static map<string, Object *(*)(const Object *)> copyFunctions;
  return copyFunctions;
}

void Factories::addCopyFunction(const string &name, Object *(*copyFunction)(const Object *))
{
  getCopyFunction().emplace(name, copyFunction);
}

map<string, vector<PObject>> &getDefaults()
{
  static map<string, vector<PObject>> defaults;
  return defaults;
}

void loadDefaults(const string &filename)
{
  if (getDefaults().find(filename) == getDefaults().end())
    try
    {
      vector<Object *> temp = Object::load_all(filename);
      vector<PObject> res(temp.size(), nullptr);
      for (unsigned int i = 0; i < temp.size(); i++)
        res[i] = PObject(temp[i]);
      getDefaults().emplace(filename, res);
    } catch (exception &e)
    {
      getDefaults().emplace(filename, vector<PObject>());
    }
}

/*********** Store ***************/

void Store::save(const string &filename) noexcept(false)
{
  FILE *pFile;
  pFile = fopen(filename.c_str(), "wb");
  if (pFile == nullptr) throw WRITING_FILE_ERROR("Store::save: Error while opening file " + filename);
  try
  {
    for (unsigned int i = 0; i < data_.size(); i++)
      fwrite(data_[i], types_size_[i], lengths_[i], pFile);
    fclose(pFile);
  }
  catch (exception &e)
  {
    throw WRITING_FILE_ERROR("Store::save: Error while writing into the file " + filename + ":\n" + e.what());
  }
}

void Store::operator<<(const bool &data)
{
  lengths_.push_back(1);
  types_size_.push_back(sizeof(bool));
  data_.push_back(malloc(sizeof(bool)));
  memcpy(data_[data_.size() - 1], &data, sizeof(bool));
}

void Store::operator<<(const int &data)
{
  lengths_.push_back(1);
  types_size_.push_back(sizeof(int));
  data_.push_back(malloc(sizeof(int)));
  memcpy(data_[data_.size() - 1], &data, sizeof(int));
}

void Store::operator<<(const float &data)
{
  lengths_.push_back(1);
  types_size_.push_back(sizeof(float));
  data_.push_back(malloc(sizeof(float)));
  memcpy(data_[data_.size() - 1], &data, sizeof(float));
}

void Store::operator<<(const string &data)
{
  lengths_.push_back(1);
  lengths_.push_back(static_cast<unsigned int>(data.size() + 1));
  types_size_.push_back(sizeof(unsigned int));
  types_size_.push_back(sizeof(char));
  data_.push_back(malloc(sizeof(unsigned int)));
  auto current_size = data_.size() - 1;
  memcpy(data_[current_size], &lengths_[current_size + 1], sizeof(unsigned int));
  data_.push_back(malloc(lengths_[current_size + 1]));
  memcpy(data_[current_size + 1], (void *) data.c_str(), lengths_[current_size + 1]);
}


/*********** Object **************/

vector<Object*> Object::loaded_ = vector<Object*>();

string Object::readString(FILE *pFile) noexcept(false)
{
  unsigned int N(0);
  char *result;
  string result2;
  try
  {
    fread(&N, sizeof(N), 1, pFile);
    result = (char *) malloc(sizeof(char) * N);
    fread(result, sizeof(char), N, pFile);
  }
  catch (const exception &e)
  {
    throw READING_FILE_ERROR(
      "Object::readString: Error while trying to read a string from the file:\n" + string(e.what()));
  }
  result2 = string(result);
  free(result);
  return result2;
}

int Object::readInt(FILE *pFile) noexcept(false)
{
  int result;
  try
  {
    fread(&result, sizeof(int), 1, pFile);
  }
  catch (const exception &e)
  {
    throw READING_FILE_ERROR(
      "Reader::readInt: Error while trying to read an integer from the file:\n" + string(e.what()));
  }
  return result;
}

bool Object::readBool(FILE *pFile) noexcept(false)
{
  bool result;
  try
  {
    fread(&result, sizeof(bool), 1, pFile);
  }
  catch (const exception &e)
  {
    throw READING_FILE_ERROR(
      "Reader::readInt: Error while trying to read a boolean from the file:\n" + string(e.what()));
  }
  return result;
}

float Object::readFloat(FILE *pFile) noexcept(false)
{
  float result;
  try
  {
    fread(&result, sizeof(float), 1, pFile);
  }
  catch (const exception &e)
  {
    throw READING_FILE_ERROR(
      "Reader::readFloat: Error while trying to read a float from the file:\n" + string(e.what()));
  }
  return result;
}

Object *Object::load(FILE *file) noexcept(false)
{
  auto className = readString(file);
  auto result = getFactory()[className](file);
  result->nameClass_ = className;
  return result;
}

Object *Object::getCopy(const Object *source)
{
  return getCopyFunction()[source->getNameClass()](source);
}

vector<Object *> Object::load_all(FILE *pFile) noexcept(false)
{
  auto size = readInt(pFile);
  vector<Object *> result(size);
  for (auto i = 0; i < size; i++)
  {
    result[i] = load(pFile);
    loaded_.push_back(result[i]);
  }
  return result;
}

vector<Object *> Object::load_all(const string &filename, bool clear) noexcept(false)
{
  FILE *pFile;
  pFile = fopen(filename.c_str(), "rb");
  if (pFile == nullptr) throw READING_FILE_ERROR("Object::load_all: Error while opening file " + filename);
  vector<Object *> result = load_all(pFile);
  fclose(pFile);
  if(clear)
  {
    for (auto& obj: loaded_)
    {
      bool found = false;
      for(auto& res: result)
        if(res==obj)
        {
          found = true;
          break;
        }
      if (!found)
        delete(obj);
    }
    loaded_.clear();
  }
  return result;
}
