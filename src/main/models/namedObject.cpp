/**
 * @file ${FILE}
 * @author drodarie
 * @date 10/2/19.
*/

#include "namedObject.h"

void NamedObject::updateDefaults() const
{
  bool found = false;
  if (getDefaults().find(filename_) != getDefaults().end() && !getDefaults()[filename_].empty())
  {
    for (auto &i : getDefaults()[filename_])
    {
      try
      {
        if (*dynamic_cast<NamedObject *>(i.get()) == *this && !dynamic_cast<NamedObject *>(i.get())->equals(*this))
        {
          i.reset(getCopy(this));
          found = true;
          break;
        }
      } catch (exception &e)
      {}
    }
  }
  if (!found)
  {
    getDefaults()[filename_].emplace_back(getCopy(this));
  }
}

void NamedObject::save(Store *store, bool subclass) const
{
  Object::save(store, subclass);
  *store << id_;
  *store << filename_;
  *store << name_;
  *store << description_;
  updateDefaults();
}

Object *NamedObject::load(FILE *file)
{
  return new NamedObject(static_cast<unsigned int>(readInt(file)), readString(file),
                         readString(file), readString(file));
}

void NamedObject::saveSignature(Store *store) const {
  *store << id_;
  *store << filename_;
  *store << nameClass_;
}
