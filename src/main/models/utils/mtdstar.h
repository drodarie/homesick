/**
 * @file mtdstar.h
 * @author drodarie
 * @date 7/26/18.
*/

#ifndef HOMESICK_DSTAR_H
#define HOMESICK_DSTAR_H

#include <limits>
#include <utility>
#include "../maps.h"

using namespace std;

typedef pair<float, float> Key;

/**
 * @brief Class to perform path-finding in a Map according to the Moving Target D* lite algorithm.
 * Will extract a graph of the Map based on the explorer size.
 * The cost of the movements will depend on the position blocker.
 */
class MTDstar
{
public:
  /**
   * @brief Class to store the node information of the path-finding graph.
   */
  class Node
  {
  public:
    float g_; //!< Approximation cost function from start to node
    float rhs_; //!< One step look-ahead g-value
    Position parent_; //!< Parent of the node
    vector<Position> predecessors_; //!< List of positions preceding the node
    vector<Position> successors_; //!< List of positions succeeding the node

    /**
     * @brief Basic Constructor for unexplored Node
     */
    Node() : g_(INT16_MAX), rhs_(INT16_MAX), parent_(_ERR_POS_)
    {}
  };

protected:
  vector<Position> positions_; //!< List of the positions of the graph
  map<Position, Node> nodes_; //!< List of the node of each graph position
  map<pair<Position, Position>, float> costs_; //!< List of the edges of the graph
  vector<pair<Position, Key>> openNodes_; //!< List of the remaining Nodes to test ordered by Key
  Position start_; //!< Initial position on the graph
  Position current_pos_; //!< Current position on the graph
  Position target_; //!< Target position on the graph
  Position original_target_; //!< Target position on the graph
  unsigned int km_; //!< Additional cost of moving the target
  vector<Position> path_; //!< Computed path to reach the target from the start
  float basicCost_; //!< Basic cost of each edge of the graph
  bool recompute_; //!< Flag to recompute the path

  /**
   * @brief Compute the Key of the position.
   * @param position
   * @return Key or pair of the heuristic value and the cost-value
   */
  Key computeKey(const Position &position);

  /**
   * @brief Place a key link to a position in the opened node list.
   * The opened node list is ordered by Key
   * @param position
   * @param key
   */
  void updateKey(const Position &position, Key key);

  /**
   * @brief Update the state of a position.
   * Recompute and replace a position in the opened nodes if need be.
   * @param position
   */
  void updateState(const Position &position);

  /**
   * @brief Update the predecessor of a position in the graph to get the one with minimal cost
   * @param position
   */
  void updateNodePred(const Position &position);

  /**
   * @brief Explore the graph from start to reach target.
   * Compute a key for each position crossed.
   */
  void computePath();

public:
  /**
   * @brief Basic constructor
   * @param start Start Position in the Map or the closest in
   * @param target Target Position in the Map or the closest in
   * @param height Minimal height of the actor exploring the graph
   * @param area Map to explore, will be converted to a graph
   * @param basic_cost Cost of Moving from one position to the adjacent one. Minimum 0
   */
  MTDstar(const Position &start, const Position &target,
          const unsigned int height, const Map &area, const float basic_cost = 1) :
    km_(0), recompute_(true)
  {
    basicCost_ = basic_cost;
    if (basicCost_ < 0)
      basicCost_ = 0;
    openNodes_ = vector<pair<Position, Key>>();
    positions_ = area.getFloor(height);
    start_ = findCloserPosition(start);
    original_target_ = target;
    target_ = findCloserPosition(target);
    current_pos_ = start_;
    nodes_ = map<Position, Node>();
    for (auto &position: positions_)
    {
      auto blocker = area.getBlocker(position);
      nodes_[position] = Node();
      auto node = &nodes_[position];

      for (auto &pos: positions_)
        if (pos.getDistance(position) <= 1 && position != pos)
        {
          node->successors_.push_back(pos);
          costs_[{position, pos}] = basicCost_ + float(pow(int(blocker), 2));
        }
      node->predecessors_ = vector<Position>(node->successors_.size());
      std::copy(node->successors_.begin(), node->successors_.end(), node->predecessors_.begin());
    }
    if (!positions_.empty())
    {
      nodes_[start_].rhs_ = 0;
      openNodes_.emplace_back(pair<Position, Key>(start_, computeKey(start_)));
    }
  }

  const map<Position, Node> &getNodes() const
  { return nodes_; }

  const vector<Position> &getPositions() const
  { return positions_; }

  const Position &getStart() const
  { return start_; }

  const Position &getCurrentPosition() const
  { return current_pos_; }

  const Position &getTarget() const
  { return target_; }

  unsigned int getKm() const
  { return km_; }

  float getBasicCost() const
  { return basicCost_; }

  const vector<Position> &getPath() const
  { return path_; }

  /**
   * @brief Get the cost to move from a position to the other.
   * @param parent
   * @param child
   * @return float cost to move or INT16_MAX
   */
  const float getCost(const Position &parent, const Position &child);

  /**
   * @brief Retrieve the closest position on the graph of a position
   * @param start
   * @return
   */
  Position findCloserPosition(const Position &start);

  /**
   * @brief Retrieve the children of the position according to the current exploration
   * @param start
   * @return
   */
  vector<Position> getChildren(const Position &start);

  void setStart(const Position &start);

  void setTarget(const Position &target);

  void setBasicCost(float basicCost);

  /**
   * @brief Add value to the cost between two position.
   * Minimum value is 1.
   * @param source
   * @param target
   * @param cost
   */
  void addCost(const Position &source, const Position &target, const float &cost);

  /**
   * @brief Add value to the cost between every position of the map where an sized actor can stand.
   * minimum value is 1.
   * @param height
   * @param area
   * @param cost
   */
  void addCost(unsigned int height, const Map &area, const float &cost);

  /**
   * @brief Computes the best path to move from start to target.
   */
  void computeBestPath();

  /**
   * @brief Update the path from start to target according to updated parameters.
   * @param height
   * @param area
   */
  void update(unsigned int height, const Map &area);
};


#endif //HOMESICK_DSTAR_H
