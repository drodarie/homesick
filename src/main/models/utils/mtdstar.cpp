/**
 * @file dstar.cpp
 * @author drodarie
 * @date 7/26/18.
*/

#include "mtdstar.h"

Key MTDstar::computeKey(const Position &position)
{
  float gstar = min(nodes_[position].g_, nodes_[position].rhs_);
  return {min(float(INT16_MAX), gstar + float(position.getManhattanDistance(target_)) + km_), gstar};
}

void MTDstar::updateKey(const Position &position, Key key)
{
  openNodes_.erase(remove_if(openNodes_.begin(), openNodes_.end(),
                             [&](pair<Position, Key> n)
                             { return n.first == position; }), openNodes_.end());
  vector<pair<Position, Key>>::iterator it;
  for (it = openNodes_.begin(); it != openNodes_.end(); it++)
    if (key.first < it.operator*().second.first
        || (key.first == it.operator*().second.first && key.second < it.operator*().second.second))
      break;
  openNodes_.insert(it, {position, key});
}

void MTDstar::updateState(const Position &position)
{
  if (nodes_[position].g_ != nodes_[position].rhs_)
    updateKey(position, computeKey(position));
  else
    openNodes_.erase(remove_if(openNodes_.begin(), openNodes_.end(),
                               [&](pair<Position, Key> n)
                               { return n.first == position; }), openNodes_.end());
}

void MTDstar::updateNodePred(const Position &position)
{
  auto node = &nodes_[position];
  unsigned int min_ = 0;
  node->rhs_ = float(INT16_MAX);
  for (unsigned int j = 0; j < node->predecessors_.size(); j++)
  {
    auto rhs = nodes_[node->predecessors_[j]].g_ + costs_[{node->predecessors_[j], position}];
    if (rhs < node->rhs_)
    {
      node->rhs_ = rhs;
      min_ = j;
    }
  }
  if (node->rhs_ >= INT16_MAX)
    node->parent_ = _ERR_POS_;
  else
    node->parent_ = node->predecessors_[min_];
  updateState(position);
}

void MTDstar::computePath()
{
  while (!openNodes_.empty() &&
         (openNodes_[0].second < computeKey(target_) || nodes_[target_].rhs_ > nodes_[target_].g_))
  {
    auto currentPosition = openNodes_[0].first;
    auto currentNode = &nodes_[currentPosition];
    auto oldKey = openNodes_[0].second, newKey = computeKey(openNodes_[0].first);
    if (oldKey < newKey)
      updateKey(currentPosition, newKey);
    else if (currentNode->g_ > currentNode->rhs_)
    {
      currentNode->g_ = currentNode->rhs_;
      openNodes_.erase(openNodes_.begin(), openNodes_.begin() + 1);
      for (unsigned int i = 0; i < currentNode->successors_.size(); i++)
      {
        auto succ = currentNode->successors_[i];
        auto cost = costs_[{currentPosition, succ}];
        if (succ != start_ && nodes_[succ].rhs_ > currentNode->g_ + cost)
        {
          nodes_[succ].parent_ = currentPosition;
          nodes_[succ].rhs_ = currentNode->g_ + cost;
          updateState(succ);
        }
      }
    } else
    {
      currentNode->g_ = INT16_MAX;
      for (const auto &succ : currentNode->successors_)
        if (succ != start_ && nodes_[succ].parent_ == currentPosition)
          updateNodePred(succ);
      updateState(currentPosition);
    }
  }
}

Position MTDstar::findCloserPosition(const Position &start)
{
  Position result = _ERR_POS_;
  if (find(positions_.begin(), positions_.end(), start) != positions_.end())
    result = start;
  else
  {
    auto currentDist = double(INT16_MAX);
    for (auto &position: positions_) // Return the closest position reachable
    {
      auto dist = position.getDistance(start);
      if (nodes_[position].rhs_ <= INT16_MAX && currentDist > dist)
      {
        currentDist = dist;
        result = position;
      }
    }
  }
  return result;
}

vector<Position> MTDstar::getChildren(const Position &start)
{
  vector<Position> result(1, start);
  if (nodes_.find(start) != nodes_.end())
    for (const auto &succ: nodes_[start].successors_)
      if (nodes_[succ].parent_ == start)
      {
        auto children = getChildren(succ);
        result.insert(result.end(), children.begin(), children.end());
      }
  return result;
}

void MTDstar::setStart(const Position &start)
{
  auto best = findCloserPosition(start);
  if (best != start_)
  {
    recompute_ = true;
    start_ = best;
    current_pos_ = start_;
    if (best != _ERR_POS_)
    {
      nodes_[start_].parent_ = _ERR_POS_;
      auto children = getChildren(start_);
      vector<Position> deleted(positions_.size() - children.size());
      unsigned int i = 0;
      for (const auto &position: positions_)
        if (find(children.begin(), children.end(), position) == children.end())
        {
          nodes_[position].parent_ = _ERR_POS_;
          nodes_[position].g_ = INT16_MAX;
          deleted[i] = position;
          i++;
          openNodes_.erase(remove_if(openNodes_.begin(), openNodes_.end(),
                                     [&](pair<Position, Key> n)
                                     { return n.first == position; }), openNodes_.end());
        }
      for (const auto &position: deleted)
        updateNodePred(position);
    }
  }
}

void MTDstar::setTarget(const Position &target)
{
  original_target_ = target;
  auto best = findCloserPosition(target);
  if (best != target_)
  {
    recompute_ = true;
    km_ += target_.getManhattanDistance(best);
    target_ = best;
  }
}

void MTDstar::setBasicCost(float basicCost)
{
  auto best = basicCost;
  if (best < 0)
    best = 0;
  if (best != basicCost_)
  {
    basicCost_ = best;
    for (auto edge: costs_)
      addCost(edge.first.first, edge.first.second, best - basicCost_);
  }
}

const float MTDstar::getCost(const Position &parent, const Position &child)
{
  if (find(positions_.begin(), positions_.end(), parent) != positions_.end() &&
      find(positions_.begin(), positions_.end(), child) != positions_.end())
  {
    return costs_[{parent, child}];
  }
  return INT16_MAX;
}

void MTDstar::addCost(const Position &source, const Position &target, const float &cost)
{
  if (find(positions_.begin(), positions_.end(), source) != positions_.end() &&
      find(positions_.begin(), positions_.end(), target) != positions_.end() && cost != 0)
  {
    auto node = &nodes_[source];
    auto old_cost = costs_[{source, target}];
    auto local_cost = old_cost + cost;
    if (local_cost < 0)
      local_cost = 0;
    costs_[{source, target}] = local_cost;

    if (old_cost > local_cost)
    {
      recompute_ = true;
      if (target != start_ && nodes_[target].rhs_ > node->g_ + local_cost)
      {
        nodes_[target].parent_ = source;
        nodes_[target].rhs_ = node->g_ + local_cost;
        updateState(target);
      }
    } else if (old_cost < local_cost)
    {
      recompute_ = true;
      if (target != start_ && nodes_[target].parent_ == source)
        updateNodePred(target);
    }
  }
}

void MTDstar::addCost(unsigned int height, const Map &area, const float &cost)
{
  auto positions = area.getFloor(height);
  for (const auto &position: positions)
    for (const auto &successor : nodes_[position].successors_)
      addCost(position, successor, cost);
}

void MTDstar::computeBestPath()
{
  path_ = vector<Position>();
  if (start_ != target_)
  {
    computePath();
    auto current = target_;
    if (nodes_[current].parent_ != _ERR_POS_)
      while (current != _ERR_POS_ && current != start_)
      {
        path_.push_back(current);
        current = nodes_[current].parent_;
      }
  }
  recompute_ = false;
}

void MTDstar::update(unsigned int height, const Map &area)
{
  positions_ = area.getFloor(height);

  for (auto it = nodes_.begin(); it != nodes_.end();) //Check Position deleted
  {
    auto current = it.operator*();
    if (find(positions_.begin(), positions_.end(), current.first) == positions_.end())
    {
      recompute_ = true;
      auto node = current.second;
      for (const auto &succ: node.successors_)
      {
        auto temp = &nodes_[succ];
        temp->predecessors_.erase(remove_if(temp->predecessors_.begin(), temp->predecessors_.end(),
                                            [&](Position pos)
                                            { return pos == current.first; }),
                                  temp->predecessors_.end());
        if (temp->parent_ == current.first)
          temp->parent_ = _ERR_POS_;
      }
      for (auto succ: node.predecessors_)
      {
        auto temp = &nodes_[succ];
        temp->successors_.erase(remove_if(temp->successors_.begin(), temp->successors_.end(),
                                          [&](Position pos)
                                          { return pos == current.first; }),
                                temp->successors_.end());
        costs_.erase(costs_.find({succ, current.first}));
      }
      openNodes_.erase(remove_if(openNodes_.begin(), openNodes_.end(),
                                 [&](pair<Position, Key> pos)
                                 { return pos.first == current.first; }),
                       openNodes_.end());
      it = nodes_.erase(it);
    } else
      it++;
  }

  for (auto &position: positions_)
  {
    if (nodes_.find(position) == nodes_.end()) // New Position
    {
      recompute_ = true;
      auto blocker = area.getBlocker(position);
      auto cost = basicCost_ + float(pow(int(blocker), 2));
      nodes_[position] = Node();
      auto node = &nodes_[position];
      for (auto &pos: positions_)
        if (pos.getDistance(position) <= 1 && position != pos)
        {
          node->successors_.push_back(pos);
          costs_[{position, pos}] = cost;
          nodes_[pos].successors_.push_back(position);
          nodes_[pos].predecessors_.push_back(position);
          costs_[{pos, position}] = cost;
        }
      node->predecessors_ = vector<Position>(node->successors_.size());
      std::copy(node->successors_.begin(), node->successors_.end(), node->predecessors_.begin());
    }
  }
  if (recompute_ || path_.empty())
  {
    setTarget(original_target_);
    setStart(current_pos_);
    computeBestPath();
  }
  if (!path_.empty() && current_pos_ != target_)
  {
    current_pos_ = path_.back();
    path_.pop_back();
  }
}
