/**
 * @file item.h
 * @author drodarie
 * @date 30/07/17
 */

#ifndef HOMESICK_ITEM_H
#define HOMESICK_ITEM_H


#include <map>
#include <set>
#include <utility>
#include <algorithm>
#include <ostream>
#include <cassert>
#include "../entity.h"

/**
 * @brief Class describing an abstract Item in Homesick
 */

class Item : public Entity, public ObjectProxy<Item>
{
protected:
  float value_ = 0; //!< price value of the item
  float damage_ = 0; //!< sustained damage of the item
  map<Item *, unsigned int> content_; //!< Content of the Item.

public:
  /**
   * @brief Default constructor with all required parameters
   * @param id
   * @param name
   * @param description
   * @param mass in kg
   * @param size in meters
   * @param position initial position of the item on the map
   * @param block_position occupation of its position by the Item
   * @param value default price value of the item
   * @param damage initial sustained damage on the item
   * @param filename path to file where the Item will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  Item(unsigned int id,
       const string &name,
       const string &description,
       float mass = 0,
       float size = 2,
       const Position &position = {0, 0, 0},
       Blocker block_position = Blocker::None,
       float value = 0,
       float damage = 0,
       const string &filename = ObjectProxy<Item>::defaultFilename_,
       const string &nameClass = ObjectProxy<Item>::className_) :
    Entity(id, name, description, mass, size, position, block_position, filename, nameClass),
    value_(value),
    damage_(damage),
    content_(map<Item *, unsigned int>())
  {
  }

  /**
   * @brief Default constructor with all required parameters
   * @param name
   * @param description
   * @param mass in kg
   * @param size in meters
   * @param position initial position of the item on the map
   * @param block_position occupation of its position by the Item
   * @param value default price value of the item
   * @param damage initial sustained damage on the item
   * @param filename path to file where the Item will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  Item(const string &name,
       const string &description,
       float mass = 0,
       float size = 2,
       const Position &position = {0, 0, 0},
       Blocker block_position = Blocker::None,
       float value = 0,
       float damage = 0,
       const string &filename = ObjectProxy<Item>::defaultFilename_,
       const string &nameClass = ObjectProxy<Item>::className_) :
    Entity(name, description, mass, size, position, block_position, filename, nameClass),
    value_(value),
    damage_(damage),
    content_(map<Item *, unsigned int>())
  {
  }

  Item(const Item &item) : Entity(item), value_(item.value_), damage_(item.damage_)
  {
    item.copyContent(this);
  }

  ~Item() override
  {
    if (!content_.empty())
      for (auto &i: content_)
        delete (i.first);
  }

  Item &operator=(const Item &item)
  {
    Entity::operator=(item);
    value_ = item.value_;
    damage_ = item.damage_;
    if (!content_.empty())
      for (auto &i: content_)
        delete (i.first);

    for (const auto &i:item.content_)
      content_[dynamic_cast<Item *>(getCopy(i.first))] = i.second;
    return *this;
  }

  float getValue() const
  { return value_; }

  void setValue(float value)
  { value_ = value; }

  float getDamage() const
  { return damage_; }

  void setDamage(float damage)
  { damage_ = damage; }

  bool hasContent(Item *item) const;

  map<Item *, unsigned int> getContent() const
  { return content_; }

  vector<Item *> getContentList() const;

  unsigned int getNumberContent(Item *item);

  /**
   * @brief Setter for a specific content of the item
   * @param item Item contained in the the current Item.
   * @param quantity number of iten to add or retrieve
   * @return Number of items added or retrieved from the class
   */
  unsigned int setContent(Item *item, int quantity);

  /**
   * @brief Get the total value of an item, summing with  its potential content.
   * @return Total value of an item
   */
  float getTotalValue() const;

  /**
   * @brief Get the total mass of an item, summing with its potential content.
   * @return Total mass of the Item
   */
  float getTotalMass() const;

  bool isQuestItem() const
  {
    return damage_ < 0;
  }

  bool equals(const Item &rhs) const;

  friend ostream &operator<<(ostream &os, const Item &part);

  void copyContent(Item *destination) const;

  void save(Store *writer, bool subclass = false) const override;

  static Object *load(FILE *file);
};


#endif //HOMESICK_ITEM_H
