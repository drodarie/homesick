/**
 * @file stuff.cpp
 * @author drodarie
 * @date 01/08/17
 */

#include "stuff.h"

void Equipment::save(Store *writer, bool subclass) const
{
  Item::save(writer, subclass);
  *writer << static_cast<int>(body_);
  *writer << protection_;
}

Object *Equipment::load(FILE *file)
{
  auto item = dynamic_cast<Item *>(Item::load(file));
  auto result = new Equipment(item->getId(), item->getName(), item->getDescription(),
                              item->getMass(), item->getSize(), item->getPosition(), item->getBlocker(),
                              item->getValue(), item->getDamage(), BodyPart(readInt(file)), readFloat(file),
                              item->getFilename());
  item->copyContent(result);
  delete (item);
  return result;
}

ostream &operator<<(ostream &os, const Equipment &equipment)
{
  os << static_cast<const Item &>(equipment) << ",\nbody: " << int(equipment.body_) <<
     ", protection: " << equipment.protection_;
  return os;
}

void Weapon::save(Store *writer, bool subclass) const
{
  Equipment::save(writer, subclass);
  *writer << scope_;
}

Object *Weapon::load(FILE *file)
{
  auto item = dynamic_cast<Equipment *>(Equipment::load(file));
  auto result = new Weapon(item->getId(), item->getName(), item->getDescription(),
                           item->getMass(), item->getSize(), item->getPosition(), item->getBlocker(),
                           item->getValue(), item->getDamage(), item->getBodyPart(), item->getProtection(),
                           static_cast<unsigned int>(readInt(file)),
                           item->getFilename());
  item->copyContent(result);
  delete (item);
  return result;
}

ostream &operator<<(ostream &os, const Weapon &weapon)
{
  os << static_cast<const Equipment &>(weapon) << ", scope: "
     << weapon.scope_;
  return os;
}

void Potion::save(Store *writer, bool subclass) const
{
  Item::save(writer, subclass);
  effect_.save(writer, true);
}

Object *Potion::load(FILE *file)
{
  auto item = dynamic_cast<Item *>(Item::load(file));
  auto buff = dynamic_cast<Buff *>(Buff::load(file));
  auto result = new Potion(item->getId(), item->getName(), item->getDescription(),
                           item->getMass(), item->getSize(), *buff, item->getPosition(), item->getBlocker(),
                           item->getValue(), item->getDamage(), item->getFilename());
  item->copyContent(result);
  delete (item);
  delete (buff);
  return result;
}

ostream &operator<<(ostream &os, const Potion &potion)
{
  os << static_cast<const Item &>(potion) << ",\neffect: {"
     << potion.effect_ << "}";
  return os;
}
