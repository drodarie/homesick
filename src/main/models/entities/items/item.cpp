/**
 * @file action.cpp
 * @author drodarie
 * @date 11/08/17
 */

#include "item.h"

void Item::save(Store *store, bool subclass) const
{
  Entity::save(store, subclass);
  *store << value_;
  *store << damage_;
  *store << static_cast<int>(content_.size());
  for (auto &part: content_)
  {
    part.first->saveSignature(store);
    *store << part.second;
  }
}

void Item::copyContent(Item *destination) const
{
  for (const auto &i:content_)
    destination->content_[dynamic_cast<Item *>(getCopy(i.first))] = i.second;
}

unsigned int Item::setContent(Item *content, int quantity)
{
  if (!content_.empty())
    for (auto i = content_.begin(); i != content_.end(); i++)
      if (*i.operator*().first == *content)
      {
        if (quantity < 0 && i.operator*().second <= static_cast<unsigned int> (-quantity))
        { // Removing too much
          unsigned int result = i.operator*().second;
          delete (i.operator*().first);
          content_.erase(i);
          return result;
        }
        i.operator*().second += quantity;
        return static_cast<unsigned int>(labs(quantity)); // Return quantity removed or added
      }
  if (quantity > 0)
  {
    content_[dynamic_cast<Item *>(getCopy(content))] = static_cast<unsigned int>(quantity);
    return static_cast<unsigned int>(quantity);
  }
  return 0; // Don't have item
}

unsigned int Item::getNumberContent(Item *item)
{
  for (auto &i:content_)
    if (*i.first == *item)
      return i.second;
  return 0;
}

bool Item::hasContent(Item *item) const
{
  for (auto &i:content_)
    if (*i.first == *item)
      return true;
  return false;
}

ostream &operator<<(ostream &os, const Item &part)
{
  os << static_cast<const Entity &>(part) << ", value: " << part.value_ << ", damage: " << part.damage_ << ",";
  os << "\ncontent: {";
  for (auto &i: part.content_)
    os << "\n\t{" << *i.first << "}: " << i.second << ",";
  os << "}";
  return os;
}

vector<Item *> Item::getContentList() const
{
  vector<Item *> result;
  for (auto &i: content_)
  {
    unsigned int j = 0;
    while (j < result.size() && (result.begin() + j).operator*()->getId() < i.first->getId())
    { j++; }
    result.insert(result.begin() + j, i.first);
  }
  return result;
}

float Item::getTotalValue() const
{
  float result = value_;
  for (auto content: content_)
    result += content.first->value_ * content.second;
  return result;
}

float Item::getTotalMass() const
{
  float result = mass_;
  for (auto content: content_)
    result += content.first->mass_ * content.second;
  return result;
}

Object *Item::load(FILE *file)
{
  auto entity = load_T<Entity>(file);
  auto *item = new Item(entity->getId(), entity->getName(), entity->getDescription(), entity->getMass(),
                        entity->getSize(), entity->getPosition(), entity->getBlocker(), readFloat(file),
                        readFloat(file), entity->getFilename());
  auto size = static_cast<unsigned int>(readInt(file));
  for (unsigned int i = 0; i < size; i++)
  {
    auto content = dynamic_cast<Item *>(loadSignature(file));
    item->content_[content] = static_cast<unsigned int>(readInt(file));
  }
  delete (entity);
  return item;
}

bool Item::equals(const Item &rhs) const
{
  if (!(Entity::equals(rhs) && abs(value_ - rhs.value_) <= 1e-6 && abs(damage_ - rhs.damage_) <= 1e-6
        && content_.size() == rhs.content_.size()))
    return false;
  for (auto i: content_)
  {
    auto test = false;
    for (auto j: rhs.content_)
      if (i.first->equals(*j.first) && i.second == j.second)
      {
        test = true;
        break;
      }
    if (!test) return false;
  }
  return true;
}
