/**
 * @file stuff.h
 * @author drodarie
 * @date 27/07/17
 */

#ifndef HOMESICK_STUFF_H
#define HOMESICK_STUFF_H

#include <string>
#include <map>
#include <cassert>
#include <utility>
#include <ostream>
#include "item.h"
#include "../../buff.h"

using namespace std;

/**
 * @brief Enum to describe the body parts for Equipment.
 */
enum class BodyPart : int
{
  Head = 0, //!< Helmet or crown
  Neck, //!< Specifically for collars
  Torso, //!< Clothes, or extra-protection.
  Back, //!< Specifically for bags
  Hands, //!< For gloves
  RightHand, //!< Right hand jewel
  LeftHand, //!< Left hand jewel
  Belt, //!< Belt, pockets or jewel
  Legs, //!< Clothes, or extra-protection.
  Feet, //!< Clothes, or extra-protection.
  None //!< Undefined or no body specific equipment
};

/**
 * @brief Class to describe equipment in Homesick.
 * Equipment is usually associated with a BodyPart.
 * Can be used as a Decorator of Item.
 */
class Equipment : public Item, public ObjectProxy<Equipment>
{

protected:
  BodyPart body_ = BodyPart::None; //!< Body part associated with the equipment
  float protection_; //!< Effective protection provided by the item in case of damage

public:
  /**
   * @brief Default constructor with all required parameters
   * @param id
   * @param name
   * @param description
   * @param mass
   * @param size
   * @param position initial position on the map
   * @param block_position occupation of its position by the Equipment
   * @param value default price value of the item
   * @param damage initial sustained damage on the item
   * @param body BodyPart associated to the Equipment
   * @param protection Effective protection provided by the item in case of damage.
   * @param filename path to file where the Equipment will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  Equipment(unsigned int id,
            const string &name,
            const string &description,
            float mass,
            float size,
            const Position &position = {0, 0, 0},
            Blocker block_position = Blocker::None,
            float value = 0,
            float damage = 0,
            BodyPart body = BodyPart::None, float protection = 0,
            const string &filename = ObjectProxy<Equipment>::defaultFilename_,
            const string &nameClass = ObjectProxy<Equipment>::className_) :
    Item(id, name, description, mass, size, position, block_position, value, damage, filename, nameClass),
    body_(body), protection_(protection)
  {}

  /**
   * @brief Default constructor with all required parameters
   * @param name
   * @param description
   * @param mass
   * @param size
   * @param position initial position on the map
   * @param block_position occupation of its position by the Equipment
   * @param value default price value of the item
   * @param damage initial sustained damage on the item
   * @param body BodyPart associated to the Equipment
   * @param protection Effective protection provided by the item in case of damage.
   * @param filename path to file where the Equipment will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  Equipment(const string &name,
            const string &description,
            float mass,
            float size,
            const Position &position = {0, 0, 0},
            Blocker block_position = Blocker::None,
            float value = 0,
            float damage = 0,
            BodyPart body = BodyPart::None, float protection = 0,
            const string &filename = ObjectProxy<Equipment>::defaultFilename_,
            const string &nameClass = ObjectProxy<Equipment>::className_) :
    Item(name, description, mass, size, position, block_position, value, damage, filename, nameClass),
    body_(body), protection_(protection)
  {}

  Equipment(const Equipment &equipment) : Item(equipment), body_(equipment.body_), protection_(equipment.protection_)
  {}

  BodyPart getBodyPart() const
  {
    return body_;
  }

  void setBodyPart(BodyPart part)
  {
    Equipment::body_ = part;
  }

  float getProtection() const
  { return protection_; }

  void setProtection(float protection)
  { protection_ = protection; }

  /**
   * @brief Extract the Buff associated to the equipment
   * @return  Buff associated to the equipment
   */
  Buff getBuff()
  { return {0, 0, 0, 0, 0, 0, static_cast<int>(protection_), 0}; }

  bool equals(const Equipment &rhs) const
  {
    return Item::equals(rhs) && body_ == rhs.body_ && abs(protection_ - rhs.protection_) <= 1e-6;
  }

  friend ostream &operator<<(ostream &os, const Equipment &equipment);

  void save(Store *writer, bool subclass = false) const override;

  static Object *load(FILE *file);
};

/**
 * @brief Class to describe Weapons in Homesick. Can be used as a Decorator of Item
 */
class Weapon : public Equipment, public ObjectProxy<Weapon>
{
private:
  unsigned int scope_; //!< Maximal range reachable by the weapon
public:
  /**
   * @brief Default constructor with all required parameters
   * @param id
   * @param name
   * @param description
   * @param mass
   * @param size
   * @param position initial position on the map
   * @param block_position occupation of its position by the weapon
   * @param value default price value of the item
   * @param damage initial sustained damage on the item
   * @param body BodyPart associated to the Equipment
   * @param protection Effective protection provided by the item in case of damage.
   * @param scope Maximal range reachable by the weapon
   * @param filename path to file where the Weapon will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  Weapon(unsigned int id,
         const string &name,
         const string &description,
         float mass,
         float size,
         const Position &position = {0, 0, 0},
         Blocker block_position = Blocker::None,
         float value = 0,
         float damage = 0,
         BodyPart body = BodyPart::RightHand, float protection = 0,
         unsigned int scope = 1,
         const string &filename = ObjectProxy<Weapon>::defaultFilename_,
         const string &nameClass = ObjectProxy<Weapon>::className_) :
    Equipment(id, name, description, mass, size, position, block_position, value, damage, body, protection, filename,
              nameClass),
    scope_(scope)
  {}

  /**
   * @brief Default constructor with all required parameters
   * @param name
   * @param description
   * @param mass
   * @param size
   * @param position initial position on the map
   * @param block_position occupation of its position by the weapon
   * @param value default price value of the item
   * @param damage initial sustained damage on the item
   * @param body BodyPart associated to the Equipment
   * @param protection Effective protection provided by the item in case of damage.
   * @param scope Maximal range reachable by the weapon
   * @param filename path to file where the Weapon will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  Weapon(const string &name,
         const string &description,
         float mass,
         float size,
         const Position &position = {0, 0, 0},
         Blocker block_position = Blocker::None,
         float value = 0,
         float damage = 0,
         BodyPart body = BodyPart::RightHand, float protection = 0,
         unsigned int scope = 1,
         const string &filename = ObjectProxy<Weapon>::defaultFilename_,
         const string &nameClass = ObjectProxy<Weapon>::className_) :
    Equipment(name, description, mass, size, position, block_position, value, damage, body, protection, filename,
              nameClass),
    scope_(scope)
  {}

  Weapon(const Weapon &weapon) : Equipment(weapon), scope_(weapon.scope_)
  {}

  unsigned int getScope() const
  { return scope_; }

  void setScope(unsigned int scope)
  { scope_ = scope; }

  bool equals(const Weapon &rhs) const
  {
    return Equipment::equals(rhs) && scope_ == rhs.scope_;
  }

  friend ostream &operator<<(ostream &os, const Weapon &weapon);

  void save(Store *writer, bool subclass = false) const override;

  static Object *load(FILE *file);
};

/**
 * @brief Class to describe Potion items or anything providing a buff to the Character holding it.
 * Can be used as a Decorator of Item.
 */
class Potion : public Item, public ObjectProxy<Potion>
{

private:
  Buff effect_; //!< Effect of the item on a Character.
public:
  /**
   * @brief Default constructor
   * @param id
   * @param name
   * @param description
   * @param mass
   * @param size
   * @param effect Buff provided by the item
   * @param position initial position on the map
   * @param block_position occupation of its position by the potion
   * @param value default price value of the item
   * @param damage initial sustained damage on the item
   * @param filename path to file where the Potion will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  Potion(unsigned int id, const string &name, const string &description,
         float mass, float size, const Buff &effect,
         const Position &position = {0, 0, 0},
         Blocker block_position = Blocker::None, float value = 0, float damage = 0,
         const string &filename = ObjectProxy<Potion>::defaultFilename_,
         const string &nameClass = ObjectProxy<Potion>::className_
  ) :
    Item(id, name, description, mass, size, position, block_position, value, damage, filename, nameClass),
    effect_({effect})
  {}

  /**
   * @brief Default constructor
   * @param name
   * @param description
   * @param mass
   * @param size
   * @param effect Buff provided by the item
   * @param position initial position on the map
   * @param block_position occupation of its position by the potion
   * @param value default price value of the item
   * @param damage initial sustained damage on the item
   * @param filename path to file where the Potion will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  Potion(const string &name, const string &description,
         float mass, float size, const Buff &effect,
         const Position &position = {0, 0, 0},
         Blocker block_position = Blocker::None, float value = 0, float damage = 0,
         const string &filename = ObjectProxy<Potion>::defaultFilename_,
         const string &nameClass = ObjectProxy<Potion>::className_
  ) :
    Item(name, description, mass, size, position, block_position, value, damage, filename, nameClass),
    effect_({effect})
  {}

  Potion(const Potion &potion) : Item(potion), effect_(potion.effect_)
  {}

  Buff getEffect() const
  { return effect_; }

  void setEffect(const Buff &effect)
  { effect_ = {effect}; }

  bool equals(const Potion &rhs) const
  {
    return Item::equals(rhs) && effect_ == rhs.effect_;
  }

  friend ostream &operator<<(ostream &os, const Potion &potion);

  void save(Store *writer, bool subclass = false) const override;

  static Object *load(FILE *file);
};

#endif //HOMESICK_STUFF_H
