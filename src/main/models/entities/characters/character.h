/**
 * @file character.h
 * @author drodarie
 * @date 27/07/17
 * */

#ifndef HOMESICK_CHARACTER_H
#define HOMESICK_CHARACTER_H

#include <ostream>
#include "../items/stuff.h"
#include "../../skill.h"
#include "../../maps.h"

using namespace std;

/**
 * @brief Enum to describe the sex of a Character
 */
enum class Sex : int
{
  Male = 0,
  Female,
  Unknown
};

/**
 * @brief Abstract Class to describe a Character in Homesick
 */
class Character : public Entity, public ObjectProxy<Character>
{
protected:
  Sex sex_ = Sex::Unknown; //!< Sex of the Character
  NamedObject job_; //!< Job in the society

  Buff characteristics_; //!< Basic characteristics

  map<Item *, unsigned int> inventory_; //!< Inventory hold by the character.
  map<BodyPart, Equipment *> equipment_; //!< Equipment wear by the character
  vector<NamedObject *> properties_;//!< List of properties of the character. Could be Map or Item

  vector<Buff> buffs_; //!< Current buffs accorded to the character
  set<Skill> skills_; //!< List of skills developed by the character
  unsigned int experience_; //!< Experience gained by the character

public:
  /**
   * @brief Default constructor with all required parameters
   * @param id
   * @param name
   * @param description
   * @param mass
   * @param size
   * @param sex
   * @param job role in society
   * @param characteristics initial characteristics of the Character
   * @param position initial position of the Character on the map
   * @param block_position occupation of its position by the Character
   * @param experience initial experience of the character, define level
   * @param filename path to file where the Character will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  Character(unsigned int id,
            const string &name,
            const string &description,
            float mass,
            float size,
            Sex sex,
            const NamedObject &job,
            const Buff &characteristics,
            const Position &position = {0, 0, 0},
            Blocker block_position = Blocker::Crossable,
            unsigned int experience = 0,
            const string &filename = ObjectProxy<Character>::defaultFilename_,
            const string &nameClass = ObjectProxy<Character>::className_) :
    Entity(id, name, description, mass, size, position, block_position, filename, nameClass),
    sex_(sex), job_({job}),
    characteristics_({characteristics}),
    experience_(experience)
  {
    characteristics_.cutCharacteristicsTooHigh();
  }

  /**
   * @brief Default constructor with all required parameters
   * @param name
   * @param description
   * @param mass
   * @param size
   * @param sex
   * @param job role in society
   * @param characteristics initial characteristics of the Character
   * @param position initial position of the Character on the map
   * @param block_position occupation of its position by the Character
   * @param experience initial experience of the character, define level
   * @param filename path to file where the Character will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  Character(const string &name,
            const string &description,
            float mass,
            float size,
            Sex sex,
            const NamedObject &job,
            const Buff &characteristics,
            const Position &position = {0, 0, 0},
            Blocker block_position = Blocker::Crossable,
            unsigned int experience = 0,
            const string &filename = ObjectProxy<Character>::defaultFilename_,
            const string &nameClass = ObjectProxy<Character>::className_) :
    Entity(name, description, mass, size, position, block_position, filename, nameClass),
    sex_(sex), job_({job}),
    characteristics_({characteristics}),
    experience_(experience)
  {
    characteristics_.cutCharacteristicsTooHigh();
  }

  Character(const Character &character) : Entity(character), sex_(character.sex_), job_({character.job_}),
                                          characteristics_({character.characteristics_}),
                                          experience_(character.experience_)
  {
    std::copy(character.buffs_.begin(), character.buffs_.end(), back_inserter(buffs_));
    std::copy(character.skills_.begin(), character.skills_.end(), inserter(skills_, skills_.begin()));

    properties_ = vector<NamedObject *>(character.properties_.size());
    for (unsigned int i = 0; i < character.properties_.size(); i++)
      properties_[i] = dynamic_cast<NamedObject *>(getCopy(character.properties_[i]));

    character.copyInventory(this);
    character.copyEquipment(this);
  }

  ~Character()
  {
    for (auto &i: properties_)
      delete (i);
    if (!inventory_.empty())
      for (auto &i:inventory_)
        delete (i.first);
    if (!equipment_.empty())
      for (auto &i:equipment_)
        delete (i.second);
  }

  Character &operator=(const Character &character)
  {
    for (auto &i: properties_)
      delete (i);
    properties_ = vector<NamedObject*>(character.properties_.size());
    if (!inventory_.empty())
    {
      for (auto &i:inventory_)
        delete (i.first);
      inventory_ = map<Item *, unsigned int>();
    }
    if (!equipment_.empty())
    {
      for (auto &i:equipment_)
        delete (i.second);
      equipment_ = map<BodyPart, Equipment*>();
    }
    Entity::operator=(character);
    sex_ = character.sex_;
    job_ = {character.job_};
    characteristics_ = {character.characteristics_};
    experience_ = character.experience_;

    buffs_ = vector<Buff>();
    std::copy(character.buffs_.begin(), character.buffs_.end(), back_inserter(buffs_));
    skills_ = set<Skill>();
    std::copy(character.skills_.begin(), character.skills_.end(), inserter(skills_, skills_.begin()));

    for (unsigned int i = 0; i < character.properties_.size(); i++)
      properties_[i] = dynamic_cast<NamedObject *>(getCopy(character.properties_[i]));

    character.copyInventory(this);
    character.copyEquipment(this);
    return *this;
  }

  Sex getSex() const
  {
    return sex_;
  }

  NamedObject getJob() const
  {
    return job_;
  }

  Buff getCharacteristics() const
  {
    return characteristics_;
  }

  map<Item *, unsigned int> getInventory() const
  {
    return inventory_;
  }

  const Equipment *getEquipment(BodyPart bodyPart) noexcept(false)
  {
    for (auto &i : equipment_)
      if (i.first == bodyPart)
        return i.second;
    throw ARGUMENT_ERROR("Character::getEquipment: Character does not have specific equipment");
  }

  vector<Equipment *> getEquipmentList() const
  {
    vector<Equipment *> results;
    for (auto &i : equipment_)
      results.push_back(i.second);
    return results;
  }

  vector<NamedObject *> getProperties() const
  {
    return properties_;
  }

  vector<Buff> getBuffs() const
  {
    return buffs_;
  }

  set<Skill> getSkills() const
  {
    return skills_;
  }

  void removeSkill(const Skill& skill);

  unsigned int getExperience() const
  {
    return experience_;
  }

  void setJob(const NamedObject &job)
  {
    job_ = {job};
  }

  /**
   * @brief Add a Buff to the character characteristics
   * @param buff
   */
  void buffCharacteristics(const Buff &buff)
  {
    characteristics_ = characteristics_ + buff;
    characteristics_.cutCharacteristicsTooHigh(static_cast<int>(Buff::_MAX_CHARAC_));
  }

  //TODO: Check level
  unsigned int getLevel() const
  {
    return static_cast<unsigned int> (log(experience_ + 1));
  }

  bool isAlive();

  /**
   * @brief Setter for a specific Item of the Character
   * @param item Item stored or to store in the inventory
   * @param quantity number of item to add or retrieve
   * @return Number of Item added or retrieved from the inventory
   */
  unsigned int setInventory(const Item *item, int quantity);

  unsigned int getNumberItem(Item *item) const;

  bool hasItem(const Item *part) const;

  bool hasEquipment(const Equipment* part) const;

  void addBuff(const Buff &buff);

  bool hasBuff(const Buff &buff) const;

  void removeBuff(const Buff& buff);

  /**
   * @brief Sum the list of effects on the character
   * @return Total effect applied on the character
   */
  Buff getTotalBuff() const;

  /**
   * @brief Update the remaining time of the effects applied on a character
   * @param timeSteps number of time to update the buff
   */
  void update(float timeSteps);

  bool isEquiped(BodyPart body);

  /**
   * @brief Equip the specific equipment, store the already equiped item if any
   * @param equipment item to equip
   */
  void setEquipment(const Equipment *equipment);

  /**
   * @brief Store the equipment of a certain body part in the inventory
   * @param body part of the body where the equipment is stored
   */
  void storeEquipment(BodyPart body);

  void addProperty(const NamedObject *property);

  void removeProperty(const NamedObject *property);

  bool hasProperty(const NamedObject *property) const;

  void addSkill(const Skill& skill);

  bool hasSkill(const Skill& skill) const;

  void addExperience(int experience)
  {
    experience_ += experience;
  }

  bool equals(const Character &rhs) const;

  friend ostream &operator<<(ostream &os, const Character &character);

  void save(Store *store, bool subclass = false) const override;

  static Object *load(FILE *file);

  void copyEquipment(Character *character) const;

  void copyInventory(Character *character) const;
};

#endif //HOMESICK_CHARACTER_H
