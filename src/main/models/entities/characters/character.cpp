/**
 * @file character.cpp
 * @author drodarie
 * @date 04/08/17
 * */

#include "character.h"

unsigned int Character::setInventory(const Item *item, int quantity)
{
  if (!inventory_.empty())
    for (auto i = inventory_.begin(); i != inventory_.end(); i++)
      if (*i.operator*().first == *item)
      {
        if (quantity < 0 && i.operator*().second <= static_cast<unsigned int> (-quantity))
        { // Removing too much
          unsigned int result = i.operator*().second;
          delete (i.operator*().first);
          inventory_.erase(i);
          return result;
        }
        i.operator*().second += quantity;
        return static_cast<unsigned int>(labs(quantity)); // Return quantity removed or added
      }
  if (quantity > 0)
  {
    auto *to_insert = dynamic_cast<Item *>(getCopy(item));
    inventory_[to_insert] = static_cast<unsigned int>(quantity);
    return static_cast<unsigned int>(quantity);
  }
  return 0; // Don't have item
}

unsigned int Character::getNumberItem(Item *item) const
{
  for (auto &i:inventory_)
    if (*i.first == *item)
      return i.second;
  return 0;
}

bool Character::hasItem(const Item *item) const
{
  for (auto &i:inventory_)
    if (*i.first == *item)
      return true;
  return false;
}

bool Character::hasBuff(const Buff &buff) const
{
  return find(buffs_.begin(), buffs_.end(), buff) != buffs_.end();
}

void Character::addBuff(const Buff &buff)
{
  auto it = find(buffs_.begin(), buffs_.end(), buff);
  if (it != buffs_.end())
  {
    auto old = it.operator*().getDuration();
    it.operator*().setDuration(old + buff.getDuration());
  } else if (buff.getDuration() > 0)
  {
    buffs_.push_back({buff});
  }
}

void Character::removeBuff(const Buff& buff)
{
  auto it = find(buffs_.begin(), buffs_.end(), buff);
  if (it != buffs_.end())
  {
    buffs_.erase(it);
  }
}

Buff Character::getTotalBuff() const
{
  Buff result = Buff(characteristics_);
  for (auto &i : buffs_)
    result = result + i;
  for (auto &i : equipment_)
    result = result + i.second->getBuff();
  return result;
}

void Character::update(float timeSteps)
{
  for (auto i = buffs_.begin(); i != buffs_.end();)
    if (i.operator*().update(timeSteps))
      i = buffs_.erase(i);
    else
      i++;
}

bool Character::isEquiped(BodyPart body)
{
  return equipment_.find(body) != equipment_.end();
}

void Character::storeEquipment(BodyPart body)
{
  for (auto i = equipment_.cbegin(); i != equipment_.cend();)
    if (i->first == body)
    {
      setInventory(i->second, 1);
      delete (i->second);
      equipment_.erase(i++);
    } else
      i++;
}

void Character::setEquipment(const Equipment *equipment)
{
  if (hasItem(static_cast<const Item *>(equipment)))
  {
    BodyPart part = equipment->getBodyPart();
    storeEquipment(part);
    setInventory(static_cast<const Item *>(equipment), -1); //TODO: Avoid copy
    if (part != BodyPart::None)
      equipment_[part] = dynamic_cast<Equipment *>(getCopy(equipment));
  }
}

void Character::addProperty(const NamedObject *property)
{
  if (!hasProperty(property))
    properties_.push_back(dynamic_cast<NamedObject *>(getCopy(property)));
}

bool Character::hasProperty(const NamedObject *property) const
{
  for (auto &i:properties_)
    if (*i == *property)
      return true;
  return false;
}

void Character::removeProperty(const NamedObject *property)
{
  for (auto it = properties_.begin(); it != properties_.end(); it++)
    if (*property == *it.operator*())
    {
      delete (it.operator*());
      properties_.erase(it);
      return;
    }
}

void Character::addSkill(const Skill& skill)
{
  skills_.insert(skill);
}

bool Character::hasSkill(const Skill& skill) const
{
  return find(skills_.begin(), skills_.end(), skill) != skills_.end();
}

bool Character::isAlive()
{
  return getTotalBuff().getPV() > 0.;
}

void Character::save(Store *store, bool subclass) const
{
  Entity::save(store, subclass);
  *store << static_cast<int>(sex_);
  job_.save(store, true);
  characteristics_.save(store, true);

  *store << static_cast<unsigned int>(inventory_.size());
  for (auto &item: inventory_)
  {
    item.first->saveSignature(store);
    *store << item.second;
  }

  *store << static_cast<unsigned int>(equipment_.size());
  for (auto &item: equipment_)
    item.second->saveSignature(store);

  *store << static_cast<unsigned int>(properties_.size());
  for (auto &property: properties_)
    property->saveSignature(store);

  *store << static_cast<unsigned int>(buffs_.size());
  for (auto &buff: buffs_)
    buff.save(store, true);

  *store << static_cast<unsigned int>(skills_.size());
  for (auto &skill: skills_)
    skill.save(store, true);

  *store << experience_;
}

Object *Character::load(FILE *file)
{
  auto *entity = load_T<Entity>(file);
  auto sex = Sex(readInt(file));
  auto *job = load_T<NamedObject>(file);
  auto *characteristics = load_T<Buff>(file);
  auto *result = new Character(entity->getId(), entity->getName(), entity->getDescription(), entity->getMass(),
                               entity->getSize(), sex, *job, *characteristics, entity->getPosition(),
                               entity->getBlocker());
  result->setFilename(entity->getFilename());

  auto size = static_cast<unsigned int>(readInt(file));
  for (unsigned int i = 0; i < size; i++)
  {
    auto inventory = dynamic_cast<Item *>(loadSignature(file));
    result->inventory_[inventory] = static_cast<unsigned int>(readInt(file));
  }

  size = static_cast<unsigned int>(readInt(file));
  for (unsigned int i = 0; i < size; i++)
  {
    auto equipment = dynamic_cast<Equipment *>(loadSignature(file));
    result->equipment_[equipment->getBodyPart()] = equipment;
  }

  size = static_cast<unsigned int>(readInt(file));
  result->properties_ = vector<NamedObject*> (size);
  for (unsigned int i = 0; i < size; i++)
    result->properties_[i] = dynamic_cast<NamedObject *>(loadSignature(file));

  size = static_cast<unsigned int>(readInt(file));
  for (unsigned int i = 0; i < size; i++)
  {
    auto *buff = load_T<Buff>(file);
    result->addBuff(*buff);
    delete (buff);
  }

  size = static_cast<unsigned int>(readInt(file));
  for (unsigned int i = 0; i < size; i++)
  {
    auto *skill = load_T<Skill>(file);
    result->addSkill(*skill);
    delete (skill);
  }

  result->experience_ = static_cast<unsigned int>(readInt(file));

  delete (entity);
  delete (job);
  delete (characteristics);
  return result;
}

void Character::copyEquipment(Character *character) const
{
  for (auto item: equipment_)
    character->equipment_[item.first] = dynamic_cast<Equipment *>(getCopy(item.second));
}

void Character::copyInventory(Character *character) const
{
  for (auto item: inventory_)
    character->inventory_[dynamic_cast<Item *>(getCopy(item.first))] = item.second;
}

ostream &operator<<(ostream &os, const Character &character)
{
  os << static_cast<const Entity &>(character) << ", sex: " << int(character.sex_)
     << ",\njob: {" << character.job_ << "},\ncharacteristics: {" << character.characteristics_
     << "},\ninventory: {";
  for (auto &i: character.inventory_)
    os << "\n\t{" << *i.first << "}: " << i.second << ",";
  os << "},\nequipment: {";
  for (auto &i: character.equipment_)
    os << "\n\t{" << *i.second << "},";
  os << "},\nproperties: {";
  for (auto &i: character.properties_)
    os << "\n\t{" << *i << "},";
  os << "},\nbuffs: {";
  for (auto &i: character.buffs_)
    os << "\n\t{" << i << "},";
  os << "},\nskills: {";
  for (auto &i: character.skills_)
    os << "\n\t{" << i << "},";
  os << "},\nexperience: " << character.experience_;
  return os;
}

bool Character::equals(const Character &rhs) const
{
  if (!(Entity::equals(rhs) && sex_ == rhs.sex_ && job_.equals(rhs.job_) && characteristics_ == rhs.characteristics_ &&
        experience_ == rhs.experience_ && buffs_.size() == rhs.buffs_.size() &&
        skills_.size() == rhs.skills_.size() && properties_.size() == rhs.properties_.size() &&
        inventory_.size() == rhs.inventory_.size() && equipment_.size() == rhs.equipment_.size() &&
        equal(properties_.begin(), properties_.end(), rhs.properties_.begin(),
              [](NamedObject *a, NamedObject *b)
              { return a->equals(*b); }) &&
        equal(buffs_.begin(), buffs_.end(), rhs.buffs_.begin()) &&
        equal(skills_.begin(), skills_.end(), rhs.skills_.begin(), [](Skill a, Skill b)
        { return a.equals(b); })))
    return false;
  for (auto i: inventory_)
  {
    auto test = false;
    for (auto j: rhs.inventory_)
      if (i.first->equals(*j.first) && i.second == j.second)
      {
        test = true;
        break;
      }
    if (!test) return false;
  }
  for (auto i: equipment_)
  {
    auto test = false;
    for (auto j: rhs.equipment_)
      if (i.first == j.first && i.second->equals(*j.second))
      {
        test = true;
        break;
      }
    if (!test) return false;
  }
  return true;
}

void Character::removeSkill(const Skill& skill)
{
  for (auto it = skills_.begin(); it != skills_.end(); it++)
    if (skill == it.operator*())
    {
      skills_.erase(it);
      break;
    }
}

bool Character::hasEquipment(const Equipment *part) const {
  for (auto &equip: equipment_)
    if (equip.first==part->getBodyPart())
    {
      return *equip.second==*part;
    }
  return false;
}
