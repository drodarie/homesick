/**
 * @file entity.h
 * @author drodarie
 * @date 28/07/17
 */


#ifndef HOMESICK_ENTITY_H
#define HOMESICK_ENTITY_H

#include <string>
#include <utility>
#include <vector>
#include <cmath>
#include <ostream>
#include "../namedObject.h"
#include "../position.h"
#include "../../globals.h"

using namespace std;

/**
 * @brief How much the entity blocks its position 
 */
enum class Blocker : int
{
  None = 0,
  Crossable,
  Movable,
  Breakable,
  Wall
};

/**
 * @brief Abstract Class describing the minimal abstract Entity available in Homesick
 */
class Entity : public NamedObject, public ObjectProxy<Entity>
{
protected:
  float mass_ = 0; //!< Mass of the entity in kg
  float size_ = 2; //!< Size of the entity in meters
  Orientation direction_ = Orientation::Steady; //!< Orientation for the entity
  Position position_ = {0, 0, 0}; //!< Current position of the entity
  bool is_available_ = false; //!< Availability of the entity
  Blocker block_position_ = Blocker::None; //!< How much the entity blocks its position

public:
  /**
   * @brief Default constructor with all required parameters
   * @param id
   * @param name
   * @param description
   * @param mass in kg
   * @param size in meters
   * @param position initial location on the map
   * @param block_position occupation of its position by the entity
   * @param filename path to file where the Entity will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  Entity(unsigned int id, const string &name, const string &description,
         float mass, float size, const Position &position, Blocker block_position = Blocker::None,
         const string &filename = ObjectProxy<Entity>::defaultFilename_,
         const string &nameClass = ObjectProxy<Entity>::className_) :
    NamedObject(id, filename, name, description, nameClass),
    mass_(mass),
    size_(size),
    direction_(Orientation::Steady),
    position_({position}),
    is_available_(true),
    block_position_(block_position)
  {}

  /**
   * @brief Default constructor with all required parameters
   * @param name
   * @param description
   * @param mass in kg
   * @param size in meters
   * @param position initial location on the map
   * @param block_position occupation of its position by the entity
   * @param filename path to file where the Entity will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  Entity(const string &name, const string &description,
         float mass, float size, const Position &position, Blocker block_position = Blocker::None,
         const string &filename = ObjectProxy<Entity>::defaultFilename_,
         const string &nameClass = ObjectProxy<Entity>::className_) :
    NamedObject(filename, name, description, nameClass),
    mass_(mass),
    size_(size),
    direction_(Orientation::Steady),
    position_({position}),
    is_available_(true),
    block_position_(block_position)
  {}

  Entity(const Entity &entity) : NamedObject(entity), mass_(entity.mass_), size_(entity.size_),
                                 direction_(entity.direction_), position_({entity.position_}),
                                 is_available_(entity.is_available_), block_position_(entity.block_position_)
  {}

  float getMass() const
  {
    return mass_;
  }

  void setMass(float mass)
  {
    mass_ = mass;
  }

  float getSize() const
  {
    return size_;
  }

  void setSize(float size)
  {
    size_ = size;
  }

  bool isAvailable() const
  {
    return is_available_;
  }

  void setAvailable(bool availability)
  {
    is_available_ = availability;
  }

  bool isBlockingPosition() const
  {
    return static_cast<int>(block_position_) >= static_cast<int>(Blocker::Movable);
  }

  Blocker getBlocker() const
  {
    return block_position_;
  }

  void setBlocking(Blocker block_position)
  {
    block_position_ = block_position;
  }

  const Orientation &getDirection() const
  {
    return direction_;
  }

  void setDirection(const Orientation &orientation)
  {
    direction_ = orientation;
  }

  const Position &getPosition() const
  {
    return position_;
  }

  void setPosition(const Position &position)
  {
    position_ = position;
  }

  bool equals(const Entity &rhs) const
  {
    return NamedObject::equals(rhs) && abs(mass_ - rhs.mass_) <= 1e-6 && abs(size_ - rhs.size_) <= 1e-6
           && direction_ == rhs.direction_ && position_ == rhs.position_
           && is_available_ == rhs.is_available_ && block_position_ == rhs.block_position_;
  }

  friend ostream &operator<<(ostream &os, const Entity &entity);

  void save(Store *store, bool subclass = false) const override;

  static Object *load(FILE *file);
};

#endif //HOMESICK_ENTITY_H
