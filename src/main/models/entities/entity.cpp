/**
 * @file entity.cpp
 * @author drodarie
 * @date 10/08/17
 */

#include "entity.h"

void Entity::save(Store *store, bool subclass) const
{
  NamedObject::save(store, subclass);
  *store << mass_;
  *store << size_;
  position_.save(store, true);
  *store << int(block_position_);
}

ostream &operator<<(ostream &os, const Entity &entity)
{
  os << static_cast<const NamedObject &>(entity) << ",\nmass: "
     << entity.mass_ << ", size: " << entity.size_ << ", direction: {" << Position(entity.direction_)
     << "}, position: {" << entity.position_ << "},\navailability: " << entity.is_available_
     << ", blocking: " << int(entity.block_position_);
  return os;
}

Object *Entity::load(FILE *file)
{
  auto *namedObject = load_T<NamedObject>(file);
  auto result = new Entity(namedObject->getId(), namedObject->getName(), namedObject->getDescription(), readFloat(file),
                           readFloat(file), {}, Blocker::None, namedObject->getFilename());
  auto *position = Object::load_T<Position>(file);
  result->setPosition(*position);
  result->setBlocking(Blocker(readInt(file)));
  delete (namedObject);
  delete (position);
  return result;
}
