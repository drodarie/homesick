/**
 * @file maps.cpp
 * @author drodarie
 * @date 02/08/17
 */

#include "maps.h"

bool Map::checkPosition(const Position &position, unsigned int height) const
{
  if (getStatus(position) != State::Free)
    return false;

  for (int z = position.getZ(); z < position.getZ() + int(height); z++)
    if (getStatus({position.getX(), position.getY(), z}) == State::Occupied)
      return false;
  return true;
}

void Map::addEntity(Entity *entity, bool force_position, bool copy) noexcept(false)
{
  Position pos = entity->getPosition();
  auto size = static_cast<unsigned int>(ceil(entity->getSize()));
  if (isPositionInArea(pos) && checkPosition(pos, size))
  {
    if (copy)
      entities_.push_back(dynamic_cast<Entity *>(getCopy(entity)));
    else
      entities_.push_back(entity);
    if (entity->isBlockingPosition())
      states_[pos.getX() - x_min_][pos.getY() - y_min_][pos.getZ() - z_min_] = State::Occupied;
    return;
  }
  if (force_position)
    throw CANT_DO_ERROR("Map::addEntity: The position is not Free");

  map<Position, unsigned int> distances = getManhattanDistances(pos);
  unsigned int min_dist = 0;
  Position best_position;
  for (auto &dis: distances)
  {
    if (checkPosition(dis.first, size))
      if (min_dist == 0 or dis.second < min_dist)
      {
        min_dist = dis.second;
        best_position = dis.first;
      }
  }
  if (min_dist != 0)
  {
    if (copy)
      entities_.push_back(dynamic_cast<Entity *>(getCopy(entity)));
    else
      entities_.push_back(entity);
    entities_.back()->setPosition(best_position);
    try
    {
      if (entity->isBlockingPosition())
        states_[best_position.getX() - x_min_][best_position.getY() - y_min_][best_position.getZ() -
                                                                              z_min_] = State::Occupied;
    }
    catch (gameException &e)
    {
      throw CANT_DO_ERROR("Map::copyEntity: Error while setting entity position\n" + string(e.what()));
    }
  } else throw CANT_DO_ERROR("Map::copyEntity: None of the boxes is Free");
}

State Map::getStatus(const Position &position) const
{
  if (!isPositionInArea(position))
    return State::Unknown;
  return states_[position.getX() - x_min_][position.getY() - y_min_][position.getZ() - z_min_];
}

void Map::setStatus(const Position &position, State status)
{
  if (position.getX() < x_min_ || position.getX() > getMaxLength())
  {
    if (position.getX() < x_min_)
    {
      for (int i = position.getX(); i < x_min_; i++)
        states_.insert(states_.begin(),
                       vector<vector<State>>(static_cast<unsigned long>(width_),
                                             vector<State>(static_cast<unsigned long>(height_),
                                                           State::Unknown)));
      length_ = static_cast<unsigned int>(getMaxLength() - position.getX() + 1);
      x_min_ = position.getX();
    } else
    {
      length_ = static_cast<unsigned int>(position.getX() - x_min_ + 1);
      states_.resize(static_cast<unsigned long>(length_),
                     vector<vector<State>>(static_cast<unsigned long>(width_),
                                           vector<State>(static_cast<unsigned long>(height_), State::Unknown)));
    }
  }
  if (position.getY() < y_min_ || position.getY() > getMaxWidth())
  {
    if (position.getY() < y_min_)
    {
      for (unsigned int i = 0; i < length_; i++)
        for (int j = position.getY(); j < y_min_; j++)
          states_[i].insert(states_[i].begin(), vector<State>(static_cast<unsigned long>(height_),
                                                              State::Unknown));
      width_ = static_cast<unsigned int>(getMaxWidth() - position.getY() + 1);
      y_min_ = position.getY();
    } else
    {
      width_ = static_cast<unsigned int>(position.getY() - y_min_ + 1);
      for (unsigned int i = 0; i < length_; i++)
        states_[i].resize(static_cast<unsigned long>(width_),
                          vector<State>(static_cast<unsigned long>(height_), State::Unknown));

    }
  }
  if (position.getZ() < z_min_ || position.getZ() > getMaxHeight())
  {
    if (position.getZ() < z_min_)
    {
      for (unsigned int i = 0; i < length_; i++)
        for (unsigned int j = 0; j < width_; j++)
          for (int k = position.getZ(); k < z_min_; k++)
            states_[i][j].insert(states_[i][j].begin(), State::Unknown);
      height_ = static_cast<unsigned int>(getMaxHeight() - position.getZ() + 1);
      z_min_ = position.getZ();
    } else
    {
      height_ = static_cast<unsigned int>(position.getZ() - z_min_ + 1);
      for (unsigned int i = 0; i < length_; i++)
        for (unsigned int j = 0; j < width_; j++)
          states_[i][j].resize(static_cast<unsigned long>(height_), State::Unknown);
    }
  }
  if (!hasEntity(position))
    states_[position.getX() - x_min_][position.getY() - y_min_][position.getZ() - z_min_] = status;
}

void Map::removeEntity(const Entity *entity, bool to_delete)
{
  auto old = entities_.size();
  auto found = partition(entities_.begin(), entities_.end(),
                         [=](Entity *entity1)
                         { return !(*entity == *entity1 && entity->getPosition() == entity1->getPosition()); });
  if (found == entities_.end()) // Did not find at the expected position.
  {
    found = partition(entities_.begin(), entities_.end(),
                      [=](Entity *entity1)
                      { return *entity != *entity1; });
  }
  if (to_delete)
  {
    for_each(found, entities_.end(), [](Entity *o)
    { delete o; });
  }
  entities_.erase(found, entities_.end());
  if (old != entities_.size() && entity->isBlockingPosition())
    states_[entity->getPosition().getX() - x_min_][entity->getPosition().getY() - y_min_][entity->getPosition().getZ() -
                                                                                          z_min_] = State::Free;
}

bool Map::hasEntity(const Entity *entity) const
{
  return any_of(entities_.begin(), entities_.end(), [=](Entity *entity1)
  { return *entity == *entity1; });
}

Map Map::getSubArea(int length_min, int length_max, int width_min, int width_max, int height_min,
                    int height_max) noexcept(false)
{
  if (length_max < length_min || length_max > getMaxLength() || length_min < x_min_ ||
      width_max < width_min || width_max > getMaxWidth() || width_min < y_min_ ||
      height_max < height_min || height_max > getMaxHeight() || height_min < z_min_)
    throw ARGUMENT_ERROR("Map::getSubArea: Boundaries provided are not sensible or greater than the Map's\n");

  Map result(id_, name_, description_, length_min, static_cast<unsigned int>(length_max - length_min + 1),
             width_min, static_cast<unsigned int>(width_max - width_min + 1),
             height_min, static_cast<unsigned int>(height_max - height_min + 1),
             State::Unknown,
             filename_, nameClass_);
  for (auto i = static_cast<unsigned int>(length_min - x_min_);
       i < static_cast<unsigned int>(length_max - x_min_ + 1); i++)
    for (auto j = static_cast<unsigned int>(width_min - y_min_);
         j < static_cast<unsigned int>(width_max - y_min_ + 1); j++)
      for (auto k = static_cast<unsigned int>(height_min - z_min_);
           k < static_cast<unsigned int>(height_max - z_min_ + 1); k++)
        result.setStatus(Position(i + x_min_, j + y_min_, k + z_min_), states_[i][j][k]);

  for (auto &j : entities_)
  {
    const Position &position = j->getPosition();
    if (result.isPositionInArea(position))
      result.entities_.emplace_back(dynamic_cast<Entity *>(getCopy(j)));
  }
  return result;
}

void Map::addLayer(int height, State state) noexcept(false)
{
  if (!entities_.empty() && state == State::Occupied)
    for (auto entity: entities_)
      if (entity->getPosition().getZ() + entity->getSize() >= height
          && entity->getPosition().getZ() <= height)
        throw ARGUMENT_ERROR("Map::addLayer: An entity is already present and cross the layer\n");
  for (int i = x_min_; i <= getMaxLength(); i++)
    for (int j = y_min_; j <= getMaxWidth(); j++)
      setStatus({i, j, height}, state);
}

void Map::removeLayer(int height)
{
  if (height < z_min_ || height > getMaxHeight())
    return;

  auto found = partition(entities_.begin(), entities_.end(),
                         [=](Entity *entity)
                         { return entity->getPosition().getZ() != height; });
  for_each(found, entities_.end(), [](Entity *o)
  { delete o; });
  entities_.erase(found, entities_.end());
  if (height == getMaxHeight() || height == z_min_)
  {
    for (unsigned int i = 0; i < length_; i++)
      for (unsigned int j = 0; j < width_; j++)
        states_[i][j].erase(states_[i][j].begin() + height - z_min_);
    if (height_ == 1)
    {
      states_ = vector<vector<vector<State>>>(1, vector<vector<State>>(1, vector<State>(1, State::Unknown)));
      length_ = 1;
      width_ = 1;
    } else
    {
      z_min_ += int(z_min_ == height);
      height_--;
    }
  } else
    for (unsigned int i = 0; i < length_; i++)
      for (unsigned int j = 0; j < width_; j++)
        states_[i][j][height - z_min_] = State::Unknown;
}

void Map::addLength(int length, State state)
{
  for (int j = y_min_; j <= getMaxWidth(); j++)
    for (int i = z_min_; i <= getMaxHeight(); i++)
      setStatus({length, j, i}, state);
}

void Map::removeLength(int length)
{
  if (length < x_min_ || length > getMaxLength())
    return;

  auto found = partition(entities_.begin(), entities_.end(),
                         [=](Entity *entity)
                         { return entity->getPosition().getX() != length; });
  for_each(found, entities_.end(), [](Entity *o)
  { delete o; });
  entities_.erase(found, entities_.end());

  if (length == x_min_ || length == getMaxLength())
  {
    states_.erase(states_.begin() + length - x_min_);

    if (length_ == 1)
    {
      states_ = vector<vector<vector<State>>>(1, vector<vector<State>>(1, vector<State>(1, State::Unknown)));
      width_ = 1;
      height_ = 1;
    } else
    {
      x_min_ += int(x_min_ == length);
      length_--;
    }
  } else
    for (unsigned int i = 0; i < width_; i++)
      for (unsigned int j = 0; j < height_; j++)
        states_[length - x_min_][i][j] = State::Unknown;
}

void Map::addWidth(int width, State state)
{
  for (int j = x_min_; j <= getMaxLength(); j++)
    for (int i = z_min_; i <= getMaxHeight(); i++)
      setStatus({j, width, i}, state);
}

void Map::removeWidth(int width)
{
  if (width < y_min_ || width > getMaxWidth())
    return;

  auto found = partition(entities_.begin(), entities_.end(),
                         [=](Entity *entity)
                         { return entity->getPosition().getY() != width; });
  for_each(found, entities_.end(), [](Entity *o)
  { delete o; });
  entities_.erase(found, entities_.end());

  if (width == getMaxWidth() || width == y_min_)
  {
    for (unsigned int i = 0; i < length_; i++)
      states_[i].erase(states_[i].begin() + width - y_min_);

    if (width_ == 1)
    {
      states_ = vector<vector<vector<State>>>(1, vector<vector<State>>(1, vector<State>(1, State::Unknown)));
      length_ = 1;
      height_ = 1;
    } else
    {
      y_min_ += int(y_min_ == width);
      width_--;
    }
  } else
    for (unsigned int i = 0; i < length_; i++)
      for (unsigned int j = 0; j < height_; j++)
        states_[i][width - y_min_][j] = State::Unknown;
}

void Map::removePosition(const Position &position)
{
  auto found = partition(entities_.begin(), entities_.end(),
                         [=](Entity *entity)
                         { return entity->getPosition() != position; });
  for_each(found, entities_.end(), [](Entity *o)
  { delete o; });
  entities_.erase(found, entities_.end());
  setStatus(position, State::Unknown);
}

void Map::save(Store *store, bool subclass) const
{
  NamedObject::save(store, subclass);
  *store << x_min_;
  *store << length_;
  *store << y_min_;
  *store << width_;
  *store << z_min_;
  *store << height_;
  for (auto plane: states_)
    for (auto line: plane)
      for (auto state: line)
        *store << int(state);
  *store << static_cast<int>(entities_.size());
  for (const auto &entity: entities_)
  {
    entity->saveSignature(store);
    entity->getPosition().save(store, true);
  }
}

ostream &operator<<(ostream &os, const Map &area)
{
  os << static_cast<const NamedObject &>(area) << ",\nx_min: " << area.x_min_ << ", length: " << area.getLength()
     << ", y_min: " << area.y_min_ << ", width: " << area.getWidth()
     << ", z_min: " << area.z_min_ << ", height: " << area.getHeight() << ",\nentities: {";
  for (auto &i: area.entities_)
    os << "\n\t{" << *i << "},";
  os << "}";
  return os;
}

Object *Map::load(FILE *file)
{
  auto *namedObject = load_T<NamedObject>(file);
  auto result = new Map(namedObject->getId(), namedObject->getName(), namedObject->getDescription(),
                        readInt(file), static_cast<unsigned int>(readInt(file)),
                        readInt(file), static_cast<unsigned int>(readInt(file)),
                        readInt(file), static_cast<unsigned int>(readInt(file)),
                        State::Unknown,
                        namedObject->getFilename());
  for (unsigned int x = 0; x < result->length_; x++)
    for (unsigned int y = 0; y < result->width_; y++)
      for (unsigned int z = 0; z < result->height_; z++)
        result->states_[x][y][z] = State(readInt(file));

  auto size = static_cast<unsigned int>(readInt(file));
  for (unsigned int i = 0; i < size; i++)
  {
    result->entities_.push_back(dynamic_cast<Entity *>(loadSignature(file)));
    auto position = Object::load_T<Position>(file);
    result->entities_.rbegin().operator*()->setPosition(*position);
    delete(position);
  }

  delete (namedObject);
  return result;
}

void Map::moveEntity(Entity *entity, const Position &position) noexcept(false)
{

  auto iter = find_if(entities_.begin(), entities_.end(), [=](Entity *entity1)
  { return *entity == *entity1; });
  if (iter == entities_.end())
    throw ARGUMENT_ERROR("Map::moveEntity: Entity is not on the Map.\n");
  else if (checkPosition(position, static_cast<unsigned int>(floor(entity->getSize()))))
  {
    Entity *entity1 = iter.operator*();
    if (entity1->isBlockingPosition())
      states_[entity1->getPosition().getX() - x_min_][entity1->getPosition().getY() - y_min_][
        entity1->getPosition().getZ() - z_min_] = State::Free;
    entity1->setPosition(position);
    if (entity1->isBlockingPosition())
      states_[entity1->getPosition().getX() - x_min_][entity1->getPosition().getY() - y_min_][
        entity1->getPosition().getZ() - z_min_] = State::Occupied;
  } else
  {
    throw ARGUMENT_ERROR("Map::moveEntity: Position is not free.\n");
  }
}

vector<Entity *> Map::getEntitiesInArea(Map subArea)
{
  auto entities = vector<Entity *>();
  for (auto &j : entities_)
  {
    const Position &position = j->getPosition();
    if (subArea.isPositionInArea(position))
      entities.emplace_back(j);
  }
  return entities;
}

Map Map::getObservedField(const Position &position, const Orientation &orientation,
                          unsigned int radius, float angle)
{
  Map area(0, name_, description_, position.getX(), 1, position.getY(), 1, position.getZ(), 1);
  area.setStatus(position, State::Free);
  if (radius == 0)
    return area;

  auto positions = getSurroundingPositions(position, radius);
  positions.pop_back();
  for (auto it = positions.begin(); it != positions.end();)
  {
    int res = position.isInField(it.operator*(), orientation, radius, angle);
    if (res == 0)
      positions.erase(it);
    else
    {
      auto line = position.line3d(it.operator*());
      bool stillVisible = true;
      for (unsigned int i = 1; i < line.size(); i++)
      {
        unsigned long current_size = positions.size();
        positions.erase(remove_if(positions.begin(), positions.end(),
                                  [&](Position pos)
                                  { return pos == line[i]; }), positions.end());
        State state = getStatus(line[i]);
        if (current_size > positions.size())
        {
          if (stillVisible)
          {
            area.setStatus(line[i], state);
            try
            {
              vector<Entity *> entities = getEntity(line[i]);
              for (auto entity: entities)
                area.entities_.push_back(entity);
            } catch (ARGUMENT_ERROR &e)
            {}
          }
        }
        if (state == State::Occupied)
          stillVisible = false;
      }
    }

  }
  return area;
}

map<Position, unsigned int> Map::getManhattanDistances(const Position &pos) const
{
  map<Position, unsigned int> distances;
  for (int i = x_min_; i <= getMaxLength(); i++)
    for (int j = y_min_; j <= getMaxWidth(); j++)
      for (int k = z_min_; k <= getMaxHeight(); k++)
        distances[Position(i, j, k)] = Position(i, j, k).getManhattanDistance(pos);
  return distances;
}

vector<pair<Position, double>> Map::getDistances(const Position &position) const
{
  vector<pair<Position, double>> distances(length_ * width_ * height_);
  unsigned int curr = 0;
  for (int i = x_min_; i <= getMaxLength(); i++)
    for (int j = y_min_; j <= getMaxWidth(); j++)
      for (int k = z_min_; k <= getMaxHeight(); k++)
      {
        distances[curr] = {Position(i, j, k), Position(i, j, k).getDistance(position)};
        curr++;
      }
  return distances;
}

vector<Entity *> Map::getEntity(const Position &position)
{
  auto result = vector<Entity *>();
  for (auto &i:entities_)
    if (position == i->getPosition())
      result.push_back(dynamic_cast<Entity *>(getCopy(i)));
  return result;
}

vector<Position> Map::getConnectedBoxes(const Position &position, unsigned int radius = 1) const
{
  vector<Position> results;
  if (radius == 0)
    return results;
  auto rad = int(radius),
    x_min = max(0, position.getX() - rad - x_min_), x_max = min(static_cast<int>(length_ - 1),
                                                                position.getX() + rad - x_min_),
    y_min = max(0, position.getY() - rad - y_min_), y_max = min(static_cast<int>(width_ - 1),
                                                                position.getY() + rad - y_min_),
    z_min = max(0, position.getZ() - rad - z_min_), z_max = min(static_cast<int>(height_ - 1),
                                                                position.getZ() + rad - z_min_);
  for (int i = x_min; i <= x_max; i++)
    for (int j = y_min; j <= y_max; j++)
      for (int k = z_min; k <= z_max; k++)
        results.emplace_back(i, j, k);
  return results;
}

bool Map::hasEntity(const Position &position) const
{
  return any_of(entities_.begin(), entities_.end(), [=](Entity *entity1)
  { return position == entity1->getPosition(); });
}

void Map::addLayer(int height, Entity *entity) noexcept(false)
{
  auto old = entity->getPosition();
  if (!entities_.empty() && entity->isBlockingPosition())
    for (auto ent: entities_)
      if (ent->getPosition().getZ() + ent->getSize() > height
          && ent->getPosition().getZ() <= height)
        throw ARGUMENT_ERROR("Map::addLayer: An entity is already present and cross the layer\n");
  for (int i = x_min_; i <= getMaxLength(); i++)
    for (int j = y_min_; j <= getMaxWidth(); j++)
      setStatus({i, j, height}, State::Free);
  for (int i = 0; i < int(length_); i++)
  {
    for (int j = 0; j < int(width_); j++)
    {
      entity->setPosition({x_min_ + i, y_min_ + j, z_min_ + height});
      try
      {
        addEntity(entity, true);
      } catch (gameException &e)
      {}
    }
  }
  entity->setPosition(old);
}

void Map::addLength(int length, Entity *entity)
{
  auto old = entity->getPosition();
  addLength(length, State::Free);
  for (int i = 0; i < int(width_); i++)
  {
    for (int j = 0; j < int(height_); j++)
    {
      entity->setPosition({x_min_ + length, y_min_ + i, z_min_ + j});
      try
      {
        addEntity(entity, true);
      } catch (gameException &e)
      {}
    }
  }
  entity->setPosition(old);
}

void Map::addWidth(int width, Entity *entity)
{
  auto old = entity->getPosition();
  addWidth(width, State::Free);
  for (int i = 0; i < int(length_); i++)
  {
    for (int j = 0; j < int(height_); j++)
    {
      entity->setPosition({x_min_ + i, y_min_ + width, z_min_ + j});
      try
      {
        addEntity(entity, true);
      } catch (gameException &e)
      {}
    }
  }
  entity->setPosition(old);
}

Blocker Map::getBlocker(const Position &position) const
{
  auto result = Blocker::None;
  auto state = getStatus(position);
  if (state == State::Unknown)
    return result;
  else if (state == State::Occupied)
  {
    for (auto &i:entities_)
      if (position == i->getPosition())
        if (i->getBlocker() > result)
          result = i->getBlocker();
    if (result == Blocker::None) // No entity on position
      return Blocker::Wall;
  }
  return result;
}

vector<Position> Map::getAvailablePositions(unsigned int height) const
{
  auto result = vector<Position>();
  for (auto i = x_min_; i <= getMaxLength(); i++)
    for (auto j = y_min_; j <= getMaxWidth(); j++)
      for (auto k = z_min_; k <= getMaxHeight(); k++)
      {
        Position position(i, j, k);
        if (checkPosition(position, height))
          result.push_back(position);
      }
  return result;
}

vector<Position> Map::getFloor(unsigned int height) const
{
  auto result = vector<Position>();
  for (unsigned int i = 0; i < length_; i++)
    for (unsigned int j = 0; j < width_; j++)
      for (unsigned int k = 0; k < height_; k++)
      {
        Position position(i + x_min_, j + y_min_, k + z_min_);
        if (k != 0 && checkPosition(position, height) && states_[i][j][k - 1] == State::Occupied)
        {
          for (auto entity: entities_)
          {
            if (entity->getPosition() == (position - Position(0, 0, 1)) && entity->getBlocker() >= Blocker::Movable)
            {
              result.push_back(position);
              break;
            }
          }
        }
      }
  return result;
}

bool Map::equals(const Map &rhs) const
{
  if (!(NamedObject::equals(rhs) && x_min_ == rhs.x_min_ && length_ == rhs.length_ && y_min_ == rhs.y_min_ &&
        width_ == rhs.width_ && z_min_ == rhs.z_min_ && height_ == rhs.height_ &&
        entities_.size() == rhs.entities_.size() && equal(entities_.begin(), entities_.end(), rhs.entities_.begin(),
                                                          [](Entity *a, Entity *b)
                                                          { return a->equals(*b); })))
    return false;
  for (unsigned int i = 0; i < length_; i++)
    for (unsigned int j = 0; j < width_; j++)
      for (unsigned int k = 0; k < height_; k++)
        if (states_[i][j][k] != rhs.states_[i][j][k]) return false;
  return true;
}

vector<Position> Map::getSurroundingPositions(const Position &position, unsigned int radius) const
{
  if (radius==0)
    return vector<Position>(1, position);
  auto pairs = getDistances(position);
  sort(pairs.begin(), pairs.end(), [=](pair<Position, double> &a, pair<Position, double> &b)
  { return a.second > b.second; });
  pairs.erase(pairs.begin(), find_if(pairs.begin(), pairs.end(), [=](const std::pair<Position, double> &p)
  { return p.second <= radius; }));
  vector<Position> positions;
  std::transform(pairs.begin(), pairs.end(), std::back_inserter(positions),
                 [](const std::pair<Position, double> &p)
                 { return p.first; });

  return positions;
}

Position Map::getEntityPosition(const Entity *entity) const noexcept(false)
{
  auto iter = find_if(entities_.begin(), entities_.end(), [=](Entity *entity1)
  { return *entity == *entity1; });
  if (iter == entities_.end())
    throw ARGUMENT_ERROR("Map::getEntityPosition: Entity is not on the Map.\n");
  return iter.operator*()->getPosition();
}
