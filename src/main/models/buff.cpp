/**
 * @file buff.cpp
 * @author drodarie
 * @date 01/08/17
 */

#include "buff.h"

const unsigned int Buff::_MAX_CHARAC_ = 100;

bool Buff::update(float timeSteps)
{
  duration_ -= timeSteps;
  return duration_ <= 1e-3;
}

vector<int> Buff::getTestCharacteristics() const
{
  vector<int> result;
  result.push_back(strength_);
  result.push_back(agility_);
  result.push_back(intelligence_);
  result.push_back(sociability_);

  return result;
}

ostream &operator<<(ostream &os, const Buff &buff)
{
  os << "strength: " << buff.strength_ << ", agility: " << buff.agility_
     << ", intelligence: " << buff.intelligence_ << ", sociability: " << buff.sociability_
     << ",\nmood: " << buff.mood_ << ", duration: " << buff.duration_ << ", pv: " << buff.life_points_
     << ", speed: " << buff.speed_;
  return os;
}

bool Buff::areCharacteristicsTooHigh(int maximum) const
{
  return strength_ > maximum || agility_ > maximum || intelligence_ > maximum || sociability_ > maximum;
}

void Buff::save(Store *store, bool subclass) const
{
  Object::save(store, subclass);
  *store << strength_;
  *store << agility_;
  *store << intelligence_;
  *store << sociability_;
  *store << mood_;
  *store << duration_;
  *store << life_points_;
  *store << speed_;
}

Object *Buff::load(FILE *file)
{
  return new Buff(readInt(file), readInt(file), readInt(file), readInt(file), readFloat(file),
                  readFloat(file), readInt(file), readFloat(file));
}

void Buff::cutCharacteristicsTooHigh(int max_) {
  strength_ = min(strength_, max_);
  agility_ = min(agility_, max_);
  intelligence_ = min(intelligence_, max_);
  sociability_ = min(sociability_, max_);
}

bool Buff::operator<(const Buff &rhs) const {
  return strength_ < rhs.strength_ && agility_ < rhs.agility_ && intelligence_ < rhs.intelligence_ &&
  sociability_ < rhs.sociability_ && mood_ < rhs.mood_ && duration_ < rhs.duration_ &&
  life_points_ < rhs.life_points_ && speed_ < rhs.speed_;
}

bool Buff::operator>(const Buff &rhs) const {
  return strength_ > rhs.strength_ && agility_ > rhs.agility_ && intelligence_ > rhs.intelligence_ &&
         sociability_ > rhs.sociability_ && mood_ > rhs.mood_ && duration_ > rhs.duration_ &&
         life_points_ > rhs.life_points_ && speed_ > rhs.speed_;
}
