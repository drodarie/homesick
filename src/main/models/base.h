/**
 * @file base.h
 * @author drodarie
 * @date 3/8/18.
*/

#ifndef HOMESICK_BASE_H
#define HOMESICK_BASE_H

#include <map>
#include <utility>
#include <memory>
#include <vector>
#include <cstring>
#include <typeinfo>
#include <ostream>
#include "../gameException.h"
#include "../globals.h"

using namespace std;

class Object;

typedef shared_ptr<Object> PObject;

/**
 * @brief Class to store element in a binary file
 */
class Store
{
private:
  vector<void *> data_; //!< Array of pointer to the data to save
  vector<unsigned int> types_size_; //!< Array of memory size of the data types to save
  vector<unsigned int> lengths_; //!< Number of associated data

public:
  /**
   * @brief Default Constructor
   */
  Store() = default;

  /**
   * @brief Destroy the remaining data if any
   */
  virtual ~Store()
  {
    for (auto &i : data_)
      free(i);
  }

  /**
 * @brief Save the content of data in a file
 * @param filename path to the file to save the data
 * @throws WRITING_FILE_ERROR if an error occurred while opening or writing into the file.
 */
  void save(const string &filename) noexcept(false);

  /**
   * @brief Add a boolean data to store in a binary file
   * @param data
   */
  void operator<<(const bool &data);

  /**
   * @brief Add an integer data to store in a binary file
   * @param data
   */
  void operator<<(const int &data);

  /**
   * @brief Add an unsigned integer data to store in a binary file
   * @param data
   */
  void operator<<(const unsigned int &data)
  {
    *this << (int) data;
  }

  /**
   * @brief Add a float value to store in a binary file
   * @param data
   */
  void operator<<(const float &data);

  /**
   * @brief Add a string value to store in a binary file
   * @param data
   */
  void operator<<(const string &data);

  /**
 * @brief Add an array of int values to store in a binary file
 * @param data
 */
  void operator<<(const vector<int> data)
  {
    *this << ((int) data.size());
    for (auto &i: data)
      *this << (i);
  }

  /**
   * @brief Add an array of float values to store in a binary file
   * @param data
   */
  void operator<<(const vector<float> data)
  {
    *this << ((int) data.size());
    for (auto &i: data)
      *this << (i);
  }
};

/**
 * @brief Get a map of class names to their factory functions.
 * @return map of class names to factory functions
 */
map<string, Object *(*)(FILE *)> &getFactory();

/**
 * @brief Get a map of class names to its copy functions.
 * @return map of class names to factory functions
 */
map<string, Object *(*)(const Object *)> &getCopyFunction();

map<string, vector<PObject>> &getDefaults();

void loadDefaults(const string &filename);

/**
 * @brief Static class to create the factories and copy functions maps
 */
class Factories
{
public:
  Factories(const string &name, Object *(*factory)(FILE *), Object *(*copyFunction)(const Object *),
            const string &filename)
  {
    addFactory(name, factory);
    addCopyFunction(name, copyFunction);
    loadDefaults(filename);
  }

private:
  /**
   * @brief Add Factory to the static map of factories
   * @param string name Class name
   * @param factory Factory returning Object*
   */
  static void addFactory(const string &name, Object *(*factory)(FILE *));

  /**
   * @brief Add Copy function to the static map of copy functions
   * @param string name Class name
   * @param factory Copy function returning Object*
   */
  static void addCopyFunction(const string &name, Object *(*copyFunction)(const Object *));
};

/**
 * @brief Abstract Class to define all object in Homesick
 */
class Object
{
protected:
  static vector<Object*> loaded_; //!< Loaded objects from files

  string nameClass_; //!< Name of the class

  /**
   * @brief Read a string from a file.
   * The string should be stored preceded by its size
   * @param pFile File from which we extract the string
   * @return string read
   * @throw READING_FILE_ERROR if the reading went wrong
   */
  static string readString(FILE *pFile) noexcept(false);

  /**
  * @brief Read an integer from a file.
  * @param pFile file from which we extract the integer
  * @throw READING_FILE_ERROR if the reading went wrong
  */
  static int readInt(FILE *pFile) noexcept(false);

  /**
 * @brief Read a boolean from a file.
 * @param pFile file from which we extract the boolean
 * @throw READING_FILE_ERROR if the reading went wrong
 */
  static bool readBool(FILE *pFile) noexcept(false);

  /**
   * @brief Read a float number from a file.
   * @param pFile File from which we extract the float number
   * @throw READING_FILE_ERROR if the reading went wrong
   */
  static float readFloat(FILE *pFile) noexcept(false);

public:
  /**
   * @brief Default Constructor
   * @param nameClass class of the current object
   */
  Object(const string &nameClass = "Object") : nameClass_(nameClass)
  {};

  Object(const Object &object) : nameClass_(object.nameClass_)
  {}

  /**
   * @brief Default Destructor
   */
  virtual ~Object() = default;

  /**
   * @brief Getter for class name
   * @return Stored class Name
   */
  string getNameClass() const
  {
    return nameClass_;
  }

  /**
   * @brief Loads Object from file. The Object should have the function static Object* load (File*).
   * @param file pointer to opened file
   * @return Object loaded
   * @throw READING_FILE_ERROR if the reading went wrong
   */
  static Object *load(FILE *file) noexcept(false);

  /**
   * @brief Loads an Object of class T from a file and dynamic cast it.
   * @tparam T Class of the objects to load.
   * @param file pointer to opened file
   * @return Object loaded and casted
   * @throw READING_FILE_ERROR if the reading went wrong
   */
  template<class T>
  static T *load_T(FILE *file) noexcept(false)
  {
    return dynamic_cast<T *>(T::load(file));
  }

  /**
   * @brief Get copy of an object.
   * @param source Object to copy
   * @return Copied Object
   */
  static Object *getCopy(const Object *source);

  /**
   * @brief Load all the Objects stored in a file. See load.
   * @param pFile pointer to opened file
   * @return Loaded objects
   * @throw READING_FILE_ERROR if the reading went wrong
   */
  static vector<Object *> load_all(FILE *pFile) noexcept(false);

  /**
   * @brief Load all the Objects stored in a file. See load.
   * @param filename Path to file.
   * @param clear if true will clear the loaded_ vector after reading the file (default true)
   * @return Loaded Objects
   * @throw READING_FILE_ERROR if the reading went wrong
   */
  static vector<Object *> load_all(const string &filename, bool clear=true) noexcept(false);

  /**
   * @brief Load all the objects from a file. extract only the ones matching specific class.
   * @tparam T Class of the objects to extract.
   * @param filename Path to file.
   * @param clear if true will clear the loaded_ vector after reading the file (default true)
   * @return Loaded Objects
   * @throw READING_FILE_ERROR if the reading went wrong
   */
  template<class T>
  static vector<T *> extract_all(const string &filename, bool clear=true) noexcept(false)
  {
    vector<Object *> objects = T::load_all(filename, clear);
    vector<T *> result;
    for (auto &i:objects)
    {
      try
      {
        result.emplace_back(dynamic_cast<T *>(i));
      }
      catch (exception &e)
      {}
    }
    objects.clear();
    return result;
  }

  /**
   * @brief Save all the objects to a specific file
   * @tparam T Class of the objects to store
   * @param filename filename pathname to file where the objects will be stored
   * @param objects Objects to store
   */
  template<class T>
  static void save_all(const string &filename, const vector<T> &objects)
  {
    Store store;
    store << int(objects.size());
    for (unsigned int i = 0; i < objects.size(); i++)
      objects[i].save(&store);
    store.save(filename);
  }

  /**
   * @brief Save a single object to a specific file
   * @tparam T Class of the object to store
   * @param filename filename pathname to file where the objects will be stored
   * @param object Object to store
   */
  template<class T>
  static void save_one(const string &filename, T object)
  {
    Store store;
    store << 1;
    object.save(&store);
    store.save(filename);
  }

  /**
  * @brief Add Object to Store
  * @param store
  * @param subclass if true, will store the className
  */
  virtual void save(Store *store, bool subclass = false) const
  {
    if (!subclass) *store << nameClass_;
  }
};

/**
 * @brief Static virtual class to access static T class functions
 * @tparam T class extending Object.
 */
template<typename T>
class ObjectProxy
{
protected:
  static Factories statices_; //!< Object to access factory and copy functions
  static string className_; //!< Static class Name of T
  static string defaultFilename_; //!< Default pathname to file where class will be stored

public:
  ObjectProxy()
  {
    (void) statices_;
    (void) className_;
    (void) defaultFilename_;
  }

  ~ObjectProxy() = default;

  static Object *copyPtr(const Object *source)
  {
    return new T(*dynamic_cast<const T *>(source));
  }
};

template<typename T>
string ObjectProxy<T>::defaultFilename_ = _DEFAULT_DATA_FOLDER_ + string(typeid(T).name() + 1) + ".bin";

template<typename T>
Factories ObjectProxy<T>::statices_(string(typeid(T).name() + 1), T::load, ObjectProxy<T>::copyPtr,
                                    ObjectProxy<T>::defaultFilename_);

template<typename T>
string ObjectProxy<T>::className_(typeid(T).name() + 1);

#endif //HOMESICK_BASE_H
