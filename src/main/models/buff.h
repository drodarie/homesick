/**
 * @file buff.h
 * @author drodarie
 * @date 27/07/17
 */

#ifndef HOMESICK_BUFF_H
#define HOMESICK_BUFF_H

#include <string>
#include <vector>
#include <cmath>
#include <ostream>
#include "base.h"

using namespace std;

/**
 * @brief Class to define both the characteristics of the characters and effects of entities and actions on them in homesick.
 */
class Buff : public Object, public ObjectProxy<Buff>
{
private:
  int strength_; //!< Strength characteristic or effect
  int agility_; //!< Agility characteristic or effect
  int intelligence_; //!< Intelligence characteristic or effect
  int sociability_; //!< Sociability characteristic or effect

  float mood_; //!< Mood state or effect
  float duration_; //!< Duration of the effect
  int life_points_; //!< Life points state or effect.
  float speed_; //!< Speed of movement or effect.

public:
  static const unsigned int _MAX_CHARAC_; //!< Default Maximum value for characteristics.

  /**
   * @brief Default constructor
   * @param strength
   * @param agility
   * @param intelligence
   * @param sociability
   * @param mood
   * @param duration
   * @param life_points
   * @param speed
   * @param nameClass name of the class. See Object
   */
  Buff(int strength = 0, int agility = 0, int intelligence = 0, int sociability = 0,
       float mood = 0, float duration = 0, int life_points = 0, float speed = 0,
       const string &nameClass = ObjectProxy<Buff>::className_)
    : Object(nameClass), strength_(strength), agility_(agility), intelligence_(intelligence), sociability_(sociability),
      mood_(mood), duration_(duration), life_points_(life_points), speed_(speed)
  {}

  Buff(const Buff &buff) : Object(buff), strength_(buff.strength_), agility_(buff.agility_),
                           intelligence_(buff.intelligence_), sociability_(buff.sociability_), mood_(buff.mood_),
                           duration_(buff.duration_), life_points_(buff.life_points_), speed_(buff.speed_)
  {}

  int getPV() const
  {
    return life_points_;
  }

  void setPV(int life_points)
  { life_points_ = life_points; }

  float getSpeed() const
  {
    return speed_;
  }

  void setSpeed(float speed)
  {
    speed_ = speed;
  }

  int getStrength() const
  {
    return strength_;
  }

  void setStrength(int strength)
  {
    strength_ = strength;
  }

  int getAgility() const
  {
    return agility_;
  }

  void setAgility(int agility)
  {
    agility_ = agility;
  }

  int getIntelligence() const
  {
    return intelligence_;
  }

  void setIntelligence(int intelligence)
  {
    intelligence_ = intelligence;
  }

  int getSociability() const
  {
    return sociability_;
  }

  void setSociability(int sociability)
  {
    sociability_ = sociability;
  }

  float getMood() const
  {
    return mood_;
  }

  void setMood(float mood)
  {
    mood_ = mood;
  }

  float getDuration() const
  {
    return duration_;
  }

  void setDuration(float duration)
  {
    duration_ = duration;
  }

  bool operator==(const Buff &rhs) const
  {
    return strength_ == rhs.strength_ &&
           agility_ == rhs.agility_ &&
           intelligence_ == rhs.intelligence_ &&
           sociability_ == rhs.sociability_ &&
           mood_ - rhs.mood_ <= 1e-5;
  }

  bool operator!=(const Buff &rhs) const
  {
    return !(rhs == *this);
  }

  Buff operator+(const Buff &rhs) const
  {
    return {strength_ + rhs.strength_,
            agility_ + rhs.agility_,
            intelligence_ + rhs.intelligence_,
            sociability_ + rhs.sociability_,
            mood_ + rhs.mood_,
            max(duration_, rhs.duration_),
            life_points_ + rhs.life_points_,
            speed_ + rhs.speed_};
  }

  Buff operator-(const Buff &rhs) const
  {
    return {strength_ - rhs.strength_,
            agility_ - rhs.agility_,
            intelligence_ - rhs.intelligence_,
            sociability_ - rhs.sociability_,
            mood_ - rhs.mood_,
            duration_,
            life_points_ + rhs.life_points_,
            speed_ + rhs.speed_};
  }

  bool operator<(const Buff &rhs) const;

  bool operator>(const Buff &rhs) const;

  friend ostream &operator<<(ostream &os, const Buff &buff);

  /**
   * @brief Update the duration of an effect
   * @param timeSteps number of steps to update
   * @return True if the effect is over, False if still pending
   */
  bool update(float timeSteps);

  /**
   * @brief Extract the main characteristics for further tests
   * @return array containing the characteristics. Order: Strength, Agility, Intelligence, Sociability.
   */
  vector<int> getTestCharacteristics() const;

  /**
   * @brief Test if the main characteristics are above a maximum value
   * @param max
   * @return True if the main characteristics are above max
   */
  bool areCharacteristicsTooHigh(int max= static_cast<int>(Buff::_MAX_CHARAC_)) const;

  /**
   * @brief Cut the characteristics above a maximum value
   * @param max
   */
  void cutCharacteristicsTooHigh(int max=static_cast<int>(Buff::_MAX_CHARAC_));

  static Object *load(FILE *file);

  void save(Store *store, bool subclass = false) const;
};


#endif //HOMESICK_BUFF_H
