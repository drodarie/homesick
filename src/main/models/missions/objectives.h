/**
 * @file objectives.h
 * @author drodarie
 * @date 04/08/17
 */

#ifndef HOMESICK_OBJECTIVES_H
#define HOMESICK_OBJECTIVES_H


#include <utility>
#include <algorithm>
#include "../actions/iaction.h"

using namespace std;

/**
 * @brief Status of completion of the objective
 */
enum Status:int
{
  Finished = 0, //!< Objective successfully completed
  Hidden, //!< Objective not yet accessible or hidden
  New, //!< Objective accepted or appearing
  Pending, //!< Objective in completion
  Failed, //!< Objective failed
  Refused //!< Objective refused by the character
};

/**
 * @brief Abstract Class to describe an Objective in Homesick
 */
class Objective : public NamedObject, public ObjectProxy<Objective>
{
protected:
  Status status_; //!< Status of the Objective
  float duration_; //!< Max duration of the objective. If negative, Objective has no duration.
  vector<Objective*> require_; //!< Requirements for the Objective to be completed
  float start_time_; //!< Time at which the objective started.
  unsigned int experience_; //!< Experience gained for completing the Objective.

public:
  /**
   * @brief Default Constructor
   * @param id
   * @param name
   * @param description
   * @param initial_time Time at which the objective started
   * @param status Status of completion of the objective
   * @param duration Max duration to accomplish objective, once started
   * @param experience Experience gained by accomplishing the objective itself (requirements not included)
   * @param filename path to file where the Objective will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  Objective(unsigned int id, const string &name, const string &description,
            float initial_time, Status status=Status::Hidden,
            float duration=-1, unsigned int experience=0,
            const string &filename = ObjectProxy<Objective>::defaultFilename_,
            const string &nameClass = ObjectProxy<Objective>::className_) :
    NamedObject(id, filename, name, description, nameClass),
    status_(status),
    duration_(duration),
    start_time_(initial_time),
    experience_(experience)
  {}

  /**
   * @brief Default Constructor
   * @param name
   * @param description
   * @param initial_time Time at which the objective started
   * @param status Status of completion of the objective
   * @param duration Max duration to accomplish objective, once started
   * @param experience Experience gained by accomplishing the objective itself (requirements not included)
   * @param filename path to file where the Objective will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  Objective(const string &name, const string &description,
            float initial_time, Status status=Status::Hidden, float duration=-1,
            unsigned int experience=0,
            const string &filename = ObjectProxy<Objective>::defaultFilename_,
            const string &nameClass = ObjectProxy<Objective>::className_) :
    NamedObject(filename, name, description, nameClass),
    status_(status),
    duration_(duration),
    start_time_(initial_time),
    experience_(experience)
  {}

  Objective(const Objective &objective):
    NamedObject(objective), status_(objective.status_),
    duration_(objective.duration_), start_time_(objective.start_time_),
    experience_(objective.experience_)
  {
    require_ = vector<Objective *>(objective.require_.size());
    for (unsigned int i = 0; i < objective.require_.size(); i++)
      require_[i] = dynamic_cast<Objective *>(getCopy(objective.require_[i]));
  }

  ~Objective()
  {
    for (auto &i: require_)
      delete (i);
  }

  Objective &operator=(const Objective &objective)
  {
    for (auto &i: require_)
      delete (i);
    NamedObject::operator=(objective);
    require_ = vector<Objective *>(objective.require_.size());
    for (unsigned int i = 0; i < objective.require_.size(); i++)
      require_[i] = dynamic_cast<Objective *>(getCopy(objective.require_[i]));
    status_ = objective.status_;
    duration_ = objective.duration_;
    experience_ = objective.experience_;
    start_time_ = objective.start_time_;
    return *this;
  }

  Status getStatus() const
  { return status_; }

  void setStatus(Status status)
  { status_ = status; }

  float getDuration() const
  { return duration_; }

  void setDuration(float duration)
  { duration_ = duration; }

  unsigned int getExperience() const {
    return experience_;
  }

  void setExperience(unsigned int experience) {
    experience_ = experience;
  }

  unsigned int getTotalExperience();

  float getStartingTime() const
  { return start_time_; }

  vector<Objective*> getRequiredObjectives() const
  { return require_; }

  bool hasObjective(const Objective* objective);

  void addObjective(Objective* objective);

  bool removeObjective(const Objective* objective);

  /**
   * @brief Checks if all the requirements of the objective have been fulfilled.
   * @param actor Character completing the objective.
   * @param location Map location where the actor is.
   * @param global_time Time of the update.
   * @return true if the objectives have been fulfilled, false else. Does not change the objective Status.
   */
  virtual bool meetRequirements(Character const* const actor, Map const * const location, const float global_time) const
  {
    if (duration_>0)
      return global_time-start_time_< duration_;
    else return true;
  }

  /**
   * @brief Updates the status of the objective and its required objectives.
   * @param actor Character completing the objective.
   * @param location Map location where the actor is.
   * @param global_time Time of the update.
   * @return Status of the objective after update.
   */
  Status update(const Character* const actor, const Map* const location, const float global_time);

  bool equals(const Objective &rhs) const;

  friend ostream &operator<<(ostream &os, const Objective &objective);

  void save(Store *store, bool subclass = false) const override;

  static Object *load(FILE *file);
};

#endif //HOMESICK_OBJECTIVES_H
