/**
 * @file statusObjectives.h
 * @author drodarie
 * @date 11/25/19.
*/

#ifndef HOMESICK_STATUSOBJECTIVES_H
#define HOMESICK_STATUSOBJECTIVES_H

#include "objectives.h"

/**
 * @brief Objectives for which the status of the character completing the objective is a requirement.
 */
class StatusObjective: public Objective, public ObjectProxy<StatusObjective>
{
  protected:
    Character final_; //!< Required final status of the character to validate objective.
  public:
    /**
   * @brief Default Constructor
   * @param id
   * @param name
   * @param description
   * @param finalStatus Status that the character should target to accomplish the objective.
   * @param initial_time Time at which the objective started
   * @param status Status of completion of the objective
   * @param duration Max duration to accomplish objective, once started
   * @param experience Experience gained by accomplishing the objective itself (requirements not included)
   * @param filename path to file where the Objective will be loaded and saved
   * @param nameClass name of the class. See Object
   */
    StatusObjective(unsigned int id, const string &name, const string &description,
                    const Character& finalStatus, float initial_time, Status status=Status::Hidden,
                    float duration=-1, unsigned int experience=0,
                    const string &filename = ObjectProxy<StatusObjective>::defaultFilename_,
                    const string &nameClass = ObjectProxy<StatusObjective>::className_) :
    Objective(id, name, description, initial_time, status, duration, experience, filename, nameClass),
    final_ ({finalStatus})
  {}

  /**
   * @brief Default Constructor
   * @param name
   * @param description
   * @param finalStatus Status that the character should target to accomplish the objective.
   * @param initial_time Time at which the objective started
   * @param status Status of completion of the objective
   * @param duration Max duration to accomplish objective, once started
   * @param experience Experience gained by accomplishing the objective itself (requirements not included)
   * @param filename path to file where the Objective will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  StatusObjective(const string &name, const string &description,
            const Character& finalStatus, float initial_time, Status status=Status::Hidden,
            float duration=-1, unsigned int experience=0,
            const string &filename = ObjectProxy<StatusObjective>::defaultFilename_,
            const string &nameClass = ObjectProxy<StatusObjective>::className_) :
    Objective(name, description, initial_time, status, duration, experience, filename, nameClass),
    final_({finalStatus})
  {}

  StatusObjective(const StatusObjective &objective):
    Objective(objective), final_({objective.final_})
  {}

  Character getFinalStatus() const
  { return final_; }

  void setFinalStatus(const Character& finalStatus)
  { final_ = {finalStatus}; }

  bool meetRequirements(Character const* const actor, Map const* const location, const float global_time) const override;

  bool equals(const StatusObjective &rhs) const
  {
    return Objective::equals(rhs) && final_.equals(rhs.final_);
  }

  friend ostream &operator<<(ostream &os, const StatusObjective &objective);

  void save(Store *writer, bool subclass = false) const override;

  static Object *load(FILE *file);
};


#endif //HOMESICK_STATUSOBJECTIVES_H
