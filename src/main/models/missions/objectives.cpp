/**
 * @file objectives.cpp
 * @author drodarie
 * @date 04/08/17
 */

#include "objectives.h"

bool Objective::hasObjective(const Objective* objective) {
  for (auto &i:require_)
    if (*i == *objective || i->hasObjective(objective))
      return true;
  return false;
}

void Objective::addObjective(Objective* objective) {
    if (!hasObjective(objective))
        require_.push_back(dynamic_cast<Objective*>(getCopy(objective)));
}

bool Objective::removeObjective(const Objective* objective) {
  for (auto it= require_.begin(); it!=require_.end(); it++)
  {
    if (*it.operator*()==*objective)
    {
      delete (it.operator*());
      require_.erase(it);
      return true;
    }
     if (it.operator*()->removeObjective(objective))
       return true;
  }
  return false;
}

bool Objective::equals(const Objective &rhs) const {
    return NamedObject::equals(rhs) && duration_ == rhs.duration_ &&
    status_== rhs.status_ && experience_== rhs.experience_ &&
      equal(require_.begin(), require_.end(), rhs.require_.begin(),
            [](Objective *a, Objective *b)
            { return a->equals(*b); });
}

ostream &operator<<(ostream &os, const Objective &objective)
{
  os << static_cast<const NamedObject &>(objective)
     << ",\nstatus: " << int(objective.status_) << ", duration: " << objective.duration_
     << ", experience: " << objective.experience_  << ", start time: " << objective.start_time_
     << ",\nrequired objectives: {";
  for (auto &i: objective.require_)
    os << "\n\t{" << *i << "},";
  os << "}";
  return os;
}

void Objective::save(Store *store, bool subclass) const {
  NamedObject::save(store, subclass);
  *store<<start_time_;
  *store<<static_cast<int>(status_);
  *store<<duration_;
  *store<<experience_;
  *store << static_cast<unsigned int>(require_.size());
  for (auto &part: require_)
    part->saveSignature(store);
}

Object *Objective::load(FILE *file) {
  auto *namedObject = load_T<NamedObject>(file);
  auto *result = new Objective(namedObject->getId(), namedObject->getName(), namedObject->getDescription(),
                              readFloat(file), Status(readInt(file)), readFloat(file),
                              static_cast<unsigned int>(readInt(file)), namedObject->getFilename());
  auto size = static_cast<unsigned int>(readInt(file));
  result->require_ = vector<Objective*> (size);
  for (unsigned int i = 0; i < size; i++)
    result->require_[i]= dynamic_cast<Objective*>(loadSignature(file));

  delete (namedObject);
  return result;
}

Status Objective::update(const Character *const actor, const Map *const location, const float global_time)
{
  Status result = Status::Finished;
  for (auto require: require_)
  {
    auto temp = require->update(actor, location, global_time);
    if (int(temp)>result)
      result = temp;
    if (temp== Status::Refused)
      break;
  }
  if (result>Status::Failed)
  {
    status_ = result;
    return status_;
  }

  switch(status_)
  {
    case Status::Hidden:
      if(result>Status::Finished)
        break;
      status_ = Status::New;
      start_time_ = global_time;
      break;
    case Status::New:
      status_ = Status::Pending;
    case Status::Pending:
      if (duration_>0 && global_time-start_time_> duration_)
      {
        status_ = Status::Failed;
        break;
      }
      if(result>Status::Finished || !meetRequirements(actor, location, global_time))
        break;
      status_ = Status::Finished;
    default:
      break;
  }
  return status_;
}

unsigned int Objective::getTotalExperience()
{
  unsigned int result = experience_;
  for (auto require: require_)
    result+= require->getExperience();
  return result;
}
