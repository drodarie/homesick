/**
 * @file statusObjectives.cpp
 * @author drodarie
 * @date 11/25/19.
*/

#include "statusObjectives.h"

Object *StatusObjective::load(FILE *file) {
  auto objective = dynamic_cast<Objective*>(Objective::load(file));
  auto final = dynamic_cast<Character*>(Character::load(file));
  auto result = new StatusObjective(objective->getId(), objective->getName(), objective->getDescription(), *final,
                                    objective->getStartingTime() , objective->getStatus(),
                                    objective->getDuration(), objective->getExperience(), objective->getFilename());
  for (auto require: objective->getRequiredObjectives())
    result->require_.push_back(dynamic_cast<Objective*>(getCopy(require)));
  delete(objective);
  delete(final);
  return result;
}

void StatusObjective::save(Store *writer, bool subclass) const
{
  Objective::save(writer, subclass);
  final_.save(writer, true);
}

bool StatusObjective::meetRequirements(Character const* const actor, Map const* const location, const float global_time) const {
  if (!Objective::meetRequirements(actor, location, global_time))
    return false;

  if (actor->getTotalBuff()<final_.getTotalBuff() ||
    final_.getExperience()>actor->getExperience())
    return false;

  for(auto &equip: final_.getEquipmentList())
    if(!actor->hasEquipment(equip))
      return false;

  for (auto &item: final_.getInventory())
    if (actor->getNumberItem(item.first)<item.second)
      return false;

  for (auto &prop: final_.getProperties())
    if(!actor->hasProperty(prop))
      return false;

  for (auto &skill: final_.getSkills())
    if (!actor->hasSkill(skill))
      return false;
  return true;
}

ostream &operator<<(ostream &os, const StatusObjective &objective) {
  os << static_cast<const Objective &>(objective)
  << ",\nFinal actor status: {"<<objective.final_<<"}";
  return os;
}
