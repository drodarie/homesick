/**
 * @file locationObjective.h
 * @author drodarie
 * @date 12/2/19.
*/

#ifndef HOMESICK_LOCATIONOBJECTIVE_H
#define HOMESICK_LOCATIONOBJECTIVE_H


#include "objectives.h"

/**
 * @brief Objectives for which the location of the character completing the objective is a requirement.
 */
class LocationObjective: public Objective, public ObjectProxy<LocationObjective>
{
protected:
  NamedObject location_; //!< Map signature in which the Objective will be completed.
  Position position_; //!< Required final position of the character to validate objective.
  unsigned int accepted_radius_; //!< Radius of acceptance for character position.

public:
  /**
   * @brief Default Constructor
   * @param id
   * @param name
   * @param description
   * @param initialTime Time at which the objective started.
   * @param location Signature of the Map where the objective should be completed.
   * @param position Position in the map where the Character should stand.
   * @param acceptedRadius Radius of acceptance around the objective position.
   * @param status Status of completion of the objective.
   * @param duration Max duration to accomplish objective, once started
   * @param experience Experience gained by accomplishing the objective itself (requirements not included)
   * @param filename path to file where the Objective will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  LocationObjective(unsigned int id, const string &name, const string &description, float initialTime,
    const NamedObject &location, const Position &position, unsigned int acceptedRadius=0, Status status=Status::Hidden,
    float duration=-1, unsigned int experience=0,
    const string &filename=ObjectProxy<LocationObjective>::defaultFilename_,
    const string &nameClass=ObjectProxy<LocationObjective>::className_):
      Objective(id, name, description, initialTime, status, duration, experience, filename, nameClass),
      location_(location), position_(position), accepted_radius_(acceptedRadius) {}

  /**
  * @brief Default Constructor
  * @param name
  * @param description
  * @param initialTime Time at which the objective started.
  * @param location Signature of the Map where the objective should be completed.
  * @param position Position in the map where the Character should stand.
  * @param acceptedRadius Radius of acceptance around the objective position.
  * @param status Status of completion of the objective.
  * @param duration Max duration to accomplish objective, once started
  * @param experience Experience gained by accomplishing the objective itself (requirements not included)
  * @param filename path to file where the Objective will be loaded and saved
  * @param nameClass name of the class. See Object
  */
  LocationObjective(const string &name, const string &description, float initialTime, const NamedObject &location,
    const Position &position, unsigned int acceptedRadius=0, Status status=Status::Hidden,
    float duration=-1, unsigned int experience=0,
    const string &filename=ObjectProxy<LocationObjective>::defaultFilename_,
    const string &nameClass=ObjectProxy<LocationObjective>::className_) :
      Objective(name, description, initialTime, status, duration, experience, filename, nameClass),
      location_({location}), position_(position), accepted_radius_(acceptedRadius) {}

  LocationObjective(const LocationObjective &objective) :
    Objective(objective), location_({objective.location_}),
    position_(objective.position_), accepted_radius_(objective.accepted_radius_)
  {}

  NamedObject getLocation() const
  { return location_; }

  void setLocation(const NamedObject &location)
  { location_ = {location}; }

  Position getPosition() const
  { return position_; }

  void setPosition(const Position &position)
  { position_ = position; }

  unsigned int getAcceptedRadius() const
  { return accepted_radius_; }

  void setAcceptedRadius(unsigned int acceptedRadius)
  { accepted_radius_ = acceptedRadius; }

  bool meetRequirements(Character const *const actor, Map const *const location, const float global_time) const override;

  bool equals(const LocationObjective &rhs) const
  {
    return Objective::equals(rhs) && location_== rhs.location_
     && position_==rhs.position_ && accepted_radius_ == rhs.accepted_radius_;
  }

  friend ostream &operator<<(ostream &os, const LocationObjective &objective);

  void save(Store *store, bool subclass=false) const override;

  static Object* load(FILE *file);
};


#endif //HOMESICK_LOCATIONOBJECTIVE_H
