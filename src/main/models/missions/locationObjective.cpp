/**
 * @file locationObjective.cpp
 * @author drodarie
 * @date 12/2/19.
*/

#include "locationObjective.h"


bool LocationObjective::meetRequirements(Character const *const actor, Map const *const location,
                                         const float global_time) const
{
  if (!Objective::meetRequirements(actor, location, global_time) or static_cast<NamedObject>(*location)!=location_ or
  !location->hasEntity(actor))
    return false;
  auto positions = location->getSurroundingPositions(position_, accepted_radius_);
  return find(positions.begin(), positions.end(), location->getEntityPosition(actor))!= positions.end();
}

void LocationObjective::save(Store *store, bool subclass) const
{
  Objective::save(store, subclass);
  location_.saveSignature(store);
  position_.save(store, true);
  *store<<accepted_radius_;
}

Object *LocationObjective::load(FILE *file)
{
  auto objective = dynamic_cast<Objective*>(Objective::load(file));
  unsigned int id = readInt(file);
  auto filename = readString(file);
  NamedObject location (id, filename, readString(file));
  auto position = dynamic_cast<Position*>(Position::load(file));
  auto result = new LocationObjective(objective->getId(), objective->getName(), objective->getDescription(),
    objective->getStartingTime(), location, *position, static_cast<unsigned int>(readInt(file)), objective->getStatus(),
    objective->getDuration(), objective->getExperience(), objective->getFilename());

  delete(position);
  delete(objective);
  return result;
}

ostream &operator<<(ostream &os, const LocationObjective &objective)
{
  os << static_cast<const Objective &>(objective)
  << ",\nlocation: {"<< objective.location_
  <<"},\nposition: {"<< objective.position_<<"}, accepted radius: "<<objective.accepted_radius_;
  return os;
}
