/**
 * @file ${FILE}
 * @author drodarie
 * @date 10/2/19.
*/

#ifndef HOMESICK_NAMEDOBJECT_H
#define HOMESICK_NAMEDOBJECT_H

#include <cassert>
#include <algorithm>
#include "base.h"

/**
 * @brief Interface for classes with ID, name and description.
 */
class NamedObject : public Object, public ObjectProxy<NamedObject>
{
protected:
  unsigned int id_; //!< Id of the object
  string filename_; //!< Path to file where the complete description of the class is stored
  string name_; //!< Name of the Object
  string description_; //!< Brief description of the skill and its utility

public:
  NamedObject(unsigned int id, const string &filename,
              const string &name, const string &description,
              const string &className = ObjectProxy<NamedObject>::className_) :
    Object(className),
    id_(id), filename_(filename),
    name_(name), description_(description)
  {}

  NamedObject(const string &filename, const string &name,
              const string &description,
              const string &className = ObjectProxy<NamedObject>::className_) :
    Object(className),
    filename_(filename),
    name_(name), description_(description)
  {
    unsigned int max_id = 0;
    if (getDefaults().find(filename) == getDefaults().end() && exists_file(filename))
      loadDefaults(filename);

    if (getDefaults().find(filename) != getDefaults().end() && !getDefaults()[filename].empty())
    {
      for (auto &i : getDefaults()[filename])
      {
        if (className == i->getNameClass())
          max_id = max(max_id, dynamic_cast<NamedObject *>(i.get())->getId());
      }
      max_id++;
    }
    id_ = max_id;
  }

  NamedObject(unsigned int id, const string &filename,
              const string &className):
    Object(className),
    id_(id), filename_(filename),
    name_(""), description_("")
  {}

  NamedObject(const NamedObject &namedObject) :
    Object(namedObject), id_(namedObject.id_), filename_(namedObject.filename_),
    name_(namedObject.name_), description_(namedObject.description_)
  {}

  unsigned int getId() const
  {
    return id_;
  }

  const string &getFilename() const
  {
    return filename_;
  }

  void setFilename(const string &filename)
  {
    filename_ = filename;
  }

  const string &getName() const
  {
    return name_;
  }

  void setName(const string &name)
  {
    NamedObject::name_ = name;
  }

  const string &getDescription() const
  {
    return description_;
  }

  void setDescription(const string &description)
  {
    NamedObject::description_ = description;
  }

  bool operator<(const NamedObject &rhs) const
  {
    return id_ < rhs.id_;
  }

  bool operator>(const NamedObject &rhs) const
  {
    return rhs < *this;
  }

  bool operator<=(const NamedObject &rhs) const
  {
    return !(rhs < *this);
  }

  bool operator>=(const NamedObject &rhs) const
  {
    return !(*this < rhs);
  }

  bool operator==(const NamedObject &rhs) const
  {
    return id_ == rhs.id_ && filename_ == rhs.filename_ && nameClass_ == rhs.nameClass_;
  }

  bool equals(const NamedObject &rhs) const
  {
    return rhs == *this && name_ == rhs.name_ && description_ == rhs.description_;
  }

  bool operator!=(const NamedObject &rhs) const
  {
    return !(rhs == *this);
  }

  friend ostream &operator<<(ostream &os, const NamedObject &object)
  {
    os << "id: " << object.id_ << ", filename: " << object.filename_ << ", name: " << object.name_ << ", description: "
       << object.description_;
    return os;
  }

  void updateDefaults() const;

  void save(Store *store, bool subclass = false) const override;

  /**
   * @brief Add the minimum data to describe an NamedObject to a Store
   * @param store
   */
  void saveSignature(Store *store) const;

  static Object *load(FILE *file);

  /**
   * @brief Load an NamedObject according to its signature, will open extra files if needed.
   * @param file
   * @return Loaded object
   * @throw FILE_ERROR if the referenced object could not be found
   */
  static Object* loadSignature(FILE* file) noexcept(false) {
    auto id =static_cast<unsigned int>(readInt(file));
    auto filename=readString(file), classname=readString(file);
    auto signature = NamedObject(id, filename, classname);
    string opened_file = getFileFilename(file);
    string partFilename_ = signature.filename_;
    if (partFilename_ == opened_file)
    {
      for (auto &i: loaded_)
      {
        try
        {
          if (signature == *dynamic_cast<NamedObject*>(i))
            return getCopy(i);
        }
        catch (exception& e)
        {}
      }

      throw FILE_ERROR("NamedObject::loadSignature: The file " + partFilename_ + " does not contain the object of id=" + to_string(signature.id_));
    }
    else
    {
      vector<NamedObject *> load_items = extract_all<NamedObject>(partFilename_, false);
      for (auto &i: load_items)
        if (signature == *i)
          return getCopy(i);
      throw FILE_ERROR("NamedObject::loadSignature: The file " + partFilename_ + " does not contain the object of id=" + to_string(signature.id_));
    }
  }
};

#endif //HOMESICK_NAMEDOBJECT_H
