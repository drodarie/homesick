/**
 * @file maps.h
 * @author drodarie
 * @date 02/08/17
 */

#ifndef HOMESICK_AREA_H
#define HOMESICK_AREA_H


#include <utility>
#include <iostream>
#include <algorithm>
#include "position.h"
#include "entities/entity.h"
#include "../globals.h"


/**
 * @brief Enum to describe the Box state
 */
enum class State : int
{
  Free = 0, //!< Box is free or empty
  Occupied, //!< Box is already occupied by an entity or filled
  Unknown //!< Default unknown state
};

/**
 * @brief 3D map of boxes containing entities for environment interactions in homesick
 */
class Map : public NamedObject, public ObjectProxy<Map>
{
protected:
  vector<vector<vector<State>>> states_; //!< Status of each box of the Map
  vector<Entity *> entities_; //!< Entities present in the Map

  int x_min_ = INT16_MAX; //!< minimum x-axis coordinate
  unsigned int length_ = 0; //!< length of the Map
  int y_min_ = INT16_MAX; //!< minimum y-axis coordinate
  unsigned int width_ = 0; //!< width of the Map
  int z_min_ = INT16_MAX; //!< minimum z-axis coordinate
  unsigned int height_ = 0; //!< height of the Map

  void reset(State state = State::Unknown)
  {
    states_.resize(length_);
    for (unsigned int i = 0; i < length_; i++)
    {
      states_[i].resize(width_);
      for (unsigned int j = 0; j < width_; j++)
      {
        states_[i][j].resize(height_, state);
      }
    }
  }

  void reset(const Entity *entity)
  {
    auto temp = dynamic_cast<Entity *>(getCopy(entity));
    states_.resize(length_);
    for (int i = 0; i < int(length_); i++)
    {
      states_[i].resize(width_);
      for (int j = 0; j < int(width_); j++)
      {
        states_[i][j].resize(height_, State::Free);
        for (int h = 0; h < int(height_); h++)
        {
          temp->setPosition({i, j, h});
          addEntity(temp, true);
        }
      }
    }
    delete (temp);
  }

public:
  /**
   * @brief Complete constructor
   * @param id
   * @param name
   * @param description
   * @param min_length starting coordinate for x-axis
   * @param length size for x-axis, minimum 1
   * @param min_width starting coordinate for y-axis
   * @param width size for y-axis, minimum 1
   * @param min_height starting coordinate for z-axis
   * @param height size for z-axis, minimum 1
   * @param state State of each positions generated
   * @param filename path to file where the Map will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  Map(unsigned int id, const string &name, const string &description,
      int min_length, unsigned int length,
      int min_width, unsigned int width,
      int min_height, unsigned int height,
      State state = State::Unknown,
      const string &filename = ObjectProxy<Map>::defaultFilename_,
      const string &nameClass = ObjectProxy<Map>::className_) :
    NamedObject(id, filename, name, description, nameClass),
    x_min_(min_length), length_(max(length, static_cast<unsigned int>(1))),
    y_min_(min_width), width_(max(width, static_cast<unsigned int>(1))),
    z_min_(min_height), height_(max(height, static_cast<unsigned int>(1)))
  {
    reset(state);
  }

  /**
   * @brief Complete constructor
   * @param name
   * @param description
   * @param min_length starting coordinate for x-axis
   * @param length size for x-axis, minimum 1
   * @param min_width starting coordinate for y-axis
   * @param width size for y-axis, minimum 1
   * @param min_height starting coordinate for z-axis
   * @param height size for z-axis, minimum 1
   * @param state State of each positions generated
   * @param filename path to file where the Map will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  Map(const string &name, const string &description,
      int min_length, unsigned int length,
      int min_width, unsigned int width,
      int min_height, unsigned int height,
      State state = State::Unknown,
      const string &filename = ObjectProxy<Map>::defaultFilename_,
      const string &nameClass = ObjectProxy<Map>::className_) :
    NamedObject(filename, name, description, nameClass),
    x_min_(min_length), length_(max(length, static_cast<unsigned int>(1))),
    y_min_(min_width), width_(max(width, static_cast<unsigned int>(1))),
    z_min_(min_height), height_(max(height, static_cast<unsigned int>(1)))
  {
    reset(state);
  }

  /**
   * @brief Complete constructor
   * @param id
   * @param name
   * @param description
   * @param min_length starting coordinate for x-axis
   * @param length size for x-axis, minimum 1
   * @param min_width starting coordinate for y-axis
   * @param width size for y-axis, minimum 1
   * @param min_height starting coordinate for z-axis
   * @param height size for z-axis, minimum 1
   * @param entity Entity to place on each position generated
   * @param filename path to file where the Map will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  Map(unsigned int id, const string &name, const string &description,
      int min_length, unsigned int length,
      int min_width, unsigned int width,
      int min_height, unsigned int height,
      const Entity *entity,
      const string &filename = ObjectProxy<Map>::defaultFilename_,
      const string &nameClass = ObjectProxy<Map>::className_) :
    NamedObject(id, filename, name, description, nameClass),
    x_min_(min_length), length_(max(length, static_cast<unsigned int>(1))),
    y_min_(min_width), width_(max(width, static_cast<unsigned int>(1))),
    z_min_(min_height), height_(max(height, static_cast<unsigned int>(1)))
  {
    reset(entity);
  }

  /**
   * @brief Complete constructor
   * @param name
   * @param description
   * @param min_length starting coordinate for x-axis
   * @param length size for x-axis, minimum 1
   * @param min_width starting coordinate for y-axis
   * @param width size for y-axis, minimum 1
   * @param min_height starting coordinate for z-axis
   * @param height size for z-axis, minimum 1
   * @param entity Entity to place on each position generated
   * @param filename path to file where the Map will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  Map(const string &name, const string &description,
      int min_length, unsigned int length,
      int min_width, unsigned int width,
      int min_height, unsigned int height,
      const Entity *entity,
      const string &filename = ObjectProxy<Map>::defaultFilename_,
      const string &nameClass = ObjectProxy<Map>::className_) :
    NamedObject(filename, name, description, nameClass),
    x_min_(min_length), length_(max(length, static_cast<unsigned int>(1))),
    y_min_(min_width), width_(max(width, static_cast<unsigned int>(1))),
    z_min_(min_height), height_(max(height, static_cast<unsigned int>(1)))
  {
    reset(entity);
  }

  /**
   * @brief Default constructor
   * @param id
   * @param name
   * @param description
   * @param length Size of x-axis, positions will start at 0, minimum 1
   * @param width Size of y-axis, positions will start at 0, minimum 1
   * @param height Size of z-axis, positions will start at 0, minimum 1
   * @param state State of each positions generated
   * @param filename path to file where the Map will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  Map(unsigned int id, const string &name, const string &description, unsigned int length,
      unsigned int width, unsigned int height,
      State state = State::Unknown,
      const string &filename = ObjectProxy<Map>::defaultFilename_,
      const string &nameClass = ObjectProxy<Map>::className_) :
    Map(id, name, description, 0, length, 0, width, 0, height, state, filename, nameClass)
  {}

  /**
   * @brief Default constructor
   * @param name
   * @param description
   * @param length Size of x-axis, positions will start at 0, minimum 1
   * @param width Size of y-axis, positions will start at 0, minimum 1
   * @param height Size of z-axis, positions will start at 0, minimum 1
   * @param state State of each positions generated
   * @param filename path to file where the Map will be loaded and saved
   * @param nameClass name of the class. See Object
   */
  Map(const string &name, const string &description, unsigned int length,
      unsigned int width, unsigned int height,
      State state = State::Unknown,
      const string &filename = ObjectProxy<Map>::defaultFilename_,
      const string &nameClass = ObjectProxy<Map>::className_) :
    Map(name, description, 0, length, 0, width, 0, height, state, filename, nameClass)
  {}

  /**
   * @brief Default constructor
   * @param id
   * @param name
   * @param description
   * @param length Size of x-axis, positions will start at 0, minimum 1
   * @param width Size of y-axis, positions will start at 0, minimum 1
   * @param height Size of z-axis, positions will start at 0, minimum 1
   * @param entity Entity to place on each position generated
   * @param filename path to file where the Map will be loaded and saved
   * @param nameClass name of the class. See Object
 */
  Map(unsigned int id, const string &name, const string &description,
      unsigned int length, unsigned int width, unsigned int height,
      const Entity *entity,
      const string &filename = ObjectProxy<Map>::defaultFilename_,
      const string &nameClass = ObjectProxy<Map>::className_) :
    Map(id, name, description, 0, length, 0, width, 0, height, entity, filename, nameClass)
  {}

  /**
   * @brief Default constructor
   * @param name
   * @param description
   * @param length Size of x-axis, positions will start at 0, minimum 1
   * @param width Size of y-axis, positions will start at 0, minimum 1
   * @param height Size of z-axis, positions will start at 0, minimum 1
   * @param entity Entity to place on each position generated
   * @param filename path to file where the Map will be loaded and saved
   * @param nameClass name of the class. See Object
 */
  Map(const string &name, const string &description,
      unsigned int length, unsigned int width, unsigned int height,
      Entity *entity,
      const string &filename = ObjectProxy<Map>::defaultFilename_,
      const string &nameClass = ObjectProxy<Map>::className_) :
    Map(name, description, 0, length, 0, width, 0, height, entity, filename, nameClass)
  {}

  Map(const Map &area) : NamedObject(area),
                         x_min_(area.x_min_), length_(area.length_),
                         y_min_(area.y_min_), width_(area.width_),
                         z_min_(area.z_min_), height_(area.height_)
  {
    states_ = vector<vector<vector<State>>>(area.states_.size());
    std::copy(area.states_.begin(), area.states_.end(), states_.begin());

    entities_ = vector<Entity *>(area.entities_.size());
    for (unsigned int i = 0; i < area.entities_.size(); i++)
      entities_[i] = dynamic_cast<Entity *>(getCopy(area.entities_[i]));
  }

  ~Map()
  {
    for (auto &i :entities_)
      delete (i);
  }

  Map &operator=(const Map &area)
  {
    for (auto &i :entities_)
      delete (i);
    NamedObject::operator=(area);
    x_min_ = area.x_min_;
    length_ = area.length_;
    y_min_ = area.y_min_;
    width_ = area.width_;
    z_min_ = area.z_min_;
    height_ = area.height_;
    states_ = vector<vector<vector<State>>>(area.states_.size());
    std::copy(area.states_.begin(), area.states_.end(), states_.begin());

    entities_ = vector<Entity *>(area.entities_.size());
    for (unsigned int i = 0; i < area.entities_.size(); i++)
      entities_[i] = dynamic_cast<Entity *>(getCopy(area.entities_[i]));
    return *this;
  }

  unsigned int getLength() const
  {
    return length_;
  }

  unsigned int getWidth() const
  {
    return width_;
  }

  unsigned int getHeight() const
  {
    return height_;
  }

  int getMinHeight() const
  { return z_min_; }

  int getMaxHeight() const
  { return z_min_ + height_ - 1; }

  int getMinWidth() const
  { return y_min_; }

  int getMaxWidth() const
  { return y_min_ + width_ - 1; }

  int getMinLength() const
  { return x_min_; }

  int getMaxLength() const
  { return x_min_ + length_ - 1; };

  void setMinLength(int x_min)
  { x_min_ = x_min; }

  void setMinWidth(int y_min)
  { y_min_ = y_min; }

  void setMinHeight(int z_min)
  { z_min_ = z_min; }

  vector<Entity *> getEntities()
  { return entities_; }

  /**
 * @brief Get the status of a position in the Map
 * @param position
 * @return Status of the position or Unknown if the position is not in the Map
 */
  State getStatus(const Position &position) const;

  /**
   * @brief Get the blocker of a position in the Map
   * @param position
   * @return Blocker of the Position, None if the position is not in the Map
   * or Wall if the position is occupied with no entity
   */
  Blocker getBlocker(const Position &position) const;

  /**
   * @brief Set the status of a position in the Map, unless an entity is at the position.
   * If the position does not exist, will add it
   * @param position
   * @param status
   * @throw gameException if the position is not in the Map
   */
  void setStatus(const Position &position, State status);

  /**
 * @brief Check if a Position is in the Map
 * @param position
 * @return
 */
  bool isPositionInArea(const Position &position) const
  {
    return !(position.getX() < x_min_ || position.getX() > getMaxLength() ||
             position.getY() < y_min_ || position.getY() > getMaxWidth() ||
             position.getZ() < z_min_ || position.getZ() > getMaxHeight());
  }

  /**
 * @brief Check if a position is free and if all the positions under position.z + height are free
 * @param position
 * @param height size of the empty space needed
 * @return true if the box has enough space
 */
  bool checkPosition(const Position &position, unsigned int height) const;

  /**
 * @brief Check if an Entity is on the map
 * @param entity
 * @return True if the Entity is on the map, False else.
 */
  bool hasEntity(const Entity *entity) const;

  /**
* @brief Check if there is an Entity at a defined position on the map
* @param position
* @return True if there is an Entity at the position on the map, False else.
*/
  bool hasEntity(const Position &position) const;

  /**
 * @brief Retrieve Entities at a specific position.
 * @param position Position of the entity.
 * @return Entities located at the position if any
 */
  vector<Entity *> getEntity(const Position &position);

  /**
   * @brief Return the position of an entity on the map
   * @param entity 
   * @throw ARGUMENT_ERROR if the entity is not on the map.
   * @return Position of the entity on the map.
   */
  Position getEntityPosition(const Entity* entity)const noexcept(false);

  /**
   * @brief Copies an entity on the map, according to its position.
   * If force_position is false, will place it to the closest position available on the map if any is available.
   * @param entity
   * @param force_position If true, will test only the provided position.
   * @param copy if True the entity will be copied on the maps else it will be passed by reference
   * @throw ARGUMENT_ERROR if the entity coordinates are not in the boundaries of the Map
   * CANT_DO_ERROR if the Map is completely full or if something went wrong while placing the entity
   */
  void addEntity(Entity *entity, bool force_position = false, bool copy=true) noexcept(false);

  /**
   * @brief Remove an Entity from the map if it exists.
   * @param entity
   * @param to_delete If true, will delete all matching entities, else just remove them from the entities list.
   */
  void removeEntity(const Entity *entity, bool to_delete = true);

  /**
   * @brief Change the position on the Map of the entity
   * @param entity entity to move
   * @param position new position for entity
   */
  void moveEntity(Entity *entity, const Position &position) noexcept(false);

  /**
   * @brief Get surrounding boxes around a Position, in a defined radius
   * @param position
   * @param radius
   * @return vector of surrounding Position in the defined radius
   */
  vector<Position> getConnectedBoxes(const Position &position, unsigned int radius) const;

  /**
   * @brief Computes the manhattan distances of each position in the Map with a position.
   * @param position
   * @return map of the manhattan distance associated with the each position of the Map
   */
  map<Position, unsigned int> getManhattanDistances(const Position &position) const;

  /**
   * @brief Remove a position of the map if the map contains it.
   * Will also remove the entity on it if any.
   * @param position
   */
  void removePosition(const Position &position);

  /**
   * @brief Extract a sub-area of the Map according to boundaries.
   * Will also extract the entities on the sub-area
   * @param length_min
   * @param length_max
   * @param width_min
   * @param width_max
   * @param height_min
   * @param height_max
   * @return extracted sub-area
   * @throw ARGUMENT_ERROR if the boundaries provided are not sensible or greater than the Map's
   */
  Map getSubArea(int length_min, int length_max, int width_min, int width_max, int height_min,
                 int height_max) noexcept(false);

  /**
   * @brief Extract the entities which position is in the subArea
   * @param subArea
   * @return List of matching Entities
   */
  vector<Entity *> getEntitiesInArea(Map subArea);

  /**
   * @brief Get the real distance between a 3D position and every position of the Map
   * @param position
   * @return map between each Map's position and their real distance with the target
   */
  vector<pair<Position, double>> getDistances(const Position &position) const;

  /**
   * @brief Return all the positions surrounding a provided one within a certain radius 
   * @param position center Position
   * @param radius 
   * @return List of Positions in radius
   */
  vector<Position> getSurroundingPositions (const Position &position, unsigned int radius) const;

  /**
   * @brief Retrieve the visible Map according to an oriented center position, a maximum distance and angle limits
   * @param position center of the view
   * @param orientation direction of the vision
   * @param radius maximum distance visible
   * @param angle limit centered on the orientation axis
   * @return
   */
  Map getObservedField(const Position &position, const Orientation &orientation,
                       unsigned int radius, float angle);

  /**
   * @brief Retrieve the visible Map according to the entity, a maximum distance and angle limits
   * @param entity
   * @param radius maximum distance visible
   * @param angle limit centered on the orientation axis
   * @return
   */
  Map getObservedField(const Entity *entity, unsigned int radius, float angle)
  {
    return getObservedField(entity->getPosition() + Position(0, 0, int(floor(entity->getSize()))),
                            entity->getDirection(), radius, angle);
  }

  /**
   * @brief Retrieves all the positions matching Area::checkPosition
   * @param height
   * @return List of the available Position
   * @see checkPosition
   */
  vector<Position> getAvailablePositions(unsigned int height = 1) const;

  /**
   * @brief Retrieves the positions where the entity can be placed
   * @param entity Entity to place
   * @return List of the available Position
   */
  vector<Position> getAvailablePositions(Entity *entity) const
  {
    return getAvailablePositions(static_cast<unsigned int>(ceil(entity->getSize())));
  }

  /**
   * @brief Retrieves the positions matching Area::checkPosition
   * and having their bellow position is filled with a blocking Entity
   * @param height
   * @return List of matching Position
   */
  vector<Position> getFloor(unsigned int height) const;

  /**
   * @brief Retrieves the positions where the entity can be places
   * and having their bellow position is filled with a blocking Entity
   * @param entity
   * @return List of matching Position
   */
  vector<Position> getFloor(Entity *entity) const
  {
    return getFloor(static_cast<unsigned int>(ceil(entity->getSize())));
  }

  /**
   * @brief Add a z-layer to the Map, which each box is set with a provided state.
   * @param height
   * @param state
   * @throw ARGUMENT_ERROR if an entity blocks the creation of the layer
   */
  void addLayer(int height, State state = State::Free) noexcept(false);

  /**
   * @brief Add a z-layer to the Map, which each box is set with a provided Entity.
   * @param height
   * @param entity
   * @throw ARGUMENT_ERROR if an entity blocks the creation of the layer
   */
  void addLayer(int height, Entity *entity) noexcept(false);

  /**
 * @brief Remove the z-layer positions of the Map
 * @param height
 */
  void removeLayer(int height);

  /**
   * @brief Add a x-layer to the Map, which each box is set with a provided state.
   * @param length
   * @param state
   */
  void addLength(int length, State state = State::Free);

  /**
   * @brief Add a x-layer axis to the Map, which each box is filled with the entity
   * @param length
   * @param entity
   */
  void addLength(int length, Entity *entity);

  /**
 * @brief Remove the x-layer positions of the Map
 * @param length
 */
  void removeLength(int length);


  /**
   * @brief Add a y-layer to the Map, which each box is set with a provided state.
   * @param width
   * @param state
   */
  void addWidth(int width, State state = State::Free);

  /**
   * @brief Add a y-layer axis to the Map, which each box is filled with the entity
   * @param width
   * @param entity
   */
  void addWidth(int width, Entity *entity);

  /**
 * @brief Remove the y-layer positions of the Map
 * @param width
 */
  void removeWidth(int width);

  bool equals(const Map &rhs) const;

  friend ostream &operator<<(ostream &os, const Map &area);

  void save(Store *writer, bool subclass = false) const override;

  static Object *load(FILE *file);
};


#endif //HOMESICK_AREA_H
