//
// Created by drodarie on 12/3/17.
//

#include <iostream>
#include "gtest/gtest.h"
#include "../../main/models/skill.h"

using namespace std;

TEST(SkillTest, BasicTest)
{
  string name1 = "Test", description1 = "Test description",
    name2 = "Test2", description2 = "Test description2",
    filename = _TEST_FOLDER_ + "skills.bin";
  Skill skill(name1, description1, filename);
  skill.updateDefaults();
  Skill skill2(name2, description2, filename);

  EXPECT_EQ(skill.getId(), 0);
  EXPECT_EQ(skill2.getId(), 1);
  EXPECT_EQ(skill.getName(), name1);
  EXPECT_EQ(skill.getDescription(), description1);
  EXPECT_TRUE(dynamic_cast<Skill *>(getDefaults()[filename][0].get())->equals(skill));
  stringstream os;
  os << skill;
  string test = "id: 0, filename: " + filename + ", name: " + name1 + ", description: " + description1;
  EXPECT_EQ(test, os.str());
  skill.setName(name2);
  skill.setDescription(description2);
  EXPECT_TRUE(skill2 != skill);
  EXPECT_TRUE(skill2 > skill);
  EXPECT_TRUE(skill2 >= skill);
  EXPECT_EQ(skill2.getDescription(), skill.getDescription());
  EXPECT_EQ(skill2.getName(), skill.getName());
  Skill skill3 = {skill};
  EXPECT_TRUE(skill3.equals(skill));
}

TEST(SkillTest, Store)
{
  string filename = _TEST_FOLDER_ + "skills.bin";

  vector<Skill> skills;
  for (unsigned int i = 0; i < 3; i++)
    skills.emplace_back(i, "Test" + to_string(i), "Description" + to_string(i), filename);
  EXPECT_EQ(*dynamic_cast<Skill *>(getDefaults()[filename][0].get()), skills[0]);
  EXPECT_FALSE(dynamic_cast<Skill *>(getDefaults()[filename][0].get())->equals(skills[0]));
  Skill::save_all(filename, skills);
  EXPECT_EQ(*dynamic_cast<Skill *>(getDefaults()[filename][0].get()), skills[0]);
  EXPECT_TRUE(dynamic_cast<Skill *>(getDefaults()[filename][0].get())->equals(skills[0]));
  vector<Object *> skills2 = Skill::load_all(filename);
  EXPECT_EQ(skills.size(), skills2.size());
  for (unsigned int i = 0; i < skills.size(); i++)
  {
    Skill to_test = *dynamic_cast<Skill *>(skills2[i]);
    EXPECT_EQ(skills[i], to_test);
    EXPECT_EQ(skills[i].getName(), to_test.getName());
    EXPECT_EQ(skills[i].getDescription(), to_test.getDescription());
  }
  for (auto &i: skills2)
    delete (i);
}