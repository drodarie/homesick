/**
 * @file test_mdstar.cpp
 * @author drodarie
 * @date 1/21/19.
*/


#include <iostream>
#include "gtest/gtest.h"
#include "../../main/models/utils/mtdstar.h"
#include "../../main/models/entities/characters/character.h"

using namespace std;

class TestMTDstar : public ::testing::Test
{
protected:
  Entity *floor_, *wall;
  Map *area;
  Position start, target;

  TestMTDstar()
  {
    string filename_ = _TEST_FOLDER_ + "entities_map.bin";
    floor_ = new Entity(2, "Floor", "Dirt and/or grass", 10, 1, {0, 0, 0}, Blocker::Wall, filename_);
    wall = new Entity(3, "Stone Wall", "The results of years of evolution, it's not just a pile of rocks.",
                      10, 1, {3, 1, 1}, Blocker::Wall,filename_);
    area = new Map(0, "MTDstar 1", "Initial map for MTDstar", 7, 7, 1, floor_);
    start = Position(2, 1, 1), target = Position(2, 5, 1);

    area->addLayer(1);
    area->addLength(0, wall);
    area->addLength(6, wall);
    area->addWidth(0, wall);
    area->addWidth(6, wall);
    area->addWidth(4, wall);
    area->removePosition({1, 4, 1});
    area->setStatus({1, 4, 1}, State::Free);
    area->addEntity(wall, true);
    wall->setPosition({3, 2, 1});
    area->addEntity(wall, true);
    wall->setPosition({4, 1, 1});
    area->addEntity(wall, true);
    wall->setPosition({4, 2, 1});
    area->addEntity(wall, true);
    wall->setPosition({5, 1, 1});
    area->addEntity(wall, true);
    wall->setPosition({5, 2, 1});
    area->addEntity(wall, true);
    Object::save_all<Entity>(filename_, {*floor_,*wall});
    Map::save_one(_TEST_FOLDER_ + "mdstar.bin", *area);
  }

  virtual ~TestMTDstar()
  {
    delete (floor_);
    delete (wall);
    delete (area);
  }
};

TEST_F (TestMTDstar, Basic_Functions)
{
  MTDstar pathfinder = MTDstar({2, 0, 1}, target, 1, *area, -1);
  EXPECT_EQ(0, pathfinder.getKm());
  EXPECT_EQ(0, pathfinder.getBasicCost());
  EXPECT_EQ(15, pathfinder.getPositions().size());
  EXPECT_EQ(start, pathfinder.getStart());
  EXPECT_EQ(start, pathfinder.getCurrentPosition());
  EXPECT_EQ(target, pathfinder.getTarget());

  auto nodes = pathfinder.getNodes();
  EXPECT_EQ(nodes.size(), 15);
  for (auto node: nodes)
  {
    if (node.first != start)
      EXPECT_EQ(node.second.rhs_, INT16_MAX);
    else
      EXPECT_EQ(node.second.rhs_, 0);
    EXPECT_LE(node.second.predecessors_.size(), 8);
    EXPECT_LE(node.second.successors_.size(), 8);
    EXPECT_EQ(node.second.successors_.size(), node.second.predecessors_.size());
  }
  pathfinder.setStart({0, 0, 1});
  EXPECT_EQ(pathfinder.getStart(), Position(1, 1, 1));
  pathfinder.setBasicCost(1);
  EXPECT_EQ(1, pathfinder.getBasicCost());
  pathfinder.setBasicCost(-1);
  EXPECT_EQ(0, pathfinder.getBasicCost());
  pathfinder.setStart(start);
  EXPECT_EQ(pathfinder.getStart(), start);
  EXPECT_EQ(pathfinder.getCurrentPosition(), start);
  pathfinder.setTarget({2, 6, 1});
  EXPECT_EQ(pathfinder.getTarget(), target);
  EXPECT_EQ(0, pathfinder.getKm());
  pathfinder.setTarget({3, 5, 1});
  EXPECT_EQ(pathfinder.getTarget(), Position(3, 5, 1));
  EXPECT_EQ(1, pathfinder.getKm());
}

TEST_F (TestMTDstar, ComputeBestPath)
{
  MTDstar pathfinder = MTDstar(start, target, 1, *area);
  pathfinder.computeBestPath();
  auto nodes = pathfinder.getNodes();
  for (auto node: nodes)
  {
    if (node.first.getX() == 1)
    {
      EXPECT_EQ(node.first.getY(), node.second.rhs_);
      EXPECT_EQ(node.first.getY(), node.second.g_);
    } else if (node.first.getX() == 2 && node.first != target)
    {
      EXPECT_EQ(node.first.getY() - 1, node.second.rhs_);
      EXPECT_EQ(node.first.getY() - 1, node.second.g_);
    } else if (node.first == Position(3, 3, 1))
    {
      EXPECT_EQ(node.first.getX(), int(node.second.rhs_));
      EXPECT_EQ(node.first.getX(), int(node.second.g_));
    } else if (node.first == target)
    {
      EXPECT_EQ(INT16_MAX, node.second.g_);
      EXPECT_EQ(6, node.second.rhs_);
    } else
      EXPECT_EQ(INT16_MAX, node.second.g_);
    vector<Position> path(6);
    path[0] = target;
    path[1] = {1, 5, 1};
    path[2] = {1, 4, 1};
    path[3] = {1, 3, 1};
    path[4] = {2, 3, 1};
    path[5] = {2, 2, 1};
    EXPECT_EQ(path, pathfinder.getPath());
    EXPECT_EQ(start, pathfinder.getCurrentPosition());
  }
}

TEST_F(TestMTDstar, TestUpdates)
{
  MTDstar pathfinder = MTDstar(start, target, 2, *area);
  Position updatePos = Position(2, 4, 1);
  pathfinder.update(2, *area);
  EXPECT_EQ(pathfinder.getCurrentPosition(), Position(2, 2, 1));
  area->removePosition(updatePos);
  area->setStatus(updatePos, State::Free);
  pathfinder.update(2, *area);
  EXPECT_EQ(pathfinder.getCurrentPosition(), Position(2, 3, 1));
  pathfinder.setTarget({3, 5, 1});
  pathfinder.update(2, *area);
  EXPECT_EQ(pathfinder.getCurrentPosition(), updatePos);
  area->setStatus(target, State::Occupied);
  pathfinder.update(2, *area);
  EXPECT_EQ(pathfinder.getCost(updatePos, target), INT16_MAX);
  EXPECT_EQ(pathfinder.getCurrentPosition(), updatePos);
  pathfinder.addCost(1, *area, 1);
  EXPECT_EQ(pathfinder.getCost(Position(2, 3, 1), updatePos), 2);
  pathfinder.update(2, *area);
  EXPECT_EQ(pathfinder.getCurrentPosition(), updatePos);
  pathfinder.addCost(1, *area, -1);
  EXPECT_EQ(pathfinder.getCost(Position(2, 3, 1), updatePos), 1);
  pathfinder.update(2, *area);
  EXPECT_EQ(pathfinder.getCurrentPosition(), updatePos);
  area->setStatus({2, 5, 1}, State::Free);
  wall->setPosition(updatePos);
  area->addEntity(wall, true);
}

TEST_F(TestMTDstar, TestNodesAfterUpdate)
{
  MTDstar pathfinder = MTDstar(start, target, 2, *area);
  pathfinder.update(2, *area);
  pathfinder.update(2, *area);
  auto old_path = pathfinder.getPath();
  EXPECT_EQ(old_path.size(), 4);

  area->removePosition({2, 4, 1});
  area->setStatus({2, 4, 1}, State::Free);
  pathfinder.setTarget({3, 5, 1});
  pathfinder.update(2, *area);

  auto nodes = pathfinder.getNodes();
  float j = nodes[pathfinder.getStart()].g_ + 1;
  EXPECT_EQ(j, nodes[Position(2, 4, 1)].g_);
  EXPECT_EQ(j, nodes[Position(2, 4, 1)].rhs_);
  EXPECT_EQ(j, nodes[Position(3, 3, 1)].g_);
  EXPECT_EQ(j, nodes[Position(3, 3, 1)].rhs_);
  EXPECT_EQ(j + 1, nodes[Position(2, 5, 1)].rhs_);
  EXPECT_EQ(j + 1, nodes[Position(2, 5, 1)].g_);

  EXPECT_EQ(INT16_MAX, nodes[Position(2, 2, 1)].g_);
  EXPECT_EQ(j, nodes[Position(2, 2, 1)].rhs_);
  EXPECT_EQ(INT16_MAX, nodes[Position(1, 2, 1)].g_);
  EXPECT_EQ(j + 1, nodes[Position(1, 2, 1)].rhs_);
  EXPECT_EQ(INT16_MAX, nodes[Position(4, 3, 1)].g_);
  EXPECT_EQ(j + 1, nodes[Position(4, 3, 1)].rhs_);
  EXPECT_EQ(INT16_MAX, nodes[pathfinder.getTarget()].g_);
  EXPECT_EQ(j + 2, nodes[pathfinder.getTarget()].rhs_);
  for (auto i = static_cast<unsigned int>(old_path.size() - 1); i > 0; i--)
  {
    auto node = nodes[old_path[i]];
    EXPECT_EQ(node.g_, j);
    EXPECT_EQ(node.rhs_, j);
    j++;
  }
}