/**
 * @file test_movement.cpp
 * @author drodarie
 * @date 7/20/18.
*/

#include <iostream>
#include "gtest/gtest.h"
#include "../../main/models/actions/movement.h"

using namespace std;

TEST(MovementTest, BasicTest)
{
  string filename = _TEST_FOLDER_ + "movements.bin";
  vector<Map*> maps = Map::extract_all<Map>(_TEST_FOLDER_ + "mdstar.bin");
  NamedObject job(2, "Test filename", "Test Job", "Test Description for job");
  Position position(2, 1, 1), target(2,5,1), updatePos = Position(2, 4, 1);
  Buff empty(-1,-1,-1,-1,0,10,0,0.5), buff(40, 75, 40, 0, 5.5, 6.6, 7, 8.8);
  auto action = IAction("Walking", "Use repetitive limb flexion to achieve movement", empty, false, false, filename);
  Character character(1, "Test Character", "Test description for character", 1.1, 1.2, Sex::Male,
                      job, buff, position);
  maps[0]->removePosition(updatePos);
  maps[0]->setStatus(updatePos, State::Free);
  auto walk = Movement(action, &character, vector<Entity*>(), maps[0],0, &target, 0,1);
  EXPECT_EQ(0, walk.getCurrentTime());
  EXPECT_EQ(0, walk.getGlobalBonus());
  EXPECT_EQ(0, walk.getAcceleration());
  walk.setAcceleration(1);
  walk.setGlobalBonus(10);
  EXPECT_EQ(1, walk.getAcceleration());
  EXPECT_EQ(10, walk.getGlobalBonus());
  EXPECT_FALSE(walk.update(10000.0));
  EXPECT_EQ(walk.getCurrentTime(),0);
  EXPECT_EQ(walk.getLocalTime(),0);
  EXPECT_EQ(walk.getCurrentTime(),0);
  EXPECT_THROW(walk.proceed(), ARGUMENT_ERROR);
  maps[0]->addEntity(&character, false, false);
  EXPECT_FALSE(walk.proceed());
  EXPECT_EQ(walk.getResultTest().success, 0);
  EXPECT_EQ(0, walk.getCurrentTime());
  for (int i = 0; i<5; i++) // acceleration
  {
    EXPECT_FALSE(walk.update(0.1));
    EXPECT_EQ(character.getPosition(), position);
    EXPECT_EQ(walk.getLocalTime(), 0);
  }
  walk.setAcceleration(0);
  EXPECT_EQ(0, walk.getAcceleration());
  for (int i = 0; i<5; i++) // walk last distance
  {
    EXPECT_FALSE(walk.update(0.1));
    EXPECT_EQ(character.getPosition(), position);
    EXPECT_EQ(walk.getLocalTime(), 0);
  }
  EXPECT_FALSE(walk.update(1.2));
  EXPECT_NEAR(walk.getLocalTime(), 2.1, 1e-7);
  EXPECT_EQ(character.getPosition(), Position(2, 3, 1));

  target.setX(3);
  EXPECT_FALSE(walk.update(1));
  EXPECT_EQ(character.getPosition(), updatePos);
  maps[0]->setStatus(target, State::Occupied);
  updatePos = {2,5,1};
  EXPECT_TRUE(walk.update(1));
  EXPECT_EQ(character.getPosition(), updatePos);
  EXPECT_EQ(walk.getLocalTarget(), updatePos);
  maps[0]->setStatus(target, State::Free);
  EXPECT_TRUE(walk.update(1));
  EXPECT_EQ(character.getPosition(), target);

  maps[0]->removeEntity(&character, false);
  for(auto map: maps)
    delete map;
}