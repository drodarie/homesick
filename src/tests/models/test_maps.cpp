//
// Created by drodarie on 12/15/17.
//

#include <iostream>
#include "gtest/gtest.h"
#include "../../main/models/maps.h"

using namespace std;

TEST (AreaTest, Basic_Fonctions)
{
  string filename = _TEST_FOLDER_ + "areas.bin";
  Map area("Test Map", "Test Map description", 2, 2, 4, 2, 6, 2, State::Free, filename);
  area.updateDefaults();
  EXPECT_EQ(0, area.getId());
  Map area2("Test Map2", "Test Map description2", 3, 3, 3, State::Free, filename),
    area3("Test Map3", "Test Map description3", 0, 0, 0, State::Unknown, filename);
  EXPECT_EQ(1, area2.getId());
  EXPECT_EQ(1, area3.getId());
  EXPECT_TRUE(dynamic_cast<Map *>(getDefaults()[filename][0].get())->equals(area));
  Position position1{2, 4, 6}, position2{1, 1, 1};
  EXPECT_EQ(2, area.getMinLength());
  EXPECT_EQ(3, area.getMaxLength());
  EXPECT_EQ(4, area.getMinWidth());
  EXPECT_EQ(5, area.getMaxWidth());
  EXPECT_EQ(6, area.getMinHeight());
  EXPECT_EQ(7, area.getMaxHeight());
  EXPECT_EQ(static_cast<unsigned int>(2), area.getLength());
  EXPECT_EQ(static_cast<unsigned int>(2), area.getWidth());
  EXPECT_EQ(static_cast<unsigned int>(2), area.getHeight());
  EXPECT_EQ(State::Free, area.getStatus(position1));
  EXPECT_EQ(Blocker::None, area.getBlocker(position1));
  EXPECT_EQ(area.getStatus(position2), State::Unknown);
  EXPECT_EQ(area.getBlocker(position2), Blocker::None);
  area.setStatus(position1, State::Occupied);
  EXPECT_EQ(State::Occupied, area.getStatus(position1));
  EXPECT_EQ(Blocker::Wall, area.getBlocker(position1));
  EXPECT_TRUE(area2 != area);
  area2 = {area};
  EXPECT_TRUE(area2.equals(area));

  EXPECT_EQ(area3.getMinLength(), 0);
  EXPECT_EQ(area3.getMinWidth(), 0);
  EXPECT_EQ(area3.getMinHeight(), 0);
  EXPECT_EQ(area3.getMaxLength(), 0);
  EXPECT_EQ(area3.getMaxWidth(), 0);
  EXPECT_EQ(area3.getMaxHeight(), 0);
  area3.setMinLength(1);
  area3.setMinWidth(1);
  area3.setMinHeight(1);
  EXPECT_EQ(area3.getMinLength(), 1);
  EXPECT_EQ(area3.getMinWidth(), 1);
  EXPECT_EQ(area3.getMinHeight(), 1);
  EXPECT_EQ(area3.getMaxLength(), 1);
  EXPECT_EQ(area3.getMaxWidth(), 1);
  EXPECT_EQ(area3.getMaxHeight(), 1);
}

TEST(AreaTest, Entities)
{
  Map area(1, "Test Map", "Test Map description", 2, 1, 4, 2, 6, 1, State::Free);
  Position position(2, 4, 6), position2(1, 1, 1);
  Entity *entity = new Entity(1,
                              "Test entity",
                              "Test description for entity",
                              static_cast<float>(2.2), static_cast<float>(1.2), position, Blocker::Breakable),
    *entity2 = new Entity(2,
                          "Test entity2",
                          "Test description for entity2",
                          static_cast<float>(2.2), static_cast<float>(1.2), position2, Blocker::Wall),
    *entity3 = new Entity(3,
                          "Test entity3",
                          "Test description for entity3",
                          static_cast<float>(2.2), static_cast<float>(1.2), position2, Blocker::Wall);
  EXPECT_EQ(vector<Entity *>(), area.getEntities());
  EXPECT_FALSE(area.hasEntity(entity));
  EXPECT_THROW(area.getEntityPosition(entity), ARGUMENT_ERROR);
  EXPECT_FALSE(area.hasEntity(position));
  EXPECT_EQ(area.getEntity(position), vector<Entity *>());
  area.addEntity(entity);
  EXPECT_EQ(position, area.getEntityPosition(entity));
  auto entities = area.getEntity(position);
  EXPECT_EQ(1, entities.size());
  EXPECT_TRUE(*entities[0] == *entity);
  delete entities[0];

  area.addEntity(entity);
  EXPECT_EQ(2, area.getEntities().size());
  for (auto &i: area.getEntities())
    EXPECT_TRUE(*entity == *i);
  EXPECT_EQ(State::Occupied, area.getStatus(position));
  EXPECT_EQ(Blocker::Breakable, area.getBlocker(position));
  EXPECT_THROW(area.addEntity(entity2), CANT_DO_ERROR);
  EXPECT_THROW(area.addEntity(entity3), CANT_DO_ERROR);
  EXPECT_TRUE(area.hasEntity(entity));
  EXPECT_FALSE(area.hasEntity(entity2));
  EXPECT_FALSE(area.hasEntity(entity3));

  area.removeEntity(entity);
  area.removeEntity(entity2);
  EXPECT_TRUE(area.hasEntity(entity));
  EXPECT_FALSE(area.hasEntity(entity2));
  area.removeEntity(entity);
  area.removeEntity(entity);
  EXPECT_THROW(area.addEntity(entity3, true), CANT_DO_ERROR);
  area.addEntity(entity3, false, false);
  EXPECT_EQ(position, entity3->getPosition());
  area.removeEntity(entity3, false);
  area.addEntity(entity3, true, false);
  EXPECT_EQ(1, area.getEntities().size());
  EXPECT_TRUE(entity3 == area.getEntities()[0]);
  Map area2(area);
  EXPECT_EQ(area2.getEntities().size(), area.getEntities().size());
  for (unsigned int i = 0; i < area.getEntities().size() && i < area2.getEntities().size(); i++)
    EXPECT_TRUE(*area2.getEntities()[i] == *area.getEntities()[i]);
  area.removeEntity(entity3, false);
  delete (entity);
  delete (entity2);
  delete (entity3);
}

TEST(AreaTest, Add_RemovePosition)
{
  Map area(0, "Test Map", "Test Map description", 0, 0, 0);
  Position pos = {1, 2, 3}, pos2{0, 3, 3};
  Entity *entity = new Entity(1,
                              "Test entity",
                              "Test description for entity",
                              static_cast<float>(2.2), static_cast<float>(1.2), pos, Blocker::Movable),
    *entity2 = new Entity(2,
                          "Test entity2",
                          "Test description for entity2",
                          static_cast<float>(2.2), static_cast<float>(1.2), pos2, Blocker::Movable);
  area.setStatus(pos, State::Free);
  EXPECT_EQ(area.getMinLength(), 0);
  EXPECT_EQ(area.getMinWidth(), 0);
  EXPECT_EQ(area.getMinHeight(), 0);
  EXPECT_EQ(area.getMaxLength(), 1);
  EXPECT_EQ(area.getMaxWidth(), 2);
  EXPECT_EQ(area.getMaxHeight(), 3);
  EXPECT_EQ(area.getStatus(pos), State::Free);
  EXPECT_EQ(area.getBlocker(pos), Blocker::None);
  area.setStatus(pos2, State::Free);
  EXPECT_EQ(area.getMinLength(), 0);
  EXPECT_EQ(area.getMinWidth(), 0);
  EXPECT_EQ(area.getMinHeight(), 0);
  EXPECT_EQ(area.getMaxLength(), 1);
  EXPECT_EQ(area.getMaxWidth(), 3);
  EXPECT_EQ(area.getMaxHeight(), 3);
  area.addEntity(entity);
  area.addEntity(entity2);
  EXPECT_TRUE(area.hasEntity(entity));
  EXPECT_TRUE(area.hasEntity(entity2));
  area.removePosition(pos);
  EXPECT_EQ(area.getMinLength(), 0);
  EXPECT_EQ(area.getMinWidth(), 0);
  EXPECT_EQ(area.getMinHeight(), 0);
  EXPECT_EQ(area.getMaxLength(), 1);
  EXPECT_EQ(area.getMaxWidth(), 3);
  EXPECT_EQ(area.getMaxHeight(), 3);
  EXPECT_FALSE(area.hasEntity(entity));
  EXPECT_TRUE(area.hasEntity(entity2));
  area.removePosition(pos2);
  EXPECT_EQ(area.getMinLength(), 0);
  EXPECT_EQ(area.getMinWidth(), 0);
  EXPECT_EQ(area.getMinHeight(), 0);
  EXPECT_EQ(area.getMaxLength(), 1);
  EXPECT_EQ(area.getMaxWidth(), 3);
  EXPECT_EQ(area.getMaxHeight(), 3);
  EXPECT_FALSE(area.hasEntity(entity2));
  delete (entity);
  delete (entity2);
}

TEST(AreaTest, Add_RemoveLayer)
{
  Map area(0, "Test Map", "Test Map description", 2, 4, 0, State::Occupied);
  Position pos = {1, 2, 1}, pos2{0, 3, 1}, pos3{0, 3, -1};
  Entity *entity = new Entity(1,
                              "Test entity",
                              "Test description for entity",
                              static_cast<float>(2.2), static_cast<float>(1.2), pos, Blocker::Breakable),
    *entity2 = new Entity(2,
                          "Test entity2",
                          "Test description for entity2",
                          static_cast<float>(2.2), static_cast<float>(1.2), pos2, Blocker::Breakable);
  area.addLayer(1, State::Occupied);
  EXPECT_EQ(area.getMinLength(), 0);
  EXPECT_EQ(area.getMinWidth(), 0);
  EXPECT_EQ(area.getMinHeight(), 0);
  EXPECT_EQ(area.getMaxLength(), 1);
  EXPECT_EQ(area.getMaxWidth(), 3);
  EXPECT_EQ(area.getMaxHeight(), 1);
  EXPECT_EQ(area.getStatus(pos), State::Occupied);
  EXPECT_THROW(area.addEntity(entity), CANT_DO_ERROR);
  area.setStatus(pos2, State::Free);
  area.addEntity(entity);
  EXPECT_TRUE(area.hasEntity(entity));
  EXPECT_TRUE(area.getEntities()[0]->getPosition() == pos2);
  area.addLayer(-1, State::Free);
  area.setStatus({0, 3, 0}, State::Unknown);
  area.addEntity(entity2);
  EXPECT_EQ(area.getMinLength(), 0);
  EXPECT_EQ(area.getMinWidth(), 0);
  EXPECT_EQ(area.getMinHeight(), -1);
  EXPECT_EQ(area.getMaxLength(), 1);
  EXPECT_EQ(area.getMaxWidth(), 3);
  EXPECT_EQ(area.getMaxHeight(), 1);
  EXPECT_TRUE(area.hasEntity(entity));
  EXPECT_TRUE(area.hasEntity(entity2));
  EXPECT_THROW(area.addLayer(1, State::Occupied), ARGUMENT_ERROR);
  vector<Entity *> entities = area.getEntity(pos3);
  EXPECT_EQ(1, entities.size());
  EXPECT_TRUE(*entity2 == *entities[0]);
  delete entities[0];
  EXPECT_TRUE(area.getEntities()[1]->getPosition() == pos3);
  area.removeLayer(-2);
  area.removeLayer(0);
  for (int i = area.getMinLength(); i <= area.getMaxLength(); i++)
    for (int j = area.getMinWidth(); j <= area.getMaxWidth(); j++)
      EXPECT_EQ(area.getStatus({i, j, 0}), State::Unknown);
  area.removeLayer(-1);
  EXPECT_FALSE(area.hasEntity(entity2));
  area.removeLayer(1);
  EXPECT_FALSE(area.hasEntity(entity));
  EXPECT_EQ(area.getMinLength(), 0);
  EXPECT_EQ(area.getMinWidth(), 0);
  EXPECT_EQ(area.getMinHeight(), 0);
  EXPECT_EQ(area.getMaxLength(), 1);
  EXPECT_EQ(area.getMaxWidth(), 3);
  EXPECT_EQ(area.getMaxHeight(), 0);

  area.removeLayer(0);
  EXPECT_EQ(area.getMinLength(), 0);
  EXPECT_EQ(area.getMinWidth(), 0);
  EXPECT_EQ(area.getMinHeight(), 0);
  EXPECT_EQ(area.getMaxLength(), 0);
  EXPECT_EQ(area.getMaxWidth(), 0);
  EXPECT_EQ(area.getMaxHeight(), 0);
  EXPECT_EQ(area.getStatus({0, 0, 0}), State::Unknown);

  area.addLayer(1, entity);
  EXPECT_EQ(1, entities.size());
  EXPECT_EQ(*entity, *area.getEntities()[0]);
  EXPECT_THROW(area.addLayer(1, entity), ARGUMENT_ERROR);
  delete (entity);
  delete (entity2);
}

TEST(AreaTest, Add_RemoveLength)
{
  Map area(0, "Test Map", "Test Map description", 0, 4, 5, State::Occupied);
  Position pos = {1, 2, 3}, pos2{1, 3, 4}, pos3{-1, 3, 4};
  Entity *entity = new Entity(1,
                              "Test entity",
                              "Test description for entity",
                              static_cast<float>(2.2), static_cast<float>(1.2), pos, Blocker::Wall),
    *entity2 = new Entity(2,
                          "Test entity2",
                          "Test description for entity2",
                          static_cast<float>(2.2), static_cast<float>(1.2), pos2, Blocker::Breakable);
  area.addLength(1, State::Occupied);
  EXPECT_EQ(area.getMinLength(), 0);
  EXPECT_EQ(area.getMinWidth(), 0);
  EXPECT_EQ(area.getMinHeight(), 0);
  EXPECT_EQ(area.getMaxLength(), 1);
  EXPECT_EQ(area.getMaxWidth(), 3);
  EXPECT_EQ(area.getMaxHeight(), 4);
  EXPECT_EQ(area.getStatus(pos), State::Occupied);
  EXPECT_THROW(area.addEntity(entity), CANT_DO_ERROR);
  area.setStatus(pos2, State::Free);
  area.addEntity(entity);
  EXPECT_TRUE(area.hasEntity(entity));
  EXPECT_TRUE(area.getEntities()[0]->getPosition() == pos2);
  area.addLength(-1, State::Free);
  area.addEntity(entity2);
  EXPECT_EQ(area.getMinLength(), -1);
  EXPECT_EQ(area.getMinWidth(), 0);
  EXPECT_EQ(area.getMinHeight(), 0);
  EXPECT_EQ(area.getMaxLength(), 1);
  EXPECT_EQ(area.getMaxWidth(), 3);
  EXPECT_EQ(area.getMaxHeight(), 4);
  EXPECT_TRUE(area.hasEntity(entity));
  EXPECT_TRUE(area.hasEntity(entity2));
  vector<Entity *> entities = area.getEntity(pos3);
  EXPECT_EQ(1, entities.size());
  EXPECT_TRUE(*entity2 == *entities[0]);
  delete entities[0];
  EXPECT_TRUE(area.getEntities()[1]->getPosition() == pos3);
  area.removeLength(-2);
  area.removeLength(0);
  for (int i = area.getMinWidth(); i <= area.getMaxWidth(); i++)
    for (int j = area.getMinHeight(); j <= area.getMaxHeight(); j++)
      EXPECT_EQ(area.getStatus({0, i, j}), State::Unknown);
  area.removeLength(-1);
  EXPECT_FALSE(area.hasEntity(entity2));
  area.removeLength(1);
  EXPECT_FALSE(area.hasEntity(entity));
  EXPECT_EQ(area.getMinLength(), 0);
  EXPECT_EQ(area.getMinWidth(), 0);
  EXPECT_EQ(area.getMinHeight(), 0);
  EXPECT_EQ(area.getMaxLength(), 0);
  EXPECT_EQ(area.getMaxWidth(), 3);
  EXPECT_EQ(area.getMaxHeight(), 4);
  area.removeLength(0);
  EXPECT_EQ(area.getMinLength(), 0);
  EXPECT_EQ(area.getMinWidth(), 0);
  EXPECT_EQ(area.getMinHeight(), 0);
  EXPECT_EQ(area.getMaxLength(), 0);
  EXPECT_EQ(area.getMaxWidth(), 0);
  EXPECT_EQ(area.getMaxHeight(), 0);
  EXPECT_EQ(area.getStatus({0, 0, 0}), State::Unknown);

  area.addLength(1, entity);
  EXPECT_EQ(1, entities.size());
  EXPECT_EQ(*entity, *area.getEntities()[0]);
  area.addLength(1, entity);
  EXPECT_EQ(1, entities.size());
  EXPECT_EQ(*entity, *area.getEntities()[0]);
  delete (entity);
  delete (entity2);
}

TEST(AreaTest, Add_RemoveWidth)
{
  Map area(0, "Test Map", "Test Map description", 2, 0, 4, State::Occupied);
  Position pos = {1, 1, 3}, pos2{0, 1, 3}, pos3{0, -1, 3};
  Entity *entity = new Entity(1,
                              "Test entity",
                              "Test description for entity",
                              static_cast<float>(2.2), static_cast<float>(1.2), pos, Blocker::Breakable),
    *entity2 = new Entity(2,
                          "Test entity2",
                          "Test description for entity2",
                          static_cast<float>(2.2), static_cast<float>(1.2), pos2, Blocker::Movable);
  area.addWidth(1, State::Occupied);
  EXPECT_EQ(area.getMinLength(), 0);
  EXPECT_EQ(area.getMinWidth(), 0);
  EXPECT_EQ(area.getMinHeight(), 0);
  EXPECT_EQ(area.getMaxLength(), 1);
  EXPECT_EQ(area.getMaxWidth(), 1);
  EXPECT_EQ(area.getMaxHeight(), 3);
  EXPECT_EQ(area.getStatus(pos), State::Occupied);
  EXPECT_THROW(area.addEntity(entity), CANT_DO_ERROR);
  area.setStatus(pos2, State::Free);
  area.addEntity(entity);
  EXPECT_TRUE(area.hasEntity(entity));
  EXPECT_TRUE(area.getEntities()[0]->getPosition() == pos2);
  area.addWidth(-1, State::Free);
  area.addEntity(entity2);
  EXPECT_EQ(area.getMinLength(), 0);
  EXPECT_EQ(area.getMinWidth(), -1);
  EXPECT_EQ(area.getMinHeight(), 0);
  EXPECT_EQ(area.getMaxLength(), 1);
  EXPECT_EQ(area.getMaxWidth(), 1);
  EXPECT_EQ(area.getMaxHeight(), 3);
  EXPECT_TRUE(area.hasEntity(entity));
  EXPECT_TRUE(area.hasEntity(entity2));
  vector<Entity *> entities = area.getEntity(pos3);
  EXPECT_EQ(1, entities.size());
  EXPECT_TRUE(*entity2 == *entities[0]);
  delete entities[0];
  EXPECT_TRUE(area.getEntities()[1]->getPosition() == pos3);
  area.removeWidth(-2);
  area.removeWidth(0);
  for (int i = area.getMinLength(); i <= area.getMaxLength(); i++)
    for (int j = area.getMinHeight(); j <= area.getMaxHeight(); j++)
      EXPECT_EQ(area.getStatus({i, 0, j}), State::Unknown);
  area.removeWidth(-1);
  EXPECT_FALSE(area.hasEntity(entity2));
  area.removeWidth(1);
  EXPECT_FALSE(area.hasEntity(entity));
  EXPECT_EQ(area.getMinLength(), 0);
  EXPECT_EQ(area.getMinWidth(), 0);
  EXPECT_EQ(area.getMinHeight(), 0);
  EXPECT_EQ(area.getMaxLength(), 1);
  EXPECT_EQ(area.getMaxWidth(), 0);
  EXPECT_EQ(area.getMaxHeight(), 3);
  area.removeWidth(0);
  EXPECT_EQ(area.getMinLength(), 0);
  EXPECT_EQ(area.getMinWidth(), 0);
  EXPECT_EQ(area.getMinHeight(), 0);
  EXPECT_EQ(area.getMaxLength(), 0);
  EXPECT_EQ(area.getMaxWidth(), 0);
  EXPECT_EQ(area.getMaxHeight(), 0);
  EXPECT_EQ(area.getStatus({0, 0, 0}), State::Unknown);

  area.addWidth(1, entity);
  EXPECT_EQ(1, entities.size());
  EXPECT_EQ(*entity, *area.getEntities()[0]);
  area.addWidth(1, entity);
  EXPECT_EQ(1, entities.size());
  EXPECT_EQ(*entity, *area.getEntities()[0]);
  delete (entity);
  delete (entity2);
}

TEST(AreaTest, GetSubArea)
{
  Map area(1, "Test Map", "Test Map description", 2, 2, 4, 2, 6, 2, State::Free),
    subArea2(2, "Test SubArea", "Test SubArea description", 2, 1, 4, 2, 6, 1, State::Free);
  Position position(2, 4, 6), position2{2, 5, 7};
  Entity *entity = new Entity(1,
                              "Test entity",
                              "Test description for entity",
                              static_cast<float>(2.2), static_cast<float>(1.2), position, Blocker::Movable),
    *entity2 = new Entity(2,
                          "Test entity2",
                          "Test description for entity2",
                          static_cast<float>(2.2), static_cast<float>(1.2), position2, Blocker::Breakable),
    *entity3 = new Entity(3,
                          "Test entity3",
                          "Test description for entity3",
                          static_cast<float>(2.2), static_cast<float>(1.2), position, Blocker::Wall);
  area.addEntity(entity);
  area.addEntity(entity2);
  area.addEntity(entity3);
  EXPECT_THROW(area.getSubArea(3, 2, 4, 5, 6, 6), ARGUMENT_ERROR);
  EXPECT_THROW(area.getSubArea(1, 2, 4, 5, 6, 6), ARGUMENT_ERROR);
  EXPECT_THROW(area.getSubArea(2, 4, 4, 5, 6, 6), ARGUMENT_ERROR);

  EXPECT_THROW(area.getSubArea(2, 3, 5, 4, 6, 6), ARGUMENT_ERROR);
  EXPECT_THROW(area.getSubArea(2, 3, 3, 4, 6, 6), ARGUMENT_ERROR);
  EXPECT_THROW(area.getSubArea(2, 3, 4, 6, 6, 6), ARGUMENT_ERROR);

  EXPECT_THROW(area.getSubArea(2, 3, 4, 5, 7, 6), ARGUMENT_ERROR);
  EXPECT_THROW(area.getSubArea(2, 3, 4, 5, 5, 6), ARGUMENT_ERROR);
  EXPECT_THROW(area.getSubArea(2, 3, 4, 5, 6, 8), ARGUMENT_ERROR);

  Map subArea = area.getSubArea(2, 2, 4, 5, 6, 6);
  EXPECT_EQ(subArea.getName(), "Test Map");
  EXPECT_EQ(subArea.getDescription(), "Test Map description");
  EXPECT_EQ(subArea.getMinLength(), 2);
  EXPECT_EQ(subArea.getMaxLength(), 2);
  EXPECT_EQ(subArea.getMinWidth(), 4);
  EXPECT_EQ(subArea.getMaxWidth(), 5);
  EXPECT_EQ(subArea.getMinHeight(), 6);
  EXPECT_EQ(subArea.getMaxHeight(), 6);
  EXPECT_EQ(subArea.getId(), static_cast<unsigned int>(1));
  EXPECT_EQ(1, subArea.getEntities().size());
  EXPECT_TRUE(*entity == *area.getEntities()[0]);
  EXPECT_EQ(subArea.getStatus(position), area.getStatus(position));

  EXPECT_TRUE(area.hasEntity(entity));
  EXPECT_TRUE(area.hasEntity(entity2));
  EXPECT_TRUE(area.hasEntity(entity3));
  vector<Entity *> entities = area.getEntitiesInArea(subArea2);
  EXPECT_EQ(entities.size(), 1);
  EXPECT_TRUE(*entities[0] == *entity);
  area.removeEntity(entity);
  area.removeEntity(entity2);
  area.removeEntity(entity3);
  EXPECT_EQ(1, subArea.getEntities().size());
  EXPECT_TRUE(*entity == *subArea.getEntities()[0]);
  delete (entity);
  delete (entity2);
  delete (entity3);
}

TEST(AreaTest, Store)
{
  string filename = _TEST_FOLDER_ + "areas.bin", filename2 = _TEST_FOLDER_ + "entities.bin";
  vector<Map> areas;
  vector<Entity> entities;
  for (unsigned int i = 0; i < 3; i++)
  {
    areas.emplace_back(i, "Test Map " + to_string(i), "Test Map description " + to_string(i),
                       i, i, i, i, i, i, State::Free, filename);
    Position position(i, i, i);
    entities.emplace_back(i,
                                "Test entity" + to_string(i),
                                "Test description for entity" + to_string(i),
                                static_cast<float>(i + i * 0.1), static_cast<float>(1 + i * 0.1),
                                position, Blocker::Movable, filename2);
    areas[i].addEntity(&entities[i]);
  }

  stringstream os, ent;
  os << areas[1];
  ent << Entity(1,
                "Test entity1",
                "Test description for entity1", 1.1, 1.1,
                {1, 1, 1}, Blocker::Movable, filename2);
  string test = "id: 1, filename: " + filename + ", name: Test Map 1, " +
                "description: Test Map description 1,\n" +
                "x_min: 1, length: 1, y_min: 1, width: 1, z_min: 1, height: 1,\n" +
                "entities: {\n\t{" + ent.str() + "},}";

  EXPECT_EQ(test, os.str());

  EXPECT_EQ(*dynamic_cast<Map *>(getDefaults()[filename][0].get()), areas[0]);
  EXPECT_FALSE(dynamic_cast<Map *>(getDefaults()[filename][0].get())->equals(areas[0]));
  Object::save_all<Entity>(filename2, entities);
  Map::save_all(filename, areas);
  EXPECT_EQ(*dynamic_cast<Map *>(getDefaults()[filename][0].get()), areas[0]);
  EXPECT_TRUE(dynamic_cast<Map *>(getDefaults()[filename][0].get())->equals(areas[0]));
  vector<Object *> areas2 = Map::load_all(filename);
  EXPECT_EQ(areas.size(), areas2.size());
  for (unsigned int i = 0; i < areas.size(); i++)
    EXPECT_TRUE(dynamic_cast<Map *>(areas2[i])->equals(areas[i]));
  for (auto &i: areas2)
    delete (i);
}

TEST(AreaTest, ConnectedBoxes)
{
  Map area(0, "Test Map", "Test Map description", 1, 3, 1, 3, 0, 3);
  Position position(2, 2, 1), position2(0, 0, -1);
  EXPECT_EQ(vector<Position>(), area.getConnectedBoxes(position, 0));
  EXPECT_EQ(27, area.getConnectedBoxes(position, 1).size());
  EXPECT_EQ(1, area.getConnectedBoxes(position2, 1).size());
  EXPECT_EQ(8, area.getConnectedBoxes(position2, 2).size());
  EXPECT_EQ(27, area.getConnectedBoxes(position2, 3).size());
}

TEST(AreaTest, Distances)
{
  Map area(0, "Test Map", "Test Map description", 1, 1, 1);
  Position position(1, 1, 1), position2(0, 0, 0);
  auto distances = area.getDistances(position);
  EXPECT_EQ(distances.size(), 1);
  EXPECT_EQ(position2, distances[0].first);
  EXPECT_NEAR(distances[0].second, sqrt(3.0), 1e-6);
}

TEST(AreaTest, MoveEntity)
{
  Map area(0, "Test Map", "Test Map description", 3, 1, 1, State::Free);
  Position position(0, 0, 0), position2{1, 0, 0}, position3{2, 0, 0};
  Entity *entity = new Entity(1,
                              "Test entity",
                              "Test description for entity",
                              static_cast<float>(2.2), static_cast<float>(1.2), position, Blocker::Movable),
    *entity2 = new Entity(2,
                          "Test entity2",
                          "Test description for entity2",
                          static_cast<float>(2.2), static_cast<float>(1.2), position2, Blocker::Crossable),
    *entity3 = new Entity(3,
                          "Test entity3",
                          "Test description for entity3",
                          static_cast<float>(2.2), static_cast<float>(1.2), position, Blocker::Wall);
  area.addEntity(entity);
  area.addEntity(entity2);

  EXPECT_THROW(area.moveEntity(entity3, position3), ARGUMENT_ERROR);
  EXPECT_THROW(area.moveEntity(entity, {-1, 0, 0}), ARGUMENT_ERROR);
  area.moveEntity(entity, position2);
  vector<Entity *> entities = area.getEntity(position2);
  EXPECT_EQ(2, entities.size());
  EXPECT_TRUE(*entities[1] == *entity2);
  EXPECT_TRUE(*entities[0] == *entity);
  EXPECT_EQ(position2, entities[1]->getPosition());
  for (auto &i: entities)
    delete (i);

  area.addEntity(entity3);
  area.moveEntity(entity, position3);
  entities = area.getEntity(position2);
  EXPECT_EQ(1, entities.size());
  EXPECT_TRUE(*entities[0] == *entity2);
  delete (entities[0]);
  entities = area.getEntity(position3);
  EXPECT_EQ(1, entities.size());
  EXPECT_TRUE(*entities[0] == *entity);
  delete (entities[0]);

  EXPECT_THROW(area.moveEntity(entity, position), ARGUMENT_ERROR);
  delete (entity);
  delete (entity2);
  delete (entity3);
}

TEST(AreaTest, GetObservedField)
{
  Entity *floor = new Entity(2, "Floor", "Dirt and/or grass", 10, 1, {0, 0, 0}, Blocker::Wall),
    *wall = new Entity(2, "Stone Wall", "The results years of evolution, it's not just a pile of rocks.",
                       10, 1, {0, 0, 0}, Blocker::Wall);
  auto area = Map(1, "Test Map", "Test Map description", 5, 5, 1, floor);
  area.addLayer(1, State::Free);
  area.addLength(1, wall);
  area.addLayer(2, State::Free);
  area.removePosition({1, 0, 1});
  area.removePosition({1, 4, 1});
  Map result = area.getObservedField({0, 2, 1}, Orientation::Left, 0, static_cast<float>(M_PI));
  EXPECT_EQ(result.getLength(), 1);
  EXPECT_EQ(result.getWidth(), 1);
  EXPECT_EQ(result.getHeight(), 1);
  EXPECT_EQ(result.getMinLength(), 0);
  EXPECT_EQ(result.getMinWidth(), 2);
  EXPECT_EQ(result.getMinHeight(), 1);
  EXPECT_EQ(vector<Entity *>(), result.getEntities());
  result = area.getObservedField({0, 2, 1}, Orientation::Left, 1, static_cast<float>(M_PI));
  EXPECT_EQ(result.getLength(), 1);
  EXPECT_EQ(result.getWidth(), 3);
  EXPECT_EQ(result.getHeight(), 3);
  EXPECT_EQ(result.getMinLength(), 0);
  EXPECT_EQ(result.getMinWidth(), 1);
  EXPECT_EQ(result.getMinHeight(), 0);
  EXPECT_EQ(1, result.getEntities().size());
  EXPECT_EQ(*floor, *result.getEntities()[0]);
  result = area.getObservedField({0, 2, 1}, Orientation::Right, 1, static_cast<float>(M_PI));
  EXPECT_EQ(result.getLength(), 2);
  EXPECT_EQ(result.getWidth(), 3);
  EXPECT_EQ(result.getHeight(), 3);
  EXPECT_EQ(result.getMinLength(), 0);
  EXPECT_EQ(result.getMinWidth(), 1);
  EXPECT_EQ(result.getMinHeight(), 0);
  EXPECT_EQ(2, result.getEntities().size());
  EXPECT_EQ(*wall, *result.getEntities()[0]);
  EXPECT_EQ(*floor, *result.getEntities()[1]);

  result = area.getObservedField({5, 2, 1}, Orientation::Left, 6, static_cast<float>(M_PI));
  EXPECT_EQ(result.getLength(), 6);
  EXPECT_EQ(result.getWidth(), 5);
  EXPECT_EQ(result.getHeight(), 3);
  EXPECT_EQ(result.getMinLength(), 0);
  EXPECT_EQ(result.getMinWidth(), 0);
  EXPECT_EQ(result.getMinHeight(), 0);
  EXPECT_EQ(16, result.getEntities().size());

  auto entity = Entity(0, "test entity", "Test entity description", 1.0, 1.6, {5, 2, 1});
  entity.setDirection(Orientation::Left);
  result = area.getObservedField(&entity, 6, static_cast<float>(M_PI));
  EXPECT_EQ(result.getLength(), 6);
  EXPECT_EQ(result.getWidth(), 5);
  EXPECT_EQ(result.getHeight(), 3);
  EXPECT_EQ(result.getMinLength(), 0);
  EXPECT_EQ(result.getMinWidth(), 0);
  EXPECT_EQ(result.getMinHeight(), 0);
  EXPECT_EQ(23, result.getEntities().size());

  delete (floor);
  delete (wall);
}

TEST(AreaTest, getFloor)
{
  string filename = _TEST_FOLDER_ + "entities_map.bin";
  Entity *floor_ = new Entity(2, "Floor", "Dirt and/or grass", 10, 1, {0, 0, 0}, Blocker::Wall, filename),
    *wall = new Entity(3, "Stone Wall", "The results of years of evolution, it's not just a pile of rocks.",
                       10, 1, {3, 1, 1}, Blocker::Wall, filename);
  auto area = Map(0, "Test Map", "Test Map description", 7, 7, 1, floor_);
  auto start = Position(2, 1, 1), target = Position(2, 5, 1), hole = Position(1, 1, 0);

  area.addLayer(1);
  area.addLength(0, wall);
  area.addLength(6, wall);
  area.addWidth(0, wall);
  area.addWidth(6, wall);
  area.addWidth(4, wall);
  area.removePosition({1, 4, 1});
  area.setStatus({1, 4, 1}, State::Free);
  area.addEntity(wall, true);
  wall->setPosition({3, 2, 1});
  area.addEntity(wall, true);
  wall->setPosition({4, 1, 1});
  area.addEntity(wall, true);
  wall->setPosition({4, 2, 1});
  area.addEntity(wall, true);
  wall->setPosition({5, 1, 1});
  area.addEntity(wall, true);
  wall->setPosition({5, 2, 1});
  area.addEntity(wall, true);
  area.removePosition(hole);
  area.setStatus(hole, State::Free);
  area.addLayer(2, wall);
  Object::save_all<Entity>(filename, vector<Entity>({*floor_, *wall}));
  Map::save_one(_TEST_FOLDER_ + "solo_map.bin", area);
  vector<Map *> maps = Object::extract_all<Map>(_TEST_FOLDER_ + "solo_map.bin");
  EXPECT_EQ(maps.size(), 1);
  EXPECT_TRUE(maps[0]->equals(area));

  vector<Position> positions = area.getAvailablePositions(wall), positions2 = area.getAvailablePositions(2),
    positions3 = area.getFloor(wall), positions4 = area.getFloor(2);
  EXPECT_EQ(vector<Position>(), positions4);
  EXPECT_EQ(14, positions3.size());
  EXPECT_EQ(vector<Position>(1, hole), positions2);
  EXPECT_EQ(16, positions.size());

  delete (wall);
  delete (floor_);
  for (auto map: maps)
    delete (map);
}