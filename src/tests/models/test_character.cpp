/**
 * @file test_character.cpp
 * @author drodarie
 * @date 6/6/18.
*/

#include <iostream>
#include "gtest/gtest.h"
#include "../../main/models/entities/characters/character.h"

using namespace std;

TEST (CharacterTest, Basic_Fonctions)
{
  string filename = _TEST_FOLDER_ + "characters.bin";
  Buff buff(1, 2, 3, 4, 5.5, 6.6, 7, 8.8),
    op_charac(Buff::_MAX_CHARAC_+1,Buff::_MAX_CHARAC_*2, Buff::_MAX_CHARAC_, Buff::_MAX_CHARAC_-1, 0, 0, 0, 333),
    cutted(Buff::_MAX_CHARAC_,Buff::_MAX_CHARAC_, Buff::_MAX_CHARAC_, Buff::_MAX_CHARAC_ - 1, 0, 0, 0, 333),
    buff2(0, 0, 0, 0, 0, 1, 3, 0),
    buff3(1, 4, 5, 7, 2, 1, 3, 0);
  NamedObject job(2, "Test filename", "Test Job", "Test Description for job"),
    job2(3, "Test filename", "Test Job2", "Test Description for job2");
  Position position(1, 2, 3), position2(2, 3, 4);
  Character character("Test Character", "Test description for character", 1.1, 1.2, Sex::Male,
                      job, buff, position, Blocker::Crossable, 0, filename);
  character.updateDefaults();
  Character character2("Test Character", "Test description for character", 1.1, 1.2, Sex::Male,
                       job, op_charac, position, Blocker::Crossable, 0, filename);
  EXPECT_EQ(static_cast<unsigned int>(0), character.getId());
  EXPECT_TRUE(dynamic_cast<Character *>(getDefaults()[filename][0].get())->equals(character));
  EXPECT_EQ(1, character2.getId());
  EXPECT_EQ(Sex::Male, character.getSex());
  EXPECT_EQ(job, character.getJob());
  EXPECT_EQ(buff, character.getCharacteristics());
  EXPECT_EQ(cutted, character2.getCharacteristics());
  EXPECT_EQ(set<Skill>(), character.getSkills());
  EXPECT_EQ(0, character.getExperience());
  EXPECT_FALSE(character2 == character);

  character.setPosition(position2);
  EXPECT_EQ(position2, character.getPosition());
  character.setJob(job2);
  EXPECT_EQ(job2, character.getJob());
  EXPECT_EQ(0, character.getLevel());
  character.addExperience(10);
  EXPECT_EQ(static_cast<unsigned int>(log(10)), character.getLevel());


  Skill skill(0, "TestSkill", "Test description for Skill"),
    skill1(1, "TestSkill 1", "Test description for Skill 1");
  EXPECT_FALSE(character.hasSkill(skill));
  EXPECT_FALSE(character.hasSkill(skill1));
  character.addSkill(skill);
  set<Skill> skills;
  skills.insert(skill);
  EXPECT_TRUE(character.hasSkill(skill));
  EXPECT_EQ(character.getSkills(), skills);
  character.addSkill(skill);
  EXPECT_TRUE(character.hasSkill(skill));
  EXPECT_EQ(character.getSkills(), skills);
  character.removeSkill(skill1);
  EXPECT_TRUE(character.hasSkill(skill));
  EXPECT_FALSE(character.hasSkill(skill1));
  EXPECT_EQ(character.getSkills(), skills);
  character.removeSkill(skill);
  EXPECT_FALSE(character.hasSkill(skill));
  EXPECT_EQ(character.getSkills(), set<Skill>());

  character.addBuff(buff2);
  character2.addBuff(buff3);
  Item bank(1, "Storage", "Personal storage in Cie", 0.0, 10.0, position, Blocker::Wall, 0, 0);
  Map home(1, "Home of Character", "Home sweet home", 2, 2, 4, 5, 6, 6);
  character2.addProperty(&home);
  character.addProperty(&bank);
  Equipment *chestPlate = new Equipment(10, "Chest plate", "Description for chestplate", 11, 1.0, {10, 10, 10},
                                        Blocker::Crossable, 101, 1001, BodyPart::Torso, 111);
  character2.setInventory(chestPlate, 1);
  character2.setEquipment(chestPlate);
  vector<Equipment *> items;
  for (unsigned int i = 0; i < 10; i++)
  {
    Position positioni(i, i, i);
    items.emplace_back(new Equipment(
      i, "Test item" + to_string(i),
      "Test description for item" + to_string(i),
      static_cast<float>(i + i * 0.1), static_cast<float>(1 + i * 0.1), position, Blocker::Crossable,
      static_cast<float>(i * 10.1), static_cast<float>(i * 100.1), BodyPart(i),
      static_cast<float>(i * 11.1))
    );
    character.setInventory(items[i], 1);
  }
  character2.setInventory(items[0], 2);
  character.setEquipment(items[3]);
  character2 = {character};
  EXPECT_TRUE(character.equals(character2));
  delete chestPlate;
  for (auto &i: items)
    delete (i);
}

TEST(CharacterTest, Buffs)
{
  Buff buff(1, 2, 3, 4, 5.5, 6.6, 7, 8.8),
    inverse_buff(-1, -2, -3, -4, static_cast<float>(-5.5), static_cast<float>(7.6), -7, static_cast<float>(-8.8)),
    buff2 = buff + inverse_buff,
    buff3(0, 0, 0, 0, 0, 0, 3, 0);
  Equipment *chestPlate = new Equipment(10, "Chest plate", "Description for chestplate", 11, 1.0, {10, 10, 10},
                                        Blocker::Crossable, 101, 1001, BodyPart::Torso, 3);
  NamedObject job(2, "Test filename", "Test Job", "Test Description for job");
  Position position(1, 2, 3);
  Character character(1, "Test Character", "Test description for character", 1.1, 1.2, Sex::Male,
                      job, buff, position);
  EXPECT_TRUE(character.isAlive());
  EXPECT_EQ(buff, character.getTotalBuff());
  EXPECT_EQ(vector<Buff>(), character.getBuffs());
  EXPECT_FALSE(character.hasBuff(inverse_buff));
  character.addBuff(inverse_buff);
  EXPECT_TRUE(character.hasBuff(inverse_buff));
  EXPECT_FALSE(character.isAlive());
  EXPECT_EQ(character.getBuffs(), vector<Buff>(1, inverse_buff));
  EXPECT_EQ(character.getTotalBuff(), buff2);
  character.addBuff(inverse_buff);
  inverse_buff.setDuration(2 * inverse_buff.getDuration());
  EXPECT_EQ(character.getBuffs(), vector<Buff>(1, inverse_buff));
  EXPECT_EQ(character.getTotalBuff(), buff2);
  character.update(15.3);

  EXPECT_EQ(buff, character.getTotalBuff());
  EXPECT_EQ(vector<Buff>(), character.getBuffs());
  EXPECT_TRUE(character.isAlive());
  character.setInventory(chestPlate, 1);
  character.setEquipment(chestPlate);
  EXPECT_EQ(buff + buff3, character.getTotalBuff());
  character.storeEquipment(chestPlate->getBodyPart());
  character.addBuff(inverse_buff);
  EXPECT_FALSE(character.isAlive());
  EXPECT_EQ(character.getBuffs(), vector<Buff>(1, inverse_buff));
  EXPECT_EQ(character.getTotalBuff(), buff2);
  character.removeBuff(inverse_buff);
  EXPECT_EQ(buff, character.getTotalBuff());
  EXPECT_EQ(vector<Buff>(), character.getBuffs());
  EXPECT_TRUE(character.isAlive());
  character.buffCharacteristics(inverse_buff);
  EXPECT_EQ(character.getTotalBuff(), buff2);
  EXPECT_FALSE(character.isAlive());
  EXPECT_EQ(vector<Buff>(), character.getBuffs());
  EXPECT_EQ(buff2, character.getCharacteristics());
  delete (chestPlate);
}

TEST(CharacterTest, Properties)
{
  Buff buff(1, 2, 3, 4, 5.5, 6.6, 7, 8.8);
  NamedObject job(2, "Test filename", "Test Job", "Test Description for job");
  Position position(1, 2, 3);
  Character character(1, "Test Character", "Test description for character", 1.1, 1.2, Sex::Female,
                      job, buff, position);
  EXPECT_EQ(vector<NamedObject *>(), character.getProperties());
  Item bank(1, "Storage", "Personal storage in Cie", 0.0, 10.0, position, Blocker::Wall, 0, 0);
  Map home(1, "Home of Character", "Home sweet home", 2, 2, 4, 5, 6, 6);
  EXPECT_FALSE(character.hasProperty(&bank));
  EXPECT_FALSE(character.hasProperty(&home));
  character.addProperty(&bank);
  EXPECT_TRUE(character.hasProperty(&bank));
  EXPECT_FALSE(character.hasProperty(&home));
  EXPECT_EQ(1, character.getProperties().size());
  EXPECT_EQ(bank, *dynamic_cast<Item *>(character.getProperties()[0]));
  character.addProperty(&bank);
  EXPECT_EQ(1, character.getProperties().size());
  EXPECT_EQ(bank, *dynamic_cast<Item *>(character.getProperties()[0]));
  character.addProperty(&home);
  EXPECT_TRUE(character.hasProperty(&bank));
  EXPECT_TRUE(character.hasProperty(&home));
  EXPECT_EQ(2, character.getProperties().size());
  EXPECT_EQ(bank, *dynamic_cast<Item *>(character.getProperties()[0]));
  EXPECT_EQ(home, *dynamic_cast<Map *>(character.getProperties()[1]));
  character.removeProperty(&home);
  EXPECT_EQ(1, character.getProperties().size());
  EXPECT_EQ(bank, *dynamic_cast<Item *>(character.getProperties()[0]));
  character.removeProperty(&home);
  EXPECT_EQ(1, character.getProperties().size());
  EXPECT_EQ(bank, *dynamic_cast<Item *>(character.getProperties()[0]));
  character.removeProperty(&bank);
  EXPECT_EQ(vector<NamedObject *>(), character.getProperties());
  character.addProperty(&home); //Test for valgrind
}

TEST(CharacterTest, Equipment)
{
  vector<Equipment *> items;
  Equipment *chestPlate = new Equipment(10, "Chest plate", "Description for chestplate", 11, 1.0, {10, 10, 10},
                                        Blocker::Crossable, 101, 1001, BodyPart::Torso, 111);
  for (unsigned int i = 0; i < 10; i++)
  {
    Position position(i, i, i);
    items.emplace_back(new Equipment(
      i, "Test item" + to_string(i),
      "Test description for item" + to_string(i),
      static_cast<float>(i + i * 0.1), static_cast<float>(1 + i * 0.1), position, Blocker::Crossable,
      static_cast<float>(i * 10.1), static_cast<float>(i * 100.1), BodyPart(i),
      static_cast<float>(i * 11.1))
    );
  }

  Buff buff(1, 2, 3, 4, 5.5, 6.6, 7, 8.8);
  NamedObject job(2, "Test filename", "Test Job", "Test Description for job");
  Position position(1, 2, 3);
  Character character(1, "Test Character", "Test description for character", 1.1, 1.2, Sex::Male,
                      job, buff, position);

  vector<Equipment *> empty;
  EXPECT_EQ(empty, character.getEquipmentList());

  for (unsigned int i = 0; i < items.size(); i++)
  {
    character.setInventory(items[i], 1);
    EXPECT_FALSE(character.isEquiped(BodyPart(i)));
    EXPECT_FALSE(character.hasEquipment(items[i]));
    EXPECT_TRUE(character.hasItem(items[i]));
    character.setEquipment(items[i]);
    EXPECT_TRUE(character.isEquiped(BodyPart(i)));
    EXPECT_TRUE(character.hasEquipment(items[i]));
    EXPECT_FALSE(character.hasItem(items[i]));
  }

  auto listEquipment = character.getEquipmentList();
  EXPECT_EQ(items.size(), listEquipment.size());
  for (unsigned int i = 0; i < listEquipment.size(); i++)
  {
    EXPECT_EQ(*listEquipment[i], *items[i]);
    EXPECT_EQ(*character.getEquipment(BodyPart(i)), *items[i]);
  }
  character.storeEquipment(BodyPart::Head);
  EXPECT_EQ(items.size() - 1, character.getEquipmentList().size());
  EXPECT_EQ(1, character.getInventory().size());

  EXPECT_TRUE(character.hasItem(items[0]));
  EXPECT_FALSE(character.isEquiped(BodyPart::Head));
  EXPECT_FALSE(character.hasEquipment(items[0]));
  character.setEquipment(chestPlate);
  EXPECT_EQ(*items[int(BodyPart::Torso)], *character.getEquipment(BodyPart::Torso));
  character.setInventory(chestPlate, 2);
  EXPECT_EQ(2, character.getInventory().size());
  character.setEquipment(chestPlate);
  EXPECT_EQ(3, character.getInventory().size());
  EXPECT_TRUE(character.hasItem(chestPlate));
  EXPECT_TRUE(character.hasItem(items[int(BodyPart::Torso)]));
  EXPECT_EQ(*chestPlate, *character.getEquipment(BodyPart::Torso));
  for (auto &i: items)
    delete (i);
  delete (chestPlate);
}

TEST(CharacterTest, Inventory)
{
  vector<Item *> items;
  for (unsigned int i = 0; i < 3; i++)
  {
    Position position(i, i, i);
    items.emplace_back(new Item(
      i, "Test item" + to_string(i),
      "Test description for item" + to_string(i),
      static_cast<float>(i + i * 0.1), static_cast<float>(1 + i * 0.1), position, Blocker(i),
      static_cast<float>(i * 10.1), static_cast<float>(i * 100.1))
    );
  }

  Buff buff(1, 2, 3, 4, 5.5, 6.6, 7, 8.8);
  NamedObject job(2, "Test filename", "Test Job", "Test Description for job");
  Position position(1, 2, 3);
  Character character(1, "Test Character", "Test description for character", 1.1, 1.2, Sex::Male,
                      job, buff, position);

  map<Item *, unsigned int> empty;
  EXPECT_EQ(empty, character.getInventory());

  for (unsigned int i = 0; i < items.size(); i++)
    EXPECT_EQ(i + 1, character.setInventory(items[i], 1 + i));
  auto listInventory = character.getInventory();
  EXPECT_EQ(items.size(), listInventory.size());
  for (auto &i: listInventory)
  {
    EXPECT_EQ(*i.first, *items[i.first->getId()]);
    EXPECT_EQ(i.second, (i.first->getId() + 1));
  }
  EXPECT_TRUE(character.hasItem(items[0]));
  EXPECT_EQ(static_cast<unsigned int>(1), character.getNumberItem(items[0]));
  EXPECT_EQ(static_cast<unsigned int>(1), character.setInventory(items[0], -1));
  EXPECT_FALSE(character.hasItem(items[0]));
  EXPECT_EQ(static_cast<unsigned int>(0), character.getNumberItem(items[0]));
  EXPECT_EQ(static_cast<unsigned int>(0), character.setInventory(items[0], -1));
  EXPECT_EQ(static_cast<unsigned int>(0), character.setInventory(items[0], 0));
  EXPECT_EQ(static_cast<unsigned int>(0), character.getNumberItem(items[0]));
  EXPECT_TRUE(character.hasItem(items[1]));
  EXPECT_EQ(static_cast<unsigned int>(2), character.getNumberItem(items[1]));
  EXPECT_EQ(static_cast<unsigned int>(1), character.setInventory(items[1], -1));
  EXPECT_TRUE(character.hasItem(items[1]));
  EXPECT_EQ(static_cast<unsigned int>(1), character.getNumberItem(items[1]));

  EXPECT_EQ(static_cast<unsigned int>(3), character.getNumberItem(items[2]));
  EXPECT_EQ(static_cast<unsigned int>(3), character.setInventory(items[2], -4));
  EXPECT_FALSE(character.hasItem(items[2]));
  EXPECT_EQ(static_cast<unsigned int>(0), character.getNumberItem(items[2]));
  for (auto &i: items)
    delete (i);
}

TEST(CharacterTest, Store)
{
  string filename = _TEST_FOLDER_ + "characters.bin",
    item_filename = _TEST_FOLDER_ + "items.bin",
    external_file1 = _TEST_FOLDER_ + "items2.bin",
    external_file2 = _TEST_FOLDER_ + "items3.bin";
  Item external_part(0,
                     "External Part",
                     "Test description for external part",
                     static_cast<float>(4 + 4 * 0.1), static_cast<float>(1.1), {4, 4, 4}, Blocker::Movable,
                     static_cast<float>(4 * 10.1), static_cast<float>(4 * 100.1),
                     external_file1),
    external_content(1, "External Content",
                     "Test description for external content",
                     static_cast<float>(5 + 5 * 0.1), static_cast<float>(1.4), {4, 4, 4}, Blocker::Breakable,
                     static_cast<float>(5 * 10.1), static_cast<float>(5 * 100.1),
                     _TEST_FOLDER_ + "items3.bin");
  vector<Skill> skills;
  vector<Buff> buffs;
  vector<Equipment> items;
  for (unsigned int i = 0; i < 3; i++)
  {
    skills.emplace_back(i, "Test" + to_string(i), "Description" + to_string(i));
    buffs.emplace_back(i, i, i, i,
                       static_cast<float>(i + i * 0.1),
                       static_cast<float>(1 + i * 0.1), i,
                       static_cast<float>(i + i * 0.1));
    Position position(i, i, i);
    items.emplace_back(i + 2,
                       "Test Equipment" + to_string(i),
                       "Test description for item" + to_string(i),
                       static_cast<float>(i + i * 0.1), static_cast<float>(1 + i * 0.1), position, Blocker(i),
                       static_cast<float>(i * 10.1), static_cast<float>(i * 100.1), BodyPart(i),
                       static_cast<float>(i * 11.1), item_filename);
  }
  for (unsigned int i = 1; i < 3; i++)
  {
    items[i].setContent(&items[i - 1], i);
    if (i == 1)
      items[i].setContent(&external_content, 4);
  }
  items[2].setContent(&items[0], 1);

  Object::save_all<Item>(external_file1, vector<Item>(1, external_part));
  Object::save_all<Item>(external_file2, vector<Item>(1, external_content));
  Object::save_all<Equipment>(item_filename, items);

  NamedObject job(2, "Test filename", "Test Job", "Test Description for job");

  vector<Character> characters;
  for (unsigned int i = 0; i < 3; i++)
  {
    Position position(i + 1, i + 2, i + 3);
    Buff buff(i + 1, i + 2, i + 3, i + 4, static_cast<float>(i + 5.5),
              static_cast<float>(i + 6.6), i + 7, static_cast<float>(i + 8.8));
    characters.emplace_back(i, "Test Character " + to_string(i), "Test description for character " + to_string(i),
                            static_cast<float>(i * 1.1), static_cast<float>(1 + i * 0.1),
                            Sex(i), job, buff, position, Blocker(i), 10, filename);
  }
  for (unsigned int i = 0; i < 3; i++)
  {
    characters[i].addProperty(&items[i]);
    characters[i].setInventory(&items[i], 2);
    characters[i].setEquipment(&items[i]);
    characters[i].addSkill(skills[i]);
    characters[i].addBuff(buffs[i]);
  }

  EXPECT_EQ(*dynamic_cast<Character *>(getDefaults()[filename][0].get()), characters[0]);
  EXPECT_FALSE(dynamic_cast<Character *>(getDefaults()[filename][0].get())->equals(characters[0]));
  Character::save_all(filename, characters);
  EXPECT_EQ(*dynamic_cast<Character *>(getDefaults()[filename][0].get()), characters[0]);
  EXPECT_TRUE(dynamic_cast<Character *>(getDefaults()[filename][0].get())->equals(characters[0]));

  stringstream os, charac, skil, buf, itm, equip, prop;
  os << characters[0];
  charac << Buff(1, 2, 3, 4, 5.5, 6.6, 7, 8.8);
  skil << skills[0];
  buf << buffs[0];
  itm << (Item) items[0];
  equip << items[0];
  prop << static_cast<NamedObject>(items[0]);

  string test = "id: 0, filename: " + filename + ", name: Test Character 0, " +
                "description: Test description for character 0,\nmass: 0, size: 1, direction: {x: 0, y: 0, z: 0}, " +
                "position: {x: 1, y: 2, z: 3},\navailability: 1, blocking: 0, sex: 0,\n" +
                "job: {id: 2, filename: Test filename, name: Test Job, description: Test Description for job},\n" +
                "characteristics: {" + charac.str() + "},\n" +
                "inventory: {\n\t{" + itm.str() + "}: 1,},\n" +
                "equipment: {\n\t{" + equip.str() + "},},\n" +
                "properties: {\n\t{" + prop.str() + "},},\n" +
                "buffs: {\n\t{" + buf.str() + "},},\n" +
                "skills: {\n\t{" + skil.str() + "},},\nexperience: 10";
  EXPECT_EQ(test, os.str());

  vector<Character *> characters2 = Character::extract_all<Character>(_TEST_FOLDER_ + "characters.bin");
  EXPECT_EQ(characters.size(), characters2.size());
  for (unsigned int i = 0; i < characters2.size(); i++)
    EXPECT_TRUE(characters2[i]->equals(characters[i]));
  auto *character1 = dynamic_cast<Character *>(Character::getCopy(characters2[0]));
  EXPECT_TRUE(character1->equals(*characters2[0]));
  delete (character1);
  for (auto character: characters2)
    delete (character);
}