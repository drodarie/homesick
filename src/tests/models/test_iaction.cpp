//
// Created by drodarie on 5/27/19.
//

#include <iostream>
#include <random>
#include "gtest/gtest.h"
#include "../../main/models/actions/iaction.h"

using namespace std;

TEST (IActionTest, Basic_Fonctions)
{
  string filename = _TEST_FOLDER_ + "actions.bin";
  Buff buff(1, 2, 3, 4, 5.5, 6.6, 7, 8.8),
    bug_buff(IAction::_MAX_DIFFICULTY_+1,IAction::_MAX_DIFFICULTY_*2,IAction::_MAX_DIFFICULTY_-1,IAction::_MAX_DIFFICULTY_,5.5,6.6,7,8.8),
    corrected_buff(IAction::_MAX_DIFFICULTY_,IAction::_MAX_DIFFICULTY_,IAction::_MAX_DIFFICULTY_-1,IAction::_MAX_DIFFICULTY_,5.5,6.6,7,8.8),
    buff2(2, 3, 4, 5, 6.6, 7.7, 8, 9.9);
  Skill skill(0, "TestSkill", "Test description for Skill"),
  skill1(1, "TestSkill1", "Test description for Skill1");
  set<Skill> result;

  auto action = IAction("test action", "random action", buff, false, false,filename);
  action.updateDefaults();
  auto action2 = IAction("test action2", "random action2", buff2, false, false, filename);
  EXPECT_EQ(static_cast<unsigned int>(0), action.getId());
  EXPECT_TRUE(dynamic_cast<IAction *>(getDefaults()[filename][0].get())->equals(action));
  EXPECT_EQ(1, action2.getId());
  EXPECT_EQ(result, action.getRequiredSkills());
  EXPECT_FALSE(action.requireSkill(skill));
  EXPECT_EQ(result,action.getLinkedSkills());
  EXPECT_FALSE(action.checkAvailability());
  EXPECT_FALSE(action.canReact());


  action.addRequiredSkill(skill);
  result.insert(skill);
  EXPECT_EQ(result, action.getRequiredSkills());
  EXPECT_TRUE(action.requireSkill(skill));
  EXPECT_FALSE(action.requireSkill(skill1));
  EXPECT_EQ(result, action.getLinkedSkills());
  EXPECT_EQ(0, action.getBonus(skill));
  EXPECT_EQ(0, action.getBonus(skill1));
  action.addRequiredSkill(skill);
  EXPECT_EQ(result, action.getRequiredSkills());
  EXPECT_TRUE(action.requireSkill(skill));
  EXPECT_FALSE(action.requireSkill(skill1));
  EXPECT_EQ(result, action.getLinkedSkills());
  EXPECT_EQ(0, action.getBonus(skill));
  EXPECT_EQ(0, action.getBonus(skill1));

  action.setLinkedSkill(skill, 10);
  EXPECT_EQ(result, action.getLinkedSkills());
  EXPECT_EQ(10, action.getBonus(skill));
  action.setLinkedSkill(skill1,20);
  EXPECT_EQ(10, action.getBonus(skill));
  EXPECT_EQ(20, action.getBonus(skill1));

  action.removeLinkedSkill(skill1);
  EXPECT_EQ(result, action.getLinkedSkills());
  EXPECT_EQ(10, action.getBonus(skill));
  action.removeLinkedSkill(skill1);
  EXPECT_EQ(result, action.getLinkedSkills());
  EXPECT_EQ(10, action.getBonus(skill));

  action.removeRequiredSkill(skill1);
  EXPECT_EQ(result, action.getLinkedSkills());
  EXPECT_EQ(10, action.getBonus(skill));
  EXPECT_TRUE(action.requireSkill(skill));
  EXPECT_EQ(result, action.getRequiredSkills());

  action.removeLinkedSkill(skill);
  EXPECT_EQ(result, action.getLinkedSkills());
  EXPECT_EQ(0, action.getBonus(skill));
  EXPECT_TRUE(action.requireSkill(skill));
  EXPECT_EQ(result, action.getRequiredSkills());

  action.removeRequiredSkill(skill);
  EXPECT_EQ(set<Skill>(), action.getLinkedSkills());
  EXPECT_FALSE(action.requireSkill(skill));
  EXPECT_EQ(set<Skill>(), action.getRequiredSkills());

  EXPECT_EQ(buff, action.getDifficulty());
  action.setDifficulty(bug_buff);
  EXPECT_EQ(buff, action.getDifficulty());
  action.setDifficulty(buff2);
  EXPECT_EQ(buff2, action.getDifficulty());

  action.setCheckAvailability(true);
  EXPECT_TRUE(action.checkAvailability());
  action.setCanReact(true);
  EXPECT_TRUE(action.canReact());

  IAction action3 ("test action", "random action", bug_buff, false, false,filename);
  EXPECT_EQ(corrected_buff, action3.getDifficulty());
}

TEST(IActionTest, Test)
{
  Buff buff(40, 75, 40, 0, 5.5, 6.6, 7, 8.8),
  strength(0,-1,-1,-1,0,10,0,0), strength_intel(1,-1,0,-1,0,10,0,0), social(-1,-1,-1,0,0,10,0,0);
  ;
  NamedObject job(2, "Test filename", "Test Job", "Test Description for job");
  Position position(1, 2, 3);
  Skill skill(0, "TestSkill", "Test description for Skill"),
    skill1(1, "TestSkill1", "Test description for Skill1");
  Character character(1, "Test Character", "Test description for character", 1.1, 1.2, Sex::Male,
                      job, buff, position);

  vector<Entity*> targets;
  for (unsigned int i=0; i<2; i++)
    targets.emplace_back(new Entity(1,"target","target of the action", 1.0, 1.1, position));
  targets[0]->setAvailable(false);

  IAction action(0, "test action", "random action", strength, false, false);
  character.setAvailable(false);
  EXPECT_FALSE(action.canPerform(&character, vector<Entity*>()));
  character.setAvailable(true);
  action.setLinkedSkill(skill,10);
  action.addRequiredSkill(skill);
  EXPECT_EQ(10, action.getBonus(skill));
  action.setLinkedSkill(skill1, 10);
  EXPECT_FALSE(action.canPerform(&character, vector<Entity*>()));
  character.addSkill(skill);
  EXPECT_TRUE(action.canPerform(&character, vector<Entity*>()));
  EXPECT_TRUE(action.canPerform(&character, targets));
  action.setCheckAvailability(true);
  EXPECT_FALSE(action.canPerform(&character, targets));
  action.setCheckAvailability(false);
  action.addRequiredSkill(skill1);
  EXPECT_FALSE(action.canPerform(&character, vector<Entity*>()));
  action.removeRequiredSkill(skill1);

  float successes = 0, crit_succ=0, crit_fail=0, num_test = 10000;
  for (unsigned int i=0; i<num_test; i++)
  {
    auto result = action.testAction(&character, 0);
    EXPECT_TRUE(result.success>=-100 && result.success<=100);
    EXPECT_EQ(result.randomTests.size(), 1);
    if (result.success>=0)
      successes++;
    if(result.success>=100)
      crit_succ++;
    if(result.success<=-100)
      crit_fail++;
  }
  EXPECT_LT(pow(successes/num_test-0.5,2)/0.5 + pow((num_test-successes)/num_test-0.5,2)/0.5, 3.841); //p-value 0.05
  EXPECT_LT(pow(crit_succ/num_test-0.05,2)/0.05 + pow((num_test-successes)/num_test-0.95,2)/0.95, 3.841); //p-value 0.05
  EXPECT_LT(pow(crit_fail/num_test-0.05,2)/0.05 + pow((num_test-successes)/num_test-0.95,2)/0.95, 3.841); //p-value 0.05

  action.setDifficulty(strength_intel);
  for (unsigned int i=0; i<100; i++)
  {
    auto result = action.testAction(&character, 0);
    EXPECT_TRUE(result.success>=-200 && result.success<=200);
    EXPECT_EQ(result.randomTests.size(), 2);
  }
  successes = 0;
  action.setLinkedSkill(skill, 0);
  action.setDifficulty(social);
  for (unsigned int i=0; i<1000; i++)
  {
    auto result = action.testAction(&character, 0);
    if (result.success>=0)
      successes++;
  }
  EXPECT_GT(successes, 0);
  for (auto target: targets)
    delete target;
}

TEST (IActionTest, Store)
{
  string filename = _TEST_FOLDER_ + "actions.bin";
  vector<IAction> actions;
  for (unsigned int i = 0; i < 3; i++)
  {
    Buff buff(10*i, 20*i, 30*i, 40*i, 5.5*i, 6.6*i, 7*i, 8.8*i);
    Skill skill(i*2, "TestSkill "+ to_string(i*2), "Test description for Skill "+ to_string(i*2)),
      skill1(i*2+1, "TestSkill "+ to_string(i*2+1), "Test description for Skill "+ to_string(i*2+1));
    actions.emplace_back(i, "Test Action " + to_string(i), "Test description for action " + to_string(i),
      buff,false, true, filename);
    actions[i].addRequiredSkill(skill);
    actions[i].setLinkedSkill(skill, 10*i+5);
    actions[i].setLinkedSkill(skill1, 5*i+10);
  }
  stringstream difficulty, os, skill, skill2;
  os<<actions[0];
  difficulty << Buff(0,0,0,0,0,0,0,0);
  skill<<Skill(0, "TestSkill 0", "Test description for Skill 0");
  skill2<<Skill(1, "TestSkill 1", "Test description for Skill 1");
  string test = "id: 0, filename: " + filename + ", name: Test Action 0, " +
                "description: Test description for action 0,\n"+
                "required skills: {\n\t{"+skill.str()+"},}\n"+
                "linked skills: {\n\t{"+skill.str()+"}, bonus: 5,\n\t{"+skill2.str()+"}, bonus: 10,}\n"+
                "difficulty: {"+difficulty.str()+"},\ncheck availability: 0, can react: 1";
  EXPECT_EQ(test, os.str());
  IAction::save_all(filename, actions);
  vector<Object *> actions2 = IAction::load_all(filename);
  EXPECT_EQ(actions.size(), actions2.size());
  for (unsigned int i = 0; i < actions.size(); i++)
    EXPECT_TRUE(dynamic_cast<IAction *>(actions2[i])->equals(actions[i]));
  for (auto &i: actions2)
    delete (i);
}