/**
 * @file test_stuff.cpp
 * @author drodarie
 * @date 2/12/18.
*/

#include <iostream>
#include "gtest/gtest.h"
#include "../../main/models/entities/items/stuff.h"

using namespace std;

TEST(EquipmentTest, Basic_Functions)
{
  string filename = _TEST_FOLDER_ + "equipments.bin";
  vector<Equipment> items;
  for (unsigned int i = 0; i < 11; i++)
  {
    Position position(i, i, i);
    items.emplace_back("Test equipment " + to_string(i),
                       "Test description for equipment " + to_string(i),
                       i + i * 0.1, 1 + i * 0.1, position, Blocker::Movable,
                       i * 10.1, i * 100.1, BodyPart(i),
                       i * 11.1, filename
    );
    items[i].updateDefaults();
    EXPECT_EQ(i, items[i].getId());
    EXPECT_TRUE(dynamic_cast<Equipment *>(getDefaults()[filename][i].get())->equals(items[i]));
  }

  stringstream os;
  os << items[2];
  string test = "id: 2, filename: " + filename + ", name: Test equipment 2, " +
                "description: Test description for equipment 2,\nmass: 2.2, size: 1.2, direction: {x: 0, y: 0, z: 0}, " +
                "position: {x: 2, y: 2, z: 2},\navailability: 1, blocking: 2, value: 20.2, damage: 200.2,\n" +
                "content: {},\nbody: 2, protection: 22.2";
  EXPECT_EQ(test, os.str());

  EXPECT_EQ(items[1].getBodyPart(), BodyPart::Neck);
  EXPECT_EQ(items[2].getBodyPart(), BodyPart::Torso);
  EXPECT_EQ(items[3].getBodyPart(), BodyPart::Back);
  EXPECT_EQ(items[4].getBodyPart(), BodyPart::Hands);
  EXPECT_EQ(items[5].getBodyPart(), BodyPart::RightHand);
  EXPECT_EQ(items[6].getBodyPart(), BodyPart::LeftHand);
  EXPECT_EQ(items[7].getBodyPart(), BodyPart::Belt);
  EXPECT_EQ(items[8].getBodyPart(), BodyPart::Legs);
  EXPECT_EQ(items[9].getBodyPart(), BodyPart::Feet);
  EXPECT_EQ(items[10].getBodyPart(), BodyPart::None);

  items[10].setBodyPart(BodyPart::Head);
  EXPECT_EQ(items[10].getBodyPart(), BodyPart::Head);

  for (unsigned int i = 0; i < items.size(); i++)
  {
    EXPECT_NEAR(items[i].getProtection(), i * 11.1, 1e-5);
    items[i].setProtection(items[i].getProtection() - i);
    EXPECT_NEAR(items[i].getProtection(), i * 10.1, 1e-5);
  }
  items[0].setContent(&items[1], 1);
  EXPECT_FALSE(items[0] == items[1]);
  items[1] = {items[0]};
  EXPECT_TRUE(items[0].equals(items[1]));
}

TEST(EquipmentTest, Store)
{
  string filename = _TEST_FOLDER_ + "equipments.bin",
    external_file1 = _TEST_FOLDER_ + "items2.bin",
    external_file2 = _TEST_FOLDER_ + "items3.bin";
  vector<Equipment> items;
  Item external_part(0,
                     "External Part",
                     "Test description for external part",
                     static_cast<float>(4 + 4 * 0.1), static_cast<float>(1.1), {4, 4, 4}, Blocker::Crossable,
                     static_cast<float>(4 * 10.1), static_cast<float>(4 * 100.1),
                     external_file1),
    external_content(1, "External Content",
                     "Test description for external content",
                     static_cast<float>(5 + 5 * 0.1), static_cast<float>(1.4), {4, 4, 4}, Blocker::Movable,
                     static_cast<float>(5 * 10.1), static_cast<float>(5 * 100.1),
                     external_file2);
  for (unsigned int i = 0; i < 3; i++)
  {
    Position position(i, i, i);
    items.emplace_back(i,
                       "Test Equipment" + to_string(i),
                       "Test description for item" + to_string(i),
                       static_cast<float>(i + i * 0.1), static_cast<float>(1 + i * 0.1), position, Blocker(i),
                       static_cast<float>(i * 10.1), static_cast<float>(i * 100.1), BodyPart(i),
                       static_cast<float>(i * 11.1), filename);
  }
  for (unsigned int i = 1; i < 3; i++)
  {
    items[i].setContent(&items[i - 1], i);
    if (i == 1)
      items[i].setContent(&external_content, 4);
  }
  items[2].setContent(&items[0], 1);
  EXPECT_EQ(*dynamic_cast<Equipment *>(getDefaults()[filename][0].get()), items[0]);
  EXPECT_FALSE(dynamic_cast<Equipment *>(getDefaults()[filename][0].get())->equals(items[0]));
  Object::save_all<Item>(external_file1, vector<Item>(1, external_part));
  Object::save_all<Item>(external_file2, vector<Item>(1, external_content));
  Object::save_all<Equipment>(filename, items);
  EXPECT_EQ(*dynamic_cast<Equipment *>(getDefaults()[filename][0].get()), items[0]);
  EXPECT_TRUE(dynamic_cast<Equipment *>(getDefaults()[filename][0].get())->equals(items[0]));
  vector<Equipment *> items2 = Item::extract_all<Equipment>(filename);
  EXPECT_EQ(items.size(), items2.size());
  for (unsigned int i = 0; i < items.size() && i < items2.size(); i++)
    EXPECT_TRUE(items2[i]->equals(items[i]));
  for (auto &i:items2)
    delete (i);
}

TEST(WeaponTest, Basic_Functions)
{
  string filename = _TEST_FOLDER_ + "weapons.bin";
  Weapon items("Test weapon",
               "Test description for weapon",
               1.1, 1.2, {1, 1, 1}, Blocker::None,
               10.1, 100.1,
               BodyPart::LeftHand, 11.1, 11, filename);
  items.updateDefaults();
  Weapon items2("Test weapon 2",
                "Test description for weapon 2",
                1.1, 1.2, {1, 1, 1}, Blocker::Wall,
                10.1, 100.1,
                BodyPart::RightHand, 11.1, 11, filename);
  EXPECT_EQ(static_cast<unsigned int>(0), items.getId());
  EXPECT_EQ(1, items2.getId());
  EXPECT_TRUE(dynamic_cast<Weapon *>(getDefaults()[filename][0].get())->equals(items));
  EXPECT_EQ(items.getBodyPart(), BodyPart::LeftHand);
  EXPECT_EQ(items.getScope(), static_cast<unsigned int>(11));

  stringstream os;
  os << items2;
  string test = "id: 1, filename: " + filename + ", name: Test weapon 2, " +
                "description: Test description for weapon 2,\nmass: 1.1, size: 1.2, direction: {x: 0, y: 0, z: 0}, " +
                "position: {x: 1, y: 1, z: 1},\navailability: 1, blocking: 4, value: 10.1, damage: 100.1,\n" +
                "content: {},\nbody: 5, protection: 11.1, scope: 11";
  EXPECT_EQ(test, os.str());
  items.setScope(1);
  EXPECT_EQ(items.getScope(), static_cast<unsigned int>(1));
  items.setContent(&items2, 1);
  EXPECT_FALSE(items == items2);
  items2 = {items};
  EXPECT_TRUE(items.equals(items2));
}

TEST(WeaponTest, Store)
{
  string filename = _TEST_FOLDER_ + "weapons.bin",
    external_file1 = _TEST_FOLDER_ + "items2.bin",
    external_file2 = _TEST_FOLDER_ + "items3.bin";
  vector<Weapon> items;
  Item external_part(0,
                     "External Part",
                     "Test description for external part",
                     static_cast<float>(4 + 4 * 0.1), static_cast<float>(1.1), {4, 4, 4}, Blocker::Movable,
                     static_cast<float>(4 * 10.1), static_cast<float>(4 * 100.1),
                     external_file1);
  Equipment external_content(1, "External Content",
                             "Test description for external content",
                             static_cast<float>(5 + 5 * 0.1), static_cast<float>(1.4), {4, 4, 4}, Blocker::Crossable,
                             static_cast<float>(5 * 10.1), static_cast<float>(5 * 100.1),
                             BodyPart(5), static_cast<float>(5 * 11.1),
                             external_file2);
  for (unsigned int i = 0; i < 3; i++)
  {
    Position position(i, i, i);
    items.emplace_back(i,
                       "Test weapon" + to_string(i),
                       "Test description for weapon" + to_string(i),
                       i + i * 0.1, 1 + i * 0.1, position, Blocker(i),
                       i * 10.1, i * 100.1, BodyPart(i),
                       i * 11.1, i * 11,
                       filename);
  }
  for (unsigned int i = 1; i < 3; i++)
  {
    items[i].setContent(&items[i - 1], i);
    if (i == 1)
      items[i].setContent(&external_content, 4);
  }
  items[2].setContent(&items[0], 1);

  EXPECT_EQ(*dynamic_cast<Weapon *>(getDefaults()[filename][0].get()), items[0]);
  EXPECT_FALSE(dynamic_cast<Weapon *>(getDefaults()[filename][0].get())->equals(items[0]));
  Object::save_all<Item>(external_file1, vector<Item>(1, external_part));
  Object::save_all<Equipment>(external_file2, vector<Equipment>(1, external_content));
  Object::save_all<Weapon>(filename, items);
  EXPECT_EQ(*dynamic_cast<Weapon *>(getDefaults()[filename][0].get()), items[0]);
  EXPECT_TRUE(dynamic_cast<Weapon *>(getDefaults()[filename][0].get())->equals(items[0]));
  vector<Weapon *> items2 = Item::extract_all<Weapon>(filename);
  EXPECT_EQ(items.size(), items2.size());
  for (unsigned int i = 0; i < items.size() && i < items2.size(); i++)
    EXPECT_TRUE(items2[i]->equals(items[i]));
  for (auto &i:items2)
    delete (i);
}

TEST(PotionTest, Basic_Functions)
{
  string filename = _TEST_FOLDER_ + "potions.bin";
  Buff buff(1, 2, 3, 4, 5.5, 6.6, 7, 8.8),
    buff2(2, 3, 4, 5, 6.6, 7.7, 8, 9.9);
  Potion items("Test potion",
               "Test description for potion",
               1.1, 1.2, buff, {1, 1, 1}, Blocker::Breakable,
               10.1, 100.1, filename);
  items.updateDefaults();
  Potion items2("Test potion 2",
                "Test description for potion 2",
                1.1, 1.3, buff, {1, 1, 1}, Blocker::None,
                10.1, 100.1, filename);
  EXPECT_EQ(static_cast<unsigned int>(0), items.getId());
  EXPECT_EQ(1, items2.getId());
  EXPECT_TRUE(dynamic_cast<Potion *>(getDefaults()[filename][0].get())->equals(items));
  stringstream os, sub;
  os << items2;
  sub << buff;
  string test = "id: 1, filename: " + filename + ", name: Test potion 2, " +
                "description: Test description for potion 2,\nmass: 1.1, size: 1.3, direction: {x: 0, y: 0, z: 0}, " +
                "position: {x: 1, y: 1, z: 1},\navailability: 1, blocking: 0, value: 10.1, damage: 100.1,\n" +
                "content: {},\neffect: {" + sub.str() + "}";
  EXPECT_EQ(test, os.str());
  EXPECT_EQ(items.getEffect(), buff);

  items.setEffect(buff2);
  EXPECT_EQ(items.getEffect(), buff2);
  items.setContent(&items2, 1);
  EXPECT_FALSE(items == items2);
  items2 = {items};
  EXPECT_TRUE(items.equals(items2));
}

TEST(PotionTest, Store)
{
  string filename = _TEST_FOLDER_ + "potions.bin",
    external_file1 = _TEST_FOLDER_ + "items2.bin",
    external_file2 = _TEST_FOLDER_ + "items3.bin";
  vector<Potion> items;
  Item external_part(0,
                     "External Part",
                     "Test description for external part",
                     static_cast<float>(4 + 4 * 0.1), static_cast<float>(1.1), {4, 4, 4}, Blocker::Wall,
                     static_cast<float>(4 * 10.1), static_cast<float>(4 * 100.1),
                     external_file1);
  Equipment external_content(1, "External Content",
                             "Test description for external content",
                             static_cast<float>(5 + 5 * 0.1), static_cast<float>(1.4), {4, 4, 4}, Blocker::Movable,
                             static_cast<float>(5 * 10.1), static_cast<float>(5 * 100.1),
                             BodyPart(5), static_cast<float>(5 * 11.1),
                             external_file2);
  for (unsigned int i = 0; i < 3; i++)
  {
    Position position(i, i, i);
    Buff buff(i, i * 2, i * 3, i * 4,
              static_cast<float>(i * 5.5),
              static_cast<float>(i * 6.6), i * 7,
              static_cast<float>(i * 8.8));
    items.emplace_back(i,
                       "Test potion" + to_string(i),
                       "Test description for potion" + to_string(i),
                       i + i * 0.1, 1 + i * 0.1, buff, position, Blocker(i),
                       i * 10.1, i * 100.1,
                       filename);
  }
  for (unsigned int i = 1; i < 3; i++)
  {
    items[i].setContent(&items[i - 1], i);
    if (i == 1)
      items[i].setContent(&external_content, 4);
  }
  items[2].setContent(&items[0], 1);

  EXPECT_EQ(*dynamic_cast<Potion *>(getDefaults()[filename][0].get()), items[0]);
  EXPECT_FALSE(dynamic_cast<Potion *>(getDefaults()[filename][0].get())->equals(items[0]));
  Object::save_all<Item>(external_file1, vector<Item>(1, external_part));
  Object::save_all<Equipment>(external_file2, vector<Equipment>(1, external_content));
  Object::save_all<Potion>(filename, items);
  EXPECT_EQ(*dynamic_cast<Potion *>(getDefaults()[filename][0].get()), items[0]);
  EXPECT_TRUE(dynamic_cast<Potion *>(getDefaults()[filename][0].get())->equals(items[0]));
  vector<Potion *> items2 = Item::extract_all<Potion>(filename);
  EXPECT_EQ(items.size(), items2.size());
  for (unsigned int i = 0; i < items.size() && i < items2.size(); i++)
    EXPECT_TRUE(items2[i]->equals(items[i]));
  for (auto &i:items2)
    delete (i);
}