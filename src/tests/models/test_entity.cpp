//
// Created by drodarie on 12/22/17.
//

#include <iostream>
#include "gtest/gtest.h"
#include "../../main/models/entities/entity.h"

using namespace std;

TEST (EntityTest, Basic_Fonctions)
{
  Position position(2, 3, 4), position2(6, 7, 8);
  string filename = _TEST_FOLDER_ + "entities.bin";
  Entity entity("Test entity", "Test description for entity", 1.1, 1.2, position, Blocker::None, filename);
  entity.updateDefaults();
  Entity entity2("Test entity", "Test description for entity", 1.1, 1.2, position, Blocker::None, filename);
  EXPECT_EQ(static_cast<unsigned int>(0), entity.getId());
  EXPECT_TRUE(dynamic_cast<Entity *>(getDefaults()[filename][0].get())->equals(entity));
  EXPECT_EQ(1, entity2.getId());
  EXPECT_EQ("Test entity", entity.getName());
  EXPECT_EQ("Test description for entity", entity.getDescription());
  EXPECT_NEAR(1.1, entity.getMass(), 1e-7);
  EXPECT_NEAR(1.2, entity.getSize(), 1e-7);
  EXPECT_TRUE(position == entity.getPosition());
  EXPECT_TRUE(Orientation::Steady == entity.getDirection());
  EXPECT_TRUE(entity.isAvailable());
  EXPECT_EQ(entity.getBlocker(), Blocker::None);
  EXPECT_FALSE(entity.isBlockingPosition());
  stringstream os;
  os << entity;
  string test = "id: 0, filename: " + filename + ", name: Test entity, " +
                "description: Test description for entity,\nmass: 1.1, size: 1.2, direction: {x: 0, y: 0, z: 0}, " +
                "position: {x: 2, y: 3, z: 4},\navailability: 1, blocking: 0";
  EXPECT_EQ(test, os.str());

  entity.setName("test entity2");
  entity.setDescription("Test description for entity2");
  entity.setMass(5.5);
  entity.setSize(1.5);
  entity.setPosition(position2);
  entity.setDirection(Orientation::Up);
  entity.setAvailable(false);
  entity.setBlocking(Blocker::Breakable);
  EXPECT_EQ("test entity2", entity.getName());
  EXPECT_EQ("Test description for entity2", entity.getDescription());
  EXPECT_NEAR(5.5, entity.getMass(), 1e-7);
  EXPECT_NEAR(1.5, entity.getSize(), 1e-7);
  EXPECT_TRUE(position2 == entity.getPosition());
  EXPECT_TRUE(Orientation::Up == entity.getDirection());
  EXPECT_FALSE(entity.isAvailable());
  EXPECT_TRUE(entity.isBlockingPosition());
  EXPECT_EQ(entity.getBlocker(), Blocker::Breakable);
  EXPECT_TRUE(entity != entity2);
  entity2 = {entity};
  EXPECT_TRUE(entity.equals(entity2));
}

TEST(EntityTest, Store)
{
  string filename = _TEST_FOLDER_ + "entities.bin";
  vector<Entity> entities;
  for (unsigned int i = 0; i < 5; i++)
  {
    Position position(i, i, i);
    entities.emplace_back(i,
                          "Test entity" + to_string(i),
                          "Test description for entity" + to_string(i),
                          i + i * 0.1, 1 + i * 0.1, position, Blocker(i), filename);
  }
  EXPECT_EQ(*dynamic_cast<Entity *>(getDefaults()[filename][0].get()), entities[0]);
  EXPECT_FALSE(dynamic_cast<Entity *>(getDefaults()[filename][0].get())->equals(entities[0]));
  Entity::save_all(filename, entities);
  EXPECT_EQ(*dynamic_cast<Entity *>(getDefaults()[filename][0].get()), entities[0]);
  EXPECT_TRUE(dynamic_cast<Entity *>(getDefaults()[filename][0].get())->equals(entities[0]));
  vector<Object *> entities2 = Entity::load_all(filename);
  EXPECT_EQ(entities.size(), entities2.size());
  for (unsigned int i = 0; i < entities.size(); i++)
    EXPECT_TRUE(dynamic_cast<Entity *>(entities2[i])->equals(entities[i]));
  for (auto &i: entities2)
    delete (i);
}