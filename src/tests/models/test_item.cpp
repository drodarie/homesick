/**
 * @file test_item.cpp
 * @author drodarie
 * @date 2/8/18.
*/

#include <iostream>
#include "gtest/gtest.h"
#include "../../main/models/entities/items/item.h"

using namespace std;

TEST (ItemTest, Basic_Fonctions)
{
  string filename = _TEST_FOLDER_ + "items.bin";
  Position position(2, 3, 4);
  Item item("test item", "Test description for item", 1.1, 1.2, position, Blocker::Crossable, 10.1, 20.1, filename);
  item.updateDefaults();
  Item item2("test item", "Test description for item", 1.1, 1.2, position, Blocker::Crossable, 10.1, 20.1, filename);
  EXPECT_EQ(static_cast<unsigned int>(0), item.getId());
  EXPECT_TRUE(dynamic_cast<Item *>(getDefaults()[filename][0].get())->equals(item));
  EXPECT_EQ(1, item2.getId());
  EXPECT_NEAR(10.1, item.getValue(), 1e-5);
  EXPECT_NEAR(20.1, item.getDamage(), 1e-5);

  item.setValue(5.5);
  EXPECT_FALSE(item.isQuestItem());
  EXPECT_FALSE(item2.isQuestItem());
  item.setDamage(static_cast<float>(-6.6));
  EXPECT_NEAR(5.5, item.getValue(), 1e-5);
  EXPECT_NEAR(static_cast<float>(-6.6), item.getDamage(), 1e-5);
  EXPECT_TRUE(item.isQuestItem());
  EXPECT_FALSE(item2.isQuestItem());
  EXPECT_TRUE(item != item2);
  item2 = {item};
  EXPECT_TRUE(item.equals(item2));
  EXPECT_TRUE(item2.isQuestItem());
}

TEST(ItemTest, Content)
{
  vector<Item *> items;
  vector<map<Item *, unsigned int>> maps(3);
  for (unsigned int i = 0; i < 3; i++)
  {
    Position position(i, i, i);
    items.emplace_back(new Item(
      i, "Test item" + to_string(i),
      "Test description for item" + to_string(i),
      static_cast<float>(i + i * 0.1), static_cast<float>(1 + i * 0.1), position, Blocker(i),
      static_cast<float>(i * 10.1), static_cast<float>(i * 100.1))
    );
    if (i > 0)
    {
      EXPECT_EQ(i, items[i]->setContent(items[i - 1], i));
      maps[i][items[i - 1]] = i;
    }
  }
  items[2]->setContent(items[0], 1);
  maps[2][items[0]] = 1;

  for (unsigned int i = 0; i < 3; i++)
  {
    EXPECT_EQ(maps[i].size(), items[i]->getContent().size());
    for (auto &j: items[i]->getContent())
    {
      bool found = false;
      for (auto &k: maps[i])
      {
        if (*k.first == *j.first)
        {
          EXPECT_EQ(j.second, k.second);
          found = true;
          break;
        }
      }
      EXPECT_TRUE(found);
    }
  }

  EXPECT_EQ(vector<Item *>(), items[0]->getContentList());
  EXPECT_NEAR(0, items[0]->getTotalMass(), 1e-6);
  EXPECT_NEAR(0, items[0]->getTotalValue(), 1e-6);
  EXPECT_FALSE(items[0]->hasContent(items[0]));
  EXPECT_EQ(static_cast<unsigned int>(0), items[0]->getNumberContent(items[1]));

  EXPECT_EQ(*items[0], *items[1]->getContentList()[0]);
  EXPECT_NEAR(1.1, items[1]->getTotalMass(), 1e-6);
  EXPECT_NEAR(10.1, items[1]->getTotalValue(), 1e-6);
  EXPECT_TRUE(items[1]->hasContent(items[0]));
  EXPECT_EQ(static_cast<unsigned int>(1), items[1]->getNumberContent(items[0]));
  EXPECT_EQ(static_cast<unsigned int>(0), items[1]->getNumberContent(items[2]));

  EXPECT_EQ(*items[0], *items[2]->getContentList()[0]);
  EXPECT_EQ(*items[1], *items[2]->getContentList()[1]);
  EXPECT_NEAR(1.1 * 2 + 2.2, items[2]->getTotalMass(), 1e-6);
  EXPECT_NEAR(10.1 * 2 + 20.2, items[2]->getTotalValue(), 1e-5);
  EXPECT_TRUE(items[2]->hasContent(items[0]));
  EXPECT_TRUE(items[2]->hasContent(items[1]));
  EXPECT_EQ(static_cast<unsigned int>(2), items[2]->getNumberContent(items[1]));
  EXPECT_EQ(static_cast<unsigned int>(1), items[2]->getNumberContent(items[0]));

  EXPECT_EQ(static_cast<unsigned int>(1), items[2]->setContent(items[1], -1));
  EXPECT_TRUE(items[2]->hasContent(items[1]));
  EXPECT_EQ(static_cast<unsigned int>(1), items[2]->getNumberContent(items[1]));
  EXPECT_EQ(static_cast<unsigned int>(1), items[2]->setContent(items[0], -1));
  EXPECT_FALSE(items[2]->hasContent(items[0]));
  EXPECT_EQ(static_cast<unsigned int>(0), items[2]->setContent(items[0], -1));
  EXPECT_EQ(static_cast<unsigned int>(0), items[2]->setContent(items[0], 0));
  EXPECT_EQ(static_cast<unsigned int>(0), items[2]->getNumberContent(items[0]));
  EXPECT_EQ(static_cast<unsigned int>(1), items[1]->getNumberContent(items[0]));
  EXPECT_EQ(static_cast<unsigned int>(0), items[2]->setContent(items[0], -3));
  for (auto &i: items)
    delete (i);
}

TEST(ItemTest, Store)
{
  string filename = _TEST_FOLDER_ + "items.bin",
    external_file1 = _TEST_FOLDER_ + "items2.bin",
    external_file2 = _TEST_FOLDER_ + "items3.bin";
  vector<Item> items;
  Item external_part(0,
                     "External Part",
                     "Test description for external part",
                     static_cast<float>(4 + 4 * 0.1), static_cast<float>(1.1), {4, 4, 4}, Blocker::None,
                     static_cast<float>(4 * 10.1), static_cast<float>(4 * 100.1),
                     external_file1),
    external_content(1, "External Content",
                     "Test description for external content",
                     static_cast<float>(5 + 5 * 0.1), static_cast<float>(1 + 1.4), {4, 4, 4}, Blocker::Breakable,
                     static_cast<float>(5 * 10.1), static_cast<float>(5 * 100.1),
                     external_file2);
  for (unsigned int i = 0; i < 3; i++)
  {
    Position position(i, i, i);
    items.emplace_back(i,
                       "Test item " + to_string(i),
                       "Test description for item " + to_string(i),
                       i + i * 0.1, 1 + i * 0.1, position, Blocker(i),
                       i * 10.1, i * 100.1, filename
    );
  }
  for (unsigned int i = 1; i < 3; i++)
  {
    items[i - 1].setContent(&external_content, 4);
    items[i].setContent(&items[i - 1], i);
  }
  items[2].setContent(&items[0], 1);

  stringstream os, part, cont, sub2;
  os << items[0];
  part << external_part;
  cont << external_content;
  string test = "id: 0, filename: " + filename + ", name: Test item 0, " +
                "description: Test description for item 0,\nmass: 0, size: 1, direction: {x: 0, y: 0, z: 0}, " +
                "position: {x: 0, y: 0, z: 0},\navailability: 1, blocking: 0, value: 0, damage: 0,\n" +
                "content: {\n\t{" + cont.str() + "}: 4,}";

  EXPECT_EQ(test, os.str());
  EXPECT_EQ(*dynamic_cast<Item *>(getDefaults()[filename][0].get()), items[0]);
  EXPECT_FALSE(dynamic_cast<Item *>(getDefaults()[filename][0].get())->equals(items[0]));
  Object::save_one(external_file1, external_part);
  Object::save_one(external_file2, external_content);
  Object::save_all<Item>(filename, items);
  EXPECT_EQ(*dynamic_cast<Item *>(getDefaults()[filename][0].get()), items[0]);
  EXPECT_TRUE(dynamic_cast<Item *>(getDefaults()[filename][0].get())->equals(items[0]));
  vector<Item *> items2 = NamedObject::extract_all<Item>(filename);
  EXPECT_EQ(items.size(), items2.size());
  for (unsigned int i = 0; i < items.size() && i < items2.size(); i++)
    EXPECT_TRUE(items2[i]->equals(items[i]));
  for (auto &i:items2)
    delete (i);
}