//
// Created by drodarie on 11/20/17.
//

#include <iostream>
#include "gtest/gtest.h"
#include "../../main/models/buff.h"

using namespace std;

TEST (BuffTest, Basic_Fonctions)
{
  Buff buff(1, 2, 3, 4, 5.5, 6.6, 7, 8.8), buff1;
  EXPECT_EQ(1, buff.getStrength());
  EXPECT_EQ(2, buff.getAgility());
  EXPECT_EQ(3, buff.getIntelligence());
  EXPECT_EQ(4, buff.getSociability());
  EXPECT_NEAR(5.5, buff.getMood(), 1e-7);
  EXPECT_NEAR(6.6, buff.getDuration(), 1e-7);
  EXPECT_EQ(7, buff.getPV());
  EXPECT_NEAR(8.8, buff.getSpeed(), 1e-6);

  buff1.setStrength(1);
  buff1.setAgility(2);
  buff1.setIntelligence(3);
  buff1.setSociability(4);
  buff1.setMood(5.5);
  buff1.setDuration(6.6);
  buff1.setPV(7);
  buff1.setSpeed(8.8);

  stringstream os;
  os << buff1;
  string outString = "strength: 1, agility: 2, intelligence: 3, sociability: 4,\nmood: 5.5, duration: 6.6, pv: 7, speed: 8.8";
  EXPECT_EQ(outString, os.str());

  EXPECT_TRUE(Buff() != buff1);
  EXPECT_TRUE(buff == buff1);
  buff = buff - buff1;
  EXPECT_FALSE(Buff() != buff);
  buff = buff + buff1;
  EXPECT_TRUE(buff == buff1);

  EXPECT_FALSE(buff.update(1));
  EXPECT_NEAR(5.6, buff.getDuration(), 1e-7);
  for (unsigned int i = 0; i < 5; i++)
    EXPECT_FALSE(buff.update(1));
  EXPECT_TRUE(buff.update(1));

  vector<int> test = {1, 2, 3, 4};
  EXPECT_EQ(test, buff.getTestCharacteristics());

  EXPECT_FALSE(buff.areCharacteristicsTooHigh());
  EXPECT_TRUE(buff.areCharacteristicsTooHigh(3));

  buff.cutCharacteristicsTooHigh();
  EXPECT_EQ(buff, buff1);
  buff.cutCharacteristicsTooHigh(3);
  test[3] = 3;
  EXPECT_EQ(test, buff.getTestCharacteristics());

  Buff *buff2 = dynamic_cast<Buff *>(Buff::getCopy(&buff));
  EXPECT_TRUE(*buff2 == buff);
  delete (buff2);
//  EXPECT_EQ("Strength: 1, Agility: 2, Intelligence: 3, Sociability: 4\nMood: 5.5, Duration: 6.6, PV: 7, Speed: 8.8", buff1.operator<<().)
}

TEST(BuffTest, Comparison)
{
  Buff buff1(1,0, 1, 1, 1.0, 1.0, 1, 1.0), buff2;
  EXPECT_FALSE(buff1>buff2);
  EXPECT_FALSE(buff2>buff1);
  buff1.setAgility(1);
  EXPECT_TRUE(buff1>buff2);
  EXPECT_FALSE(buff2>buff1);
  buff2.setIntelligence(1);
  EXPECT_FALSE(buff1>buff2);
  EXPECT_FALSE(buff2>buff1);
}

TEST(BuffTest, Store)
{
  string filename = _TEST_FOLDER_ + "buffs.bin";
  vector<Buff> buffs;
  for (unsigned int i = 0; i < 3; i++)
    buffs.emplace_back(i, i, i, i,
                       static_cast<float>(i + i * 0.1),
                       static_cast<float>(i + i * 0.1), i,
                       static_cast<float>(i + i * 0.1));
  Buff::save_all(filename, buffs);
  vector<Object *> buffs2 = Buff::load_all(filename);
  EXPECT_EQ(buffs.size(), buffs2.size());
  for (unsigned int i = 0; i < buffs.size(); i++)
    EXPECT_EQ(buffs[i], *dynamic_cast<Buff *>(buffs2[i]));
  for (auto &i:buffs2)
    delete (i);
}
