/**
 * @file test_objective.cpp
 * @author drodarie
 * @date 11/27/19.
*/

#include <iostream>
#include "gtest/gtest.h"
#include "../../main/models/missions/objectives.h"
#include "../../main/models/missions/statusObjectives.h"
#include "../../main/models/missions/locationObjective.h"

TEST (ObjectiveTest, Basic_Fonctions)
{
  string filename = _TEST_FOLDER_ + "objectives.bin";
  Objective objective = Objective("Test Objective", "Description for test objective", 1.1, Status::New, 2.2, 3, filename);
  objective.updateDefaults();
  Objective objective2 = Objective("Test Objective", "Description for test objective", 1.1, Status::New, 2.2, 3, filename);
  EXPECT_EQ(static_cast<unsigned int>(0), objective.getId());
  EXPECT_TRUE(dynamic_cast<Objective *>(getDefaults()[filename][0].get())->equals(objective));
  EXPECT_EQ(1, objective2.getId());
  EXPECT_NEAR(1.1, objective.getStartingTime(), 1e7);
  EXPECT_EQ(Status::New, objective.getStatus());
  EXPECT_NEAR(2.2, objective.getDuration(), 1e7);
  EXPECT_EQ(3, objective.getExperience());
  EXPECT_EQ(vector<Objective*>(), objective.getRequiredObjectives());
  objective.setStatus(Status::Finished);
  EXPECT_EQ(Status::Finished, objective.getStatus());
  objective.setDuration(-3.3);
  EXPECT_NEAR(-3.3, objective.getDuration(), 1e7);
  objective.setExperience(4);
  EXPECT_EQ(4, objective.getExperience());
}

TEST(ObjectiveTest, Requirements)
{
  Objective objective = Objective(0, "Test Objective0", "Description for test objective0",1.1),
    required1 = Objective(1, "Test Objective1", "Description for test objective 1",1.1),
    required2 = Objective(2, "Test Objective2", "Description for test objective 2",1.1);
  objective.setExperience(1);
  required1.setExperience(2);
  required2.setExperience(3);
  EXPECT_EQ(objective.getTotalExperience(), 1);
  EXPECT_EQ(vector<Objective *>(), objective.getRequiredObjectives());
  EXPECT_FALSE(objective.hasObjective(&required1));
  EXPECT_FALSE(objective.hasObjective(&required2));
  objective.addObjective(&required1);
  EXPECT_TRUE(objective.hasObjective(&required1));
  EXPECT_FALSE(objective.hasObjective(&required2));
  EXPECT_EQ(1, objective.getRequiredObjectives().size());
  EXPECT_EQ(required1, *dynamic_cast<Objective *>(objective.getRequiredObjectives()[0]));
  EXPECT_EQ(objective.getTotalExperience(), 3);
  objective.addObjective(&required1);
  EXPECT_EQ(1, objective.getRequiredObjectives().size());
  EXPECT_EQ(required1, *dynamic_cast<Objective *>(objective.getRequiredObjectives()[0]));
  EXPECT_EQ(objective.getTotalExperience(), 3);
  objective.addObjective(&required2);
  EXPECT_TRUE(objective.hasObjective(&required1));
  EXPECT_TRUE(objective.hasObjective(&required2));
  EXPECT_EQ(2, objective.getRequiredObjectives().size());
  EXPECT_EQ(required1, *dynamic_cast<Objective *>(objective.getRequiredObjectives()[0]));
  EXPECT_EQ(required2, *dynamic_cast<Objective *>(objective.getRequiredObjectives()[1]));
  EXPECT_EQ(objective.getTotalExperience(), 6);
  EXPECT_TRUE(objective.removeObjective(&required1));
  EXPECT_EQ(1, objective.getRequiredObjectives().size());
  EXPECT_EQ(required2, *dynamic_cast<Objective *>(objective.getRequiredObjectives()[0]));
  EXPECT_EQ(objective.getTotalExperience(), 4);
  EXPECT_FALSE(objective.removeObjective(&required1));
  EXPECT_EQ(1, objective.getRequiredObjectives().size());
  EXPECT_EQ(required2, *dynamic_cast<Objective *>(objective.getRequiredObjectives()[0]));
  EXPECT_TRUE(objective.removeObjective(&required2));
  EXPECT_EQ(vector<Objective *>(), objective.getRequiredObjectives());
  required1.addObjective(&required2);
  objective.addObjective(&required1);
  objective.addObjective(&required2);
  EXPECT_EQ(1, objective.getRequiredObjectives().size());
  EXPECT_EQ(required1, *dynamic_cast<Objective *>(objective.getRequiredObjectives()[0]));
  EXPECT_TRUE(objective.removeObjective(&required2));
}

TEST(ObjectiveTest, Update)
{
  Objective objective = Objective(0, "Test Objective0", "Description for test objective0",1.1, Status::Hidden, 2.0),
    required1 = Objective(1, "Test Objective1", "Description for test objective 1",1.1, Status::Hidden);
  objective.addObjective(&required1);
  NamedObject job(2, "Test filename", "Test Job", "Test Description for job");
  Position position(2, 1, 1), target(2,5,1), updatePos = Position(2, 4, 1);
  Buff empty(-1,-1,-1,-1,0,10,0,0.5), buff(40, 75, 40, 0, 5.5, 6.6, 7, 8.8);
  vector<Map*> maps = Map::extract_all<Map>(_TEST_FOLDER_ + "mdstar.bin");
  Character character(1, "Test Character", "Test description for character", 1.1, 1.2, Sex::Male,
                      job, buff, position);
  EXPECT_EQ(objective.getStatus(), Status::Hidden);
  EXPECT_EQ(required1.getStatus(), Status::Hidden);
  EXPECT_TRUE(required1.meetRequirements(&character, maps[0], 5.1));
  EXPECT_TRUE(objective.meetRequirements(&character, maps[0], 3.05));
  EXPECT_FALSE(objective.meetRequirements(&character, maps[0], 3.2));
  EXPECT_EQ(objective.update(&character, maps[0], 1.2), Status::Hidden);
  EXPECT_EQ(objective.getStatus(), Status::Hidden);
  EXPECT_EQ(objective.getRequiredObjectives()[0]->getStatus(), Status::New);
  EXPECT_NEAR(objective.getRequiredObjectives()[0]->getStartingTime(), 1.2, 1e-7);
  EXPECT_NEAR(objective.getStartingTime(), 1.1, 1e-7);
  objective.getRequiredObjectives()[0]->setStatus(Status::Refused);
  EXPECT_EQ(objective.update(&character, maps[0], 1.2), Status::Refused);
  EXPECT_EQ(objective.getStatus(), Status::Refused);
  EXPECT_EQ(objective.getRequiredObjectives()[0]->getStatus(), Status::Refused);
  EXPECT_EQ(objective.update(&character, maps[0], 3.4), Status::Refused);
  EXPECT_EQ(objective.getRequiredObjectives()[0]->getStatus(), Status::Refused);
  objective.getRequiredObjectives()[0]->setStatus(Status::New);
  objective.setStatus(Status::Hidden);
  EXPECT_EQ(objective.update(&character, maps[0], 1.3), Status::New);
  EXPECT_EQ(objective.getRequiredObjectives()[0]->getStatus(), Status::Finished);
  EXPECT_NEAR(objective.getRequiredObjectives()[0]->getStartingTime(), 1.2, 1e-7);
  EXPECT_NEAR(objective.getStartingTime(), 1.3, 1e-7);
  EXPECT_EQ(objective.update(&character, maps[0], 3.4), Status::Failed);
  EXPECT_EQ(objective.getRequiredObjectives()[0]->getStatus(), Status::Finished);
  EXPECT_EQ(objective.update(&character, maps[0], 3.0), Status::Failed);
  objective.setStatus(Status::Pending);
  EXPECT_EQ(objective.update(&character, maps[0], 3.0), Status::Finished);
  for (auto map: maps)
    delete(map);
}

TEST(ObjectiveTest, Store)
{
  string filename = _TEST_FOLDER_ + "objectives.bin";
  vector<Objective> objectives;
  for (unsigned int i = 0; i < 6; i++)
  {
    objectives.emplace_back(i, "Test Objective"+ to_string(i), "Description for test objective"+ to_string(i),
      1.1*i, Status(i), 10.1*i, 11*i, filename);
    if (i>0)
      objectives[i].addObjective(&objectives[i-1]);
  }
  EXPECT_EQ(*dynamic_cast<Objective *>(getDefaults()[filename][0].get()), objectives[0]);
  EXPECT_FALSE(dynamic_cast<Objective *>(getDefaults()[filename][0].get())->equals(objectives[0]));
  Objective::save_all(filename, objectives);
  EXPECT_EQ(*dynamic_cast<Objective *>(getDefaults()[filename][0].get()), objectives[0]);
  EXPECT_TRUE(dynamic_cast<Objective *>(getDefaults()[filename][0].get())->equals(objectives[0]));

  stringstream os, prop;
  os << objectives[2];
  prop << objectives[1];

  string test = "id: 2, filename: " + filename + ", name: Test Objective2, " +
                "description: Description for test objective2,\n" +
                "status: 2, duration: 20.2, experience: 22, start time: 2.2,\n" +
                "required objectives: {\n\t{" + prop.str() + "},}";
  EXPECT_EQ(test, os.str());

  vector<Objective *> objectives2 = Objective::extract_all<Objective>(filename);
  EXPECT_EQ(objectives.size(), objectives2.size());
  for (unsigned int i = 0; i < objectives2.size(); i++)
    EXPECT_TRUE(objectives2[i]->equals(objectives[i]));
  for (auto i: objectives2)
    delete(i);
}

TEST(StatusObjectiveTest, Requirements)
{
  auto characters = Object::extract_all<Character>(_TEST_FOLDER_ + "characters.bin");
  auto maps = Map::extract_all<Map>(_TEST_FOLDER_ + "mdstar.bin");
  auto objective = StatusObjective(0, "Test Objective", "Description for test objective", *characters[0], 1.1, Status::Pending),
    require = StatusObjective(1, "Requirement1", "Description requirement", *characters[2], 1.1, Status::Pending);
  EXPECT_TRUE(require.getFinalStatus().equals(*characters[2]));
  require.setFinalStatus(*characters[1]);
  EXPECT_TRUE(require.getFinalStatus().equals(*characters[1]));
  objective.addObjective(&require);
  EXPECT_TRUE(objective.meetRequirements(characters[0], maps[0], 2.2));
  EXPECT_FALSE(require.meetRequirements(characters[0], maps[0], 2.2));
  EXPECT_EQ(objective.update(characters[0], maps[0], 3.0), Status::Pending);
  characters[0]->addBuff(characters[1]->getTotalBuff());
  EXPECT_FALSE(require.meetRequirements(characters[0], maps[0], 2.2));
  characters[0]->addExperience(characters[1]->getExperience());
  EXPECT_FALSE(require.meetRequirements(characters[0], maps[0], 2.2));
  characters[0]->setInventory(characters[1]->getEquipmentList()[0], 1);
  characters[0]->setEquipment(characters[1]->getEquipmentList()[0]);
  EXPECT_FALSE(require.meetRequirements(characters[0], maps[0], 2.2));
  for (auto &item: characters[1]->getInventory())
    characters[0]->setInventory(item.first, item.second);
  EXPECT_FALSE(require.meetRequirements(characters[0], maps[0], 2.2));
  characters[0]->addProperty(characters[1]->getProperties()[0]);
  EXPECT_FALSE(require.meetRequirements(characters[0], maps[0], 2.2));
  characters[0]->addSkill(characters[1]->getSkills().begin().operator*());
  EXPECT_TRUE(require.meetRequirements(characters[0], maps[0], 2.2));
  EXPECT_EQ(objective.update(characters[0], maps[0], 3.0), Status::Finished);
  for (auto i: characters)
    delete(i);
  for (auto i: maps)
    delete(i);
}

TEST(StatusObjectiveTest, Store)
{
  string filename = _TEST_FOLDER_ + "statusObjectives.bin",
    character_filename = _TEST_FOLDER_ + "characters.bin";
  vector<StatusObjective> objectives;
  auto characters = Object::extract_all<Character>(character_filename);
  EXPECT_EQ(characters.size(), 3);
  for (unsigned int i = 0; i < 3; i++)
  {
    objectives.emplace_back(i, "Test Objective"+ to_string(i), "Description for test objective"+ to_string(i),
                            *characters[i], 1.1*i, Status(i), 10.1*i, 11*i, filename);
    if (i>0)
      objectives[i].addObjective(&objectives[i-1]);
  }
  StatusObjective::save_all(filename, objectives);

  stringstream os, charac;
  os << objectives[0];
  charac << *characters[0];

  string test = "id: 0, filename: " + filename + ", name: Test Objective0, " +
                "description: Description for test objective0,\n" +
                "status: 0, duration: 0, experience: 0, start time: 0,\n" +
                "required objectives: {},\n" +
                "Final actor status: {"+charac.str()+"}";
  EXPECT_EQ(test, os.str());

  vector<StatusObjective *> objectives2 = StatusObjective::extract_all<StatusObjective>(filename);
  EXPECT_EQ(objectives.size(), objectives2.size());
  for (unsigned int i = 0; i < objectives2.size(); i++)
    EXPECT_TRUE(objectives2[i]->equals(objectives[i]));
  for (auto objective: objectives2)
    delete(objective);
  for (auto character: characters)
    delete(character);
}

TEST(LocationObjectiveTest, Requirements)
{
  Position start = Position(2, 1, 1), target = Position(2, 5, 1), close={1, 5, 1};
  auto characters = Object::extract_all<Character>(_TEST_FOLDER_ + "characters.bin");
  auto location = NamedObject(0,"testfilename","testClassName");
  auto maps = Map::extract_all<Map>(_TEST_FOLDER_ + "mdstar.bin");
  auto objective = LocationObjective(0, "Test Objective", "Description for test objective", 1.1, location, close, 5,Status::Pending),
    require = LocationObjective(1, "Requirement1", "Description requirement", 1.1, NamedObject(*maps[0]), target, 0,Status::Pending);
  objective.addObjective(&require);
  characters[0]->setPosition(start);
  EXPECT_EQ(5, objective.getAcceptedRadius());
  EXPECT_EQ(close, objective.getPosition());
  EXPECT_EQ(location, objective.getLocation());
  objective.setLocation(NamedObject(*maps[0]));
  EXPECT_TRUE(objective.getLocation()==NamedObject(*maps[0]));
  EXPECT_FALSE(require.meetRequirements(characters[0],maps[0], 1.1));
  EXPECT_FALSE(objective.meetRequirements(characters[0],maps[0], 1.1));
  EXPECT_EQ(objective.update(characters[0], maps[0], 1.1), Status::Pending);
  maps[0]->addEntity(characters[0], true);
  EXPECT_FALSE(require.meetRequirements(characters[0],maps[0], 1.1));
  EXPECT_TRUE(objective.meetRequirements(characters[0],maps[0], 1.1));
  objective.setAcceptedRadius(4);
  EXPECT_EQ(4, objective.getAcceptedRadius());
  EXPECT_FALSE(require.meetRequirements(characters[0],maps[0], 1.1));
  EXPECT_FALSE(objective.meetRequirements(characters[0],maps[0], 1.1));
  objective.setPosition(target);
  EXPECT_EQ(target, objective.getPosition());
  EXPECT_FALSE(require.meetRequirements(characters[0],maps[0], 1.1));
  EXPECT_TRUE(objective.meetRequirements(characters[0],maps[0], 1.1));
  objective.setAcceptedRadius(1);
  EXPECT_EQ(1, objective.getAcceptedRadius());
  EXPECT_FALSE(require.meetRequirements(characters[0],maps[0], 1.1));
  EXPECT_FALSE(objective.meetRequirements(characters[0],maps[0], 1.1));
  maps[0]->moveEntity(characters[0], close);
  EXPECT_FALSE(require.meetRequirements(characters[0],maps[0], 1.1));
  EXPECT_TRUE(objective.meetRequirements(characters[0],maps[0], 1.1));
  EXPECT_EQ(objective.update(characters[0], maps[0], 1.1), Status::Pending);
  maps[0]->moveEntity(characters[0], target);
  EXPECT_TRUE(require.meetRequirements(characters[0],maps[0], 1.1));
  EXPECT_TRUE(objective.meetRequirements(characters[0],maps[0], 1.1));
  EXPECT_EQ(objective.update(characters[0], maps[0], 3.0), Status::Finished);
  for (auto i: characters)
    delete(i);
  for (auto i: maps)
    delete(i);
}

TEST(LocationObjectiveTest, Store)
{
  string filename = _TEST_FOLDER_ + "locationObjectives.bin",
    maps_filename = _TEST_FOLDER_ + "areas.bin";
  vector<LocationObjective> objectives;
  auto maps = Object::extract_all<Map>(maps_filename);
  EXPECT_EQ(maps.size(), 3);
  for (unsigned int i = 0; i < 3; i++)
  {
    objectives.emplace_back(i, "Test Objective"+ to_string(i), "Description for test objective"+ to_string(i), 1.1*i,
                            NamedObject(*maps[i]), Position(i, i, i), i, Status(i), 10.1*i, 11*i, filename);
    if (i>0)
      objectives[i].addObjective(&objectives[i-1]);
  }
  LocationObjective::save_all(filename, objectives);

  stringstream os, charac, position;
  os << objectives[0];
  charac << NamedObject(*maps[0]);
  position<<Position(0,0,0);

  string test = "id: 0, filename: " + filename + ", name: Test Objective0, " +
                "description: Description for test objective0,\n" +
                "status: 0, duration: 0, experience: 0, start time: 0,\n" +
                "required objectives: {},\n" +
                "location: {"+charac.str()+"},\n"+
                "position: {"+position.str()+"}, accepted radius: 0";
  EXPECT_EQ(test, os.str());

  auto objectives2 = LocationObjective::extract_all<LocationObjective>(filename);
  EXPECT_EQ(objectives.size(), objectives2.size());
  for (unsigned int i = 0; i < objectives2.size(); i++)
    EXPECT_TRUE(objectives2[i]->equals(objectives[i]));
  for (auto objective: objectives2)
    delete(objective);
  for (auto map: maps)
    delete(map);
}