//
// Created by drodarie on 11/17/17.
//

#include <iostream>
#include "gtest/gtest.h"
#include "../../main/models/position.h"

using namespace std;


TEST (PositionTest, Basic_Fonctions)
{
  int vect[3] = {-2, -2, -2};
  Position position(0, 0, 0), position2(-1, -1, -1), position3 = {vect};
  EXPECT_EQ(0, position.getX());
  EXPECT_EQ(0, position.getY());
  EXPECT_EQ(0, position.getZ());

  position.setX(-1);
  position.setY(-1);
  position.setZ(-1);

  EXPECT_EQ(-1, position.getX());
  EXPECT_EQ(-1, position.getY());
  EXPECT_EQ(-1, position.getZ());
  stringstream os;
  os << position;
  EXPECT_EQ("x: -1, y: -1, z: -1", os.str());
  EXPECT_TRUE(position2 == position);
  EXPECT_TRUE(position3 != position);
  EXPECT_TRUE(position3 < position);
  EXPECT_EQ(position3, position + position2);
  EXPECT_EQ(position2, position3 - position2);
  EXPECT_EQ(position3, position * 2);
  position3 = {position};
  EXPECT_TRUE(position == position3);
  auto *position4 = dynamic_cast<Position *>(Position::getCopy(&position2));
  EXPECT_TRUE(*position4 == position2);
  delete (position4);
}

TEST (PositionTest, Manhattan_Distances)
{
  Position position(0, 0, 0), position2(1, 1, -1), position3(1, 1, -1);

  EXPECT_EQ(static_cast<unsigned int>(3), position.getManhattanDistance(position2));
  EXPECT_EQ(static_cast<unsigned int>(2), position.get2DManhattanDistance(position2));

  EXPECT_EQ(static_cast<unsigned int>(0), position2.getManhattanDistance(position2));
  EXPECT_EQ(static_cast<unsigned int>(0), position2.get2DManhattanDistance(position2));

  EXPECT_EQ(static_cast<unsigned int>(0), position2.getManhattanDistance(position3));
  EXPECT_EQ(static_cast<unsigned int>(0), position2.get2DManhattanDistance(position3));
  position3.setZ(1);
  EXPECT_EQ(static_cast<unsigned int>(2), position2.getManhattanDistance(position3));
  EXPECT_EQ(static_cast<unsigned int>(0), position2.get2DManhattanDistance(position3));

  EXPECT_TRUE(position.getManhattanDistance(position2) == position2.getManhattanDistance(position));
  EXPECT_TRUE(position.get2DManhattanDistance(position2) == position2.get2DManhattanDistance(position));
  EXPECT_TRUE(position.getManhattanDistance(position2) == position3.getManhattanDistance(position));
  EXPECT_TRUE(position.get2DManhattanDistance(position2) == position3.get2DManhattanDistance(position));
}

TEST (PositionTest, Real_Distances)
{
  Position position(0, 0, 0), position2(1, 1, -1), position3(1, 1, -1);

  EXPECT_NEAR(sqrt(3), position.getDistance(position2), 1e-7);
  EXPECT_NEAR(sqrt(2), position.get2DDistance(position2), 1e-7);

  EXPECT_EQ(0, position2.getDistance(position2));
  EXPECT_EQ(0, position2.get2DDistance(position2));

  EXPECT_EQ(0, position2.getDistance(position3));
  EXPECT_EQ(0, position2.get2DDistance(position3));
  position3.setZ(1);
  EXPECT_EQ(2, position2.getDistance(position3));
  EXPECT_EQ(0, position2.get2DDistance(position3));

  EXPECT_TRUE(position.getDistance(position2) == position2.getDistance(position));
  EXPECT_TRUE(position.get2DDistance(position2) == position2.get2DDistance(position));
  EXPECT_TRUE(position.getDistance(position2) == position3.getDistance(position));
  EXPECT_TRUE(position.get2DDistance(position2) == position3.get2DDistance(position));
}

TEST(PositionTest, Norm)
{
  Position zero(0, 0, 0), unit(1, 0, 0), vec(1, 2, 2);
  vector<double> normUnit{1.0, 0.0, 0.0}, normVec{1.0 / 3.0, 2.0 / 3.0, 2.0 / 3.0};
  EXPECT_THROW(zero.norm(), ARGUMENT_ERROR);
  EXPECT_EQ(normUnit, unit.norm());
  EXPECT_EQ(normVec, vec.norm());
}

TEST(PositionTest, Dot)
{
  Position zero(0, 0, 0), vec(1, 2, 3), vec2(4, 5, 6);
  vector<double> zero2{0.0, 0.0, 0.0}, vect{1.0, 2.0, 3.0}, vect2{4.0, 5.0, 6.0, 7.0}, dot1{};
  EXPECT_EQ(0, zero.dot(vec2));
  EXPECT_EQ(0, vec.dot(zero));
  EXPECT_EQ(32, vec.dot(vec2));

  EXPECT_EQ(0.0, Position::dot(zero2, vect));
  EXPECT_EQ(0.0, Position::dot(zero2, vect2));
  EXPECT_EQ(32.0, Position::dot(vect, vect2));
}

TEST(PositionTest, Angle)
{
  Position position1(0, 0, 0), position2(1, 1, 0), position3(1, 0, 0), position4(1, 0, 1);
  EXPECT_THROW(position1.angle(position2), ARGUMENT_ERROR);
  EXPECT_THROW(position3.angle(position1), ARGUMENT_ERROR);
  EXPECT_NEAR(position2.angle(position3), M_PI_4, 1e-6);
  EXPECT_NEAR(position4.angle(position3), M_PI_4, 1e-6);
}

TEST(PositionTest, Line3D)
{
  Position center(0, 0, 0), target1(8, 1, 1), target2(1, 8, 1), target3(1, 1, 8);
  auto res = center.line3d(target1);
  EXPECT_EQ(9, res.size());
  for (unsigned int i = 0; i <= res.size() / 2; i++)
    EXPECT_EQ(res[i], Position(i, 0, 0));
  for (auto i = static_cast<unsigned int>(1 + res.size() / 2); i < res.size(); i++)
    EXPECT_EQ(res[i], Position(i, 1, 1));

  res = center.line3d(target2);
  EXPECT_EQ(9, res.size());
  for (unsigned int i = 0; i <= res.size() / 2; i++)
    EXPECT_EQ(res[i], Position(0, i, 0));
  for (auto i = static_cast<unsigned int>(1 + res.size() / 2); i < res.size(); i++)
    EXPECT_EQ(res[i], Position(1, i, 1));

  res = center.line3d(target3);
  EXPECT_EQ(9, res.size());
  for (unsigned int i = 0; i <= res.size() / 2; i++)
    EXPECT_EQ(res[i], Position(0, 0, i));
  for (auto i = static_cast<unsigned int>(1 + res.size() / 2); i < res.size(); i++)
    EXPECT_EQ(res[i], Position(1, 1, i));
}

TEST(PositionTest, IsInField)
{
  Position center(0, 0, 0), target(2, 1, 1), target2(1, 0, 0);
  EXPECT_EQ(0, center.isInField(target, Orientation::Steady, 1, M_PI));
  EXPECT_EQ(2, center.isInField(target2, Orientation::Right, 1, M_PI));
  EXPECT_EQ(0, center.isInField(target2, Orientation::Left, 1, M_PI));
  EXPECT_EQ(2, center.isInField(target2, Orientation::Forward, 1, M_PI));
  EXPECT_EQ(0, center.isInField(target, Orientation::Right, 1, M_PI));
  EXPECT_EQ(1, center.isInField(target2, Orientation::Right, 2, M_PI));
  EXPECT_EQ(0, center.isInField(target, Orientation::Right, 2, M_PI));
  EXPECT_EQ(2, center.isInField(target, Orientation::Right, 3, M_PI));
}

TEST(PositionTest, Store)
{
  string filename = _TEST_FOLDER_ + "positions.bin";

  vector<Position> positions(7);
  for (unsigned int i = 0; i < positions.size(); i++)
    positions[i] = Position(Orientation(i));
  Position::save_all(filename, positions);
  vector<Object *> positions2 = Position::load_all(filename);
  EXPECT_EQ(positions.size(), positions2.size());
  for (unsigned int i = 0; i < positions.size(); i++)
    EXPECT_EQ(positions[i], *dynamic_cast<Position *>(positions2[i]));
  for (auto &i: positions2)
    delete (i);
}