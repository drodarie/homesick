//
// Created by drodarie on 11/20/17.
//

#include <gtest/gtest.h>
#include "../main/globals.h"

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  mkdir(_DATA_FOLDER_.c_str(), 0777);
  mkdir(_TEST_FOLDER_.c_str(), 0777);

  // Delete created file if any
  for (auto &f: listDir(_TEST_FOLDER_))
    delete_file(f);
  return RUN_ALL_TESTS();
}